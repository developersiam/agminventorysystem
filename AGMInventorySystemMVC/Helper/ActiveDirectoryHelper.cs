﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace AGMInventorySystemMVC.Helper
{
    public static class ActiveDirectoryHelper
    {
        public static bool ValidateUserNameInActiveDirectory(string username)
        {
            // enter AD settings  
            PrincipalContext AD = new PrincipalContext(ContextType.Domain, "siamtobacco.com");

            // create search user and add criteria  
            UserPrincipal u = new UserPrincipal(AD);
            u.SamAccountName = username;

            // search for user  
            PrincipalSearcher search = new PrincipalSearcher(u);
            UserPrincipal result = (UserPrincipal)search.FindOne();
            search.Dispose();

            if (result != null)
                return true;
            else
                return false;
        }

        public static bool ActiveDirectoryAuthenticate(string username, string password)
        {
            bool result = false;
            using (DirectoryEntry _entry = new DirectoryEntry())
            {
                _entry.Username = username;
                _entry.Password = password;
                DirectorySearcher _searcher = new DirectorySearcher(_entry);
                _searcher.Filter = "(objectclass=user)";
                try
                {
                    SearchResult _sr = _searcher.FindOne();
                    //string _name = _sr.Properties["displayname"][0].ToString();
                    result = true;
                }
                catch(Exception ex)
                { /* Error handling omitted to keep code short: remember to handle exceptions !*/
                    throw ex;
                }
            }

            return result; //true = user authenticated!
        }
    }
}