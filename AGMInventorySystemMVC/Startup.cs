﻿using AGMInventorySystemMVC.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AGMInventorySystemMVC.Startup))]
namespace AGMInventorySystemMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            //app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
            ConfigureAuth(app);
        }
    }
}
