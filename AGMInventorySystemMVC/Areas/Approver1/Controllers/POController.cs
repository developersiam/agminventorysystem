﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemMVC.Areas.Approver1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGMInventorySystemMVC.Areas.Approver1.Controllers
{
    [Authorize(Roles = "Admin,Approver1,Approver2")]
    public class POController : Controller
    {
        // GET: Approver1/PO
        [HttpGet]
        public ActionResult Search(DateTime? from, DateTime? to)
        {
            var model = new vm_PurchaseOrder();
            model.From = (DateTime)(from == null ? new DateTime(DateTime.Now.Year, 1, 1) : from);
            model.To = (DateTime)(to == null ? DateTime.Now.Date : to);
            var purchaseOrders = BLServices.purchaseorder_masterBL()
                .GetByPurchaseOrderDateRange(model.From, model.To)
                .ToList();
            ViewBag.PurchaseOrders = purchaseOrders;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(vm_PurchaseOrder model)
        {
            var purchaseOrders = new List<purchaseorder_master>();
            if (!ModelState.IsValid)
            {
                ViewBag.PurchaseOrders = purchaseOrders;
                return View(model);
            }

            if (model.From > model.To)
            {
                ModelState.AddModelError("From", "Requested From cannot be over Requested To");
                return View(model);
            }

            purchaseOrders = BLServices.purchaseorder_masterBL()
                .GetByPurchaseOrderDateRange(Convert.ToDateTime(model.From), Convert.ToDateTime(model.To))
                .ToList();

            ViewBag.PurchaseOrders = purchaseOrders;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FirstApproverApprove(string pono)
        {
            if (string.IsNullOrEmpty(pono))
                throw new ArgumentException("Purchase order number cannot be empty.");

            if (!User.IsInRole("Approver1") && !User.IsInRole("Approver2"))
                throw new ArgumentException("This function use for the 1st approver and the final approver only.");

            var poMaster = BLServices.purchaseorder_masterBL().GetSingle(pono);
            if (poMaster == null)
                throw new ArgumentException("Fine the purchase order not found.");

            BLServices.purchaseorder_masterBL().Approve(pono, HttpContext.User.Identity.Name);
            return RedirectToAction("Search", "PO",
                new { from = DateTime.Now.Date, to = DateTime.Now.Date });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinalApproverApprove(string pono)
        {
            if (string.IsNullOrEmpty(pono))
                throw new ArgumentException("Purchase order number cannot be empty.");

            if (!User.IsInRole("Approver2"))
                throw new ArgumentException("This function use for the final approver only.");

            var poMaster = BLServices.purchaseorder_masterBL().GetSingle(pono);
            if (poMaster == null)
                throw new ArgumentException("Fine the purchase order not found.");

            BLServices.purchaseorder_masterBL().FinalApprove(pono, HttpContext.User.Identity.Name);
            return RedirectToAction("Search", "PO",
                new { from = DateTime.Now.Date, to = DateTime.Now.Date });
        }
    }
}