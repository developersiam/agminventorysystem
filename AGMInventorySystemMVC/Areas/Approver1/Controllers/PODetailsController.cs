﻿using AGMInventorySystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGMInventorySystemMVC.Areas.Approver1.Controllers
{
    [Authorize(Roles = "Admin,Approver1,Approver2")]
    public class PODetailsController : Controller
    {
        [HttpGet]
        public ActionResult Index(string pono)
        {
            if (string.IsNullOrEmpty(pono))
                throw new ArgumentException("The purchase order number cannot be empty.");

            var master = BLServices.purchaseorder_masterBL().GetSingle(pono);
            if(master==null)
                throw new ArgumentException("Find the purchase order number not found.");

            var list = BLServices.purchaseorder_detailsBL().GetByPono(pono);
            ViewBag.PurchaseOrderMaster = master;
                return View(list);
        }
    }
}