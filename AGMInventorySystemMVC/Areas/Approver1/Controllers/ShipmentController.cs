﻿using AGMInventorySystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGMInventorySystemMVC.Areas.Approver1.Controllers
{
    [Authorize(Roles = "Admin,Approver1,Approver2")]
    public class ShipmentController : Controller
    {
        // GET: Approver1/Shipment
        public ActionResult Index(string pono)
        {
            if (string.IsNullOrEmpty(pono))
                throw new ArgumentException("The purchase order number cannot be empty.");

            var poMaster = BLServices.purchaseorder_masterBL().GetSingle(pono);
            if (poMaster == null)
                throw new ArgumentException("Find the purchase order number not found.");

            ViewBag.PurchaseOrderMaster = poMaster;
            ViewBag.PurchaseOrderDetails = BLServices.purchaseorder_detailsBL().GetByPono(pono);
            ViewBag.ShipmentDetails = BLServices.shipment_detailsBL().GetByPono(pono);
            return View(BLServices.shipment_masterBL().GetByPono(pono));
        }
    }
}