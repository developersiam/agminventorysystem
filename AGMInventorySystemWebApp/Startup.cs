﻿using AGMInventorySystemWebApp.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AGMInventorySystemWebApp.Startup))]
namespace AGMInventorySystemWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
