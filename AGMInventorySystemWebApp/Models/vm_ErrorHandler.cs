﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGMInventorySystemWebApp.Models
{
    public class vm_ErrorHandler
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string ErrorMessage { get; set; }
    }
}