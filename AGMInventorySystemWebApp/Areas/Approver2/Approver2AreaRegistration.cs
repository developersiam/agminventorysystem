﻿using System.Web.Mvc;

namespace AGMInventorySystemWebApp.Areas.Approver2
{
    public class Approver2AreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Approver2";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Approver2_default",
                "Approver2/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}