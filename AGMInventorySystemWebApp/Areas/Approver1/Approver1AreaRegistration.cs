﻿using System.Web.Mvc;

namespace AGMInventorySystemWebApp.Areas.Approver1
{
    public class Approver1AreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Approver1";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Approver1_default",
                "Approver1/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}