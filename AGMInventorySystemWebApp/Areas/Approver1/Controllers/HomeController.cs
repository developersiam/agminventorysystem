﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGMInventorySystemWebApp.Areas.Approver1.Controllers
{
    [Authorize(Roles = "Admin,Approver1")]
    public class HomeController : Controller
    {
        // GET: Approver1/Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: Approver1/Home/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Approver1/Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Approver1/Home/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Approver1/Home/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Approver1/Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Approver1/Home/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Approver1/Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
