﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGMInventorySystemWebApp.Areas.Approver1.Controllers
{
    [Authorize(Roles = "Admin,Approver1")]
    public class POController : Controller
    {
        // GET: Approver1/PO
        public ActionResult Index()
        {
            return View();
        }

        // GET: Approver1/PO/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Approver1/PO/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Approver1/PO/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Approver1/PO/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Approver1/PO/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Approver1/PO/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Approver1/PO/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
