﻿using AGMInventorySystemBL;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.Helper
{
    public static class broughtforward_helper
    {
        public static List<brought_forward_d_vm> GetBFDetailsByBFNo(string BFNo)
        {
            var list = BLServices.brought_forward_d_BL()
                .GetBFDetails(BFNo);

            return list
                .Select(x => new brought_forward_d_vm
                {
                    broughtforward_d_id = x.broughtforward_d_id,
                    broughtforwardid = x.broughtforwardid,
                    itemid = x.itemid,
                    itemname = x.material_item.item_name,
                    lotno = x.material_item.lotno,
                    band_name = x.material_item.material_brand.brand_name,
                    unit_name = x.material_item.material_unit.unit_name,
                    quantity = x.quantity,
                    unitprice = x.unitprice,
                    modifieddate = x.modifieddate,
                    modifieduser = x.modifieduser
                })
                .ToList();
        }
        public static List<brought_forward_vm> GetBFListByCrop(short crop)
        {
            return BLServices.brought_forwardBL().GetByCrop(crop).Select(x => new brought_forward_vm
            {
                broughtforwardid = x.broughtforwardid,
                requesteddate = x.requesteddate,
                crop = x.crop,
                locationid = x.locationid,
                location_name = BLServices.locationBL().GetSingle(x.locationid).name,
                modifieddate = x.modifieddate,
                modifieduser = x.modifieduser,
                remark = x.remark
            }).ToList();
        }
    }
}
