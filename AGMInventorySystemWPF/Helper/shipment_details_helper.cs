﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.Helper
{
    public static class shipment_details_helper
    {
        public static List<shipment_details_vm> GetByPono(string pono)
        {
            return BLServices.shipment_detailsBL().GetByPono(pono)
                .Select(x => new shipment_details_vm
                {
                    shipmentdetailsid = x.shipmentdetailsid,
                    shipmentno = x.shipmentno,
                    itemid = x.itemid,
                    shipment_quantity = x.shipment_quantity,
                    received_quantity = x.received_quantity,
                    unitprice = x.unitprice,
                    remark = x.remark,
                    modifieddate = x.modifieddate,
                    modifieduser = x.modifieduser,
                    ReceivedButtonIsEanbled = x.received_quantity != x.shipment_quantity ? false : true
                }).ToList();
        }
    }
}
