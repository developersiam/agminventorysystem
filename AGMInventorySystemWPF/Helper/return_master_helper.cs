﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemBL;
using AGMInventorySystemWPF.ViewModel;

namespace AGMInventorySystemWPF.Helper
{
    public static class return_master_helper
    {
        public static List<return_master_vm> GetreturnListByCrop(short crop)
        {
            return BLServices.return_masterBL()
                .GetByCrop(crop)
                .Select(x => new return_master_vm
                {
                    returnno = x.returnno,
                    returndate = x.returndate,
                    issueno = x.issueno,
                    crop = x.crop,
                    tolocationid = x.tolocationid,
                    tolocation_name = x.location.name,
                    requesteddate = x.requesteddate,
                    requesteduser = x.requesteduser,
                    returntype = x.returntype,
                    remark = x.remark,
                })
                .ToList();
        }
        public static List<return_details_vm> GetreturnListByreturnNo(string returnno)
        {
            return BLServices.return_detailsBL()
                .GetRTDetails(returnno)
                .Select(x => new return_details_vm
            {
                returnno = x.returnno,
                itemid = x.itemid,
                itemname = x.material_item.item_name,
                band_name = x.material_item.material_brand.brand_name,
                description = x.material_item.item_description,
                unit_name = x.material_item.material_unit.unit_name,
                return_quantity = x.return_quantity,
            }).ToList();
        }
    }
}
