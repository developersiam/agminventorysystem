﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemBL;
using AGMInventorySystemWPF.ViewModel;

namespace AGMInventorySystemWPF.Helper
{
    public static class issue_master_helper
    {
        public static List<issue_master_vm> GetissueListByCrop(short crop)
        {
            return BLServices.issue_masterBL().GetByCrop(crop).Select(x => new issue_master_vm
            {
                issueno = x.issueno,
                issuedate = x.issuedate,
                crop = x.crop,
                locationid = x.locationid,
                location_name = BLServices.locationBL().GetSingle(x.locationid).name,
                requesteddate = x.requesteddate,
                requesteduser = x.requesteduser,
                remark = x.remark,
                supplier_name = x.issuesupplier != 0 ? BLServices.supplierBL().GetSingle((int)x.issuesupplier).supplier_name : "",
            }).ToList();
        }
        public static List<issue_details_vm> GetIssueDetailsByIssueNo(string issueNo)
        {
            return BLServices.issue_detailsBL().GetIssueDetails(issueNo).Select(x => new issue_details_vm
            {
                issuedetailsid = x.issuedetailsid,
                issueno = x.issueno,
                itemid = x.itemid,
                itemname = BLServices.material_itemBL().GetSingle(x.itemid).item_name,
                band_name = BLServices.material_brandBL().GetBrandNameByItem(x.itemid).ToString(),
                unit_name = BLServices.material_unitBL().GetUnitNameByItem(x.itemid).ToString(),
                issue_quantity = x.issue_quantity,
                remark = x.remark,
                modifieddate = x.modifieddate,
                modifieduser = x.modifieduser
            }).ToList();
        }

    }
}
