﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.Helper
{
    public struct InventoryCrop
    {
        public short crop { get; set; }
    }

    public struct TrueFalseStatus
    {
        public bool Status { get; set; }
        public string StatusName { get; set; }
    }

    public struct TaskStatus
    {
        public string Status { get; set; }
    }

    public static class user_setting
    {
        public static List<InventoryCrop> Crops
        {
            get
            {
                List<InventoryCrop> list = new List<InventoryCrop>();
                foreach (var item in BLServices.purchaseorder_masterBL()
                    .GetAll()
                    .GroupBy(x => x.crop)
                    .Select(x => new purchaseorder_master { crop = x.Key }))
                {
                    list.Add(new InventoryCrop { crop = item.crop });
                }

                if (list.Where(x => x.crop == DateTime.Now.Year).Count() <= 0)
                    list.Add(new InventoryCrop { crop = Convert.ToInt16(DateTime.Now.Year) });

                return list;
            }
        }
        public static List<TrueFalseStatus> Status
        {
            get
            {
                List<TrueFalseStatus> list = new List<TrueFalseStatus>();
                list.Add(new TrueFalseStatus { Status = true, StatusName = "Finish" });
                list.Add(new TrueFalseStatus { Status = false, StatusName = "Not Finish" });

                return list;
            }
        }
        public static policy_user User { get; set; }
        public static List<policy_role> UserRoles { get; set; }
        public static List<location> UserLocations { get; set; }
        public static List<TaskStatus> TaskStatus { get {
                List<TaskStatus> list = new List<TaskStatus>();
                list.Add(new TaskStatus { Status = "Waiting" });
                list.Add(new TaskStatus { Status = "Approved 1" });
                list.Add(new TaskStatus { Status = "Approved 2" });
                list.Add(new TaskStatus { Status = "Complete" });

                return list;
            } }
    }
}
