﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemBL;
using AGMInventorySystemWPF.ViewModel;

namespace AGMInventorySystemWPF.Helper
{
    public static class movement_master_helper
    {
        public static List<movement_master_vm> GetmovementListByCrop(short crop, string flocationid)
        {
            return BLServices.movement_masterBL().GetByCropLocation(crop, flocationid).Select(x => new movement_master_vm
            {
                movementno = x.movementno,
                movementdate = x.movementdate,
                crop = x.crop,
                fromlocationid = x.fromlocationid,
                fromlocation_name = BLServices.locationBL().GetSingle(x.fromlocationid).name,
                tolocationid = x.tolocationid,
                tolocation_name = BLServices.locationBL().GetSingle(x.tolocationid).name,
                createdate = x.createdate,
                createduser = x.createduser,
                receiveddate = x.receiveddate,
                receiveduser = x.receiveduser,
                receivedstatus = x.receivedstatus,
                modifieddate = x.modifieddate,
                modifieduser = x.modifieduser,
                approveddate = x.approveddate,
                approveduser = x.approveduser,
                isapproveddate = x.approveddate != null ? true : false,
            }).ToList();
        }
        public static List<movement_master_vm> GetmovementListBy2Location(short crop, string flocationid,string tlocationid)
        {
            return BLServices.movement_masterBL().GetByCrop2Location(crop, flocationid,tlocationid).Select(x => new movement_master_vm
            {
                movementno = x.movementno,
                movementdate = x.movementdate,
                crop = x.crop,
                fromlocationid = x.fromlocationid,
                fromlocation_name = BLServices.locationBL().GetSingle(x.fromlocationid).name,
                tolocationid = x.tolocationid,
                tolocation_name = BLServices.locationBL().GetSingle(x.tolocationid).name,
                createdate = x.createdate,
                createduser = x.createduser,
                receiveddate = x.receiveddate,
                receiveduser = x.receiveduser,
                receivedstatus = x.receivedstatus,
                modifieddate = x.modifieddate,
                modifieduser = x.modifieduser,
                approveddate = x.approveddate,
                approveduser = x.approveduser,
                isapproveddate = x.approveddate != null ? true : false,
            }).ToList();
        }
        public static List<movement_master_vm> GetmovementListByCropUserLocation(short crop, string userlocationid)
        {
            return BLServices.movement_masterBL().GetByCropuserLocation(crop, userlocationid).Select(x => new movement_master_vm
            {
                movementno = x.movementno,
                movementdate = x.movementdate,
                crop = x.crop,
                fromlocationid = x.fromlocationid,
                fromlocation_name = BLServices.locationBL().GetSingle(x.fromlocationid).name,
                tolocationid = x.tolocationid,
                tolocation_name = BLServices.locationBL().GetSingle(x.tolocationid).name,
                createdate = x.createdate,
                createduser = x.createduser,
                receiveddate = x.receiveddate,
                receiveduser = x.receiveduser,
                receivedstatus = x.receivedstatus,
                modifieddate = x.modifieddate,
                modifieduser = x.modifieduser,
                approveddate = x.approveddate,
                approveduser = x.approveduser,
                isapproveddate = x.approveddate != null ? true : false,
            }).ToList();
        }
        public static List<movement_master_vm> GetAllmovementList(short crop)
        {
            return BLServices.movement_masterBL().GetByCrop(crop).Select(x => new movement_master_vm
            {
                movementno = x.movementno,
                movementdate = x.movementdate,
                crop = x.crop,
                fromlocationid = x.fromlocationid,
                fromlocation_name = BLServices.locationBL().GetSingle(x.fromlocationid).name,
                tolocationid = x.tolocationid,
                tolocation_name = BLServices.locationBL().GetSingle(x.tolocationid).name,
                createdate = x.createdate,
                createduser = x.createduser,
                receiveddate = x.receiveddate,
                receiveduser = x.receiveduser,
                receivedstatus = x.receivedstatus,
                modifieddate = x.modifieddate,
                modifieduser = x.modifieduser,
                approveddate = x.approveddate,
                approveduser = x.approveduser,
                isapproveddate = x.approveddate != null ? true : false,
            }).ToList();
        }
        public static List<movement_details_vm> GetMovementDetailsByMovementNo(string movementNo)
        {
            return BLServices.movement_detailsBL().GetByMovementNo(movementNo).Select(x => new movement_details_vm
            {
                movementdetailsno = x.movementdetailsno,
                itemid = x.itemid,
                itemname = BLServices.material_itemBL().GetSingle(x.itemid).item_name,
                quantity = x.quantity,
                remark = x.remark,
                modifieddate = x.modifieddate
            }).ToList();
        }


    }
}
