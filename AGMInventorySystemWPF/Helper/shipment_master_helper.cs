﻿using AGMInventorySystemBL;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.Helper
{
    public static class shipment_master_helper
    {
        public static List<shipment_master_vm> GetByPono(string pono)
        {
            return BLServices.shipment_masterBL()
                .GetByPono(pono)
                .Select(x => new shipment_master_vm
                {
                    shipmentno = x.shipmentno,
                    pono = x.pono,
                    shipmentdate = x.shipmentdate,
                    crop = x.crop,
                    locationid = x.locationid,
                    location = x.location,
                    shipmentstatus = x.shipmentstatus,
                    address = x.address,
                    remark = x.remark,
                    receiveddate = x.receiveddate,
                    receivedstatus = x.receivedstatus,
                    receiveduser = x.receiveduser,
                    requesteddate = x.requesteddate,
                    requesteduser = x.requesteduser,
                    approveddate1 = x.approveddate1,
                    approveddate2 = x.approveddate2,
                    approveduser1 = x.approveduser1,
                    approveduser2 = x.approveduser2,
                    modifieddate = x.modifieddate,
                    modifieduser = x.modifieduser,
                    IsApproved = x.approveddate1 != null ? true : false,
                    IsFinalApproved = x.approveddate2 != null ? true : false
                }).ToList();
        }
    }
}
