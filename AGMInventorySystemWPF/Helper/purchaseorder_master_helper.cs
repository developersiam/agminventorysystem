﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.Helper
{
    public static class purchaseorder_master_helper
    {
        public static List<purchaseorder_master_vm> GetByCrop(short crop)
        {
            return BLServices.purchaseorder_masterBL()
                .GetByCrop(crop)
                .Select(x => new purchaseorder_master_vm
                {
                    pono = x.pono,
                    podate = x.podate,
                    invoice_no = x.invoice_no,
                    purchaserequisitionno = x.purchaserequisitionno,
                    purchaserequisitiondate = x.purchaserequisitiondate,
                    crop = x.crop,
                    venderid = x.venderid,
                    locationid = x.locationid,
                    postatus = x.postatus,
                    contact_person = x.contact_person,
                    delivery_amt = x.delivery_amt,
                    total_amt = x.total_amt,
                    faxdate = x.faxdate,
                    paymentterm = x.paymentterm,
                    deliverydate = x.deliverydate,
                    deliverystatus = x.deliverystatus,
                    issuedate = x.issuedate,
                    issueuser = x.issueuser,
                    requesteddate = x.requesteddate,
                    requesteduser = x.requesteduser,
                    approveddate1 = x.approveddate1,
                    approveduser1 = x.approveduser1,
                    approveddate2 = x.approveddate2,
                    approveduser2 = x.approveduser2,
                    modifieddate = x.modifieddate,
                    modifieduser = x.modifieduser,
                    location = x.location,
                    vender = x.vender,
                    isapproveddate1 = x.approveddate1 != null ? true : false,
                    isapproveddate2 = x.approveddate2 != null ? true : false
                }).ToList();
        }
    }
}
