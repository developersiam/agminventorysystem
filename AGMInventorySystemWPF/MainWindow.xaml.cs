﻿using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Home());
        }

        private void LogOffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                user_setting.UserRoles = null;
                MainFrame.NavigationService.Navigate(new Form.Login());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ManageUserMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Super Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.UserAccount.Users());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PurchaseOrderMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.PurchaseOrder.POList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
		}

        private void itemMenu_Click(object sender, RoutedEventArgs e)
        {
            if (user_setting.User == null)
            {
                MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                MainFrame.NavigationService.Navigate(new Form.Login());
                return;
            }
            if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");
            MainFrame.Navigate(new Form.Material.Items());
        }

        private void categoryMenu_Click(object sender, RoutedEventArgs e)
        {
            if (user_setting.User == null)
            {
                MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                MainFrame.NavigationService.Navigate(new Form.Login());
                return;
            }
            if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");
            MainFrame.Navigate(new Form.Material.Category());
        }

        private void unitMenu_Click(object sender, RoutedEventArgs e)
        {
            if (user_setting.User == null)
            {
                MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                MainFrame.NavigationService.Navigate(new Form.Login());
                return;
            }
            if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");
            MainFrame.Navigate(new Form.Material.UnitItem());
        }

        private void brandMenu_Click(object sender, RoutedEventArgs e)
        {
            if (user_setting.User == null)
            {
                MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                MainFrame.NavigationService.Navigate(new Form.Login());
                return;
            }
            if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");
            MainFrame.Navigate(new Form.Material.BrandItem());
        }

        private void VenderMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.NavigationService.Navigate(new Form.Vender.VenderList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LocationMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.NavigationService.Navigate(new Form.Location.LocationAdd());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MovementListMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.NavigationService.Navigate(new Form.Movement.MovementList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShipmentMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");               

                var window = new Form.Shipment.ShipmentList(0,"","");
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceivingMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.NavigationService.Navigate(new Form.Shipment.ShipmentReceiving());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void stockMenu_Click(object sender, RoutedEventArgs e)
        {
            if (user_setting.User == null)
            {
                MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                MainFrame.NavigationService.Navigate(new Form.Login());
                return;
            }

            if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0)
                throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

            MainFrame.NavigationService.Navigate(new Form.Material.Transactions());
        }

        private void requesterMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Super Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.UserAccount.Requesters());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void issueMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0 && user_setting.UserLocations.Where(l => l.locationid == "03").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Movement.IssueList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BFMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0 && user_setting.UserLocations.Where(l => l.locationid == "03").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.BroughtForward.BFList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void returnMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0 && user_setting.UserLocations.Where(l => l.locationid == "03").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Movement.ReturnList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SupplierMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() <= 0 && user_setting.UserLocations.Where(l => l.locationid == "03").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Movement.Supplier());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
