﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class return_details_vm : return_details
    {
        public string itemname { get; set; }
        public string band_name { get; set; }
        public string unit_name { get; set; }
        public string description { get; set; }
    }
}
