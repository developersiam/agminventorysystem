﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class purchase_order_details_vm : purchaseorder_details
    {
        public string item_name { get; set; }
        public string brand_name { get; set; }
        public string unit_name { get; set; }
        public string lotno { get; set; }
    }
}
