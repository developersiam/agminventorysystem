﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class issue_details_vm : issue_details
    {
        public string itemname { get; set; }
        public string band_name { get; set; }
        public string unit_name { get; set; }
    }
}
