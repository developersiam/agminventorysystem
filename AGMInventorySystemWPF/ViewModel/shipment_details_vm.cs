﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class shipment_details_vm : shipment_details
    {
        public bool ReceivedButtonIsEanbled { get; set; }
        public bool CancelReceivedButtonIsEanbled { get; set; }
    }
}
