﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class Item_vm
    {
        public int itemid { get; set; }
        public string item_name { get; set; }
        public string item_description { get; set; }
        public string lotno { get; set; }
        public string brandname { get; set; }
        public int brandid { get; set; }
        public string unitname { get; set; }
        public int unitid { get; set; }
        public string modifieduser { get; set; }
        public System.DateTime modifieddate { get; set; }

    }
}
