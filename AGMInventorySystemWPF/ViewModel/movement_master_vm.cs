﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemEntities;

namespace AGMInventorySystemWPF.ViewModel
{
    public class movement_master_vm : movement_master
    {
        public bool isapproveddate { get; set; }
        public string fromlocation_name { get; set; }
        public string tolocation_name { get; set; }
    }
}
