﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class shipment_master_vm : shipment_master
    {
        public bool IsApproved { get; set; }
        public bool IsFinalApproved { get; set; }        
    }
}
