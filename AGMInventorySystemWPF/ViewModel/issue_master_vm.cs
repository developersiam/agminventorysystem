﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemWPF.ViewModel
{
    public class issue_master_vm : issue_master
    {
        public string location_name { get; set; }
        public string supplier_name { get; set; }
    }
}
