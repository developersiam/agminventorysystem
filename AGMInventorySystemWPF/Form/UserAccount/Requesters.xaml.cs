﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;

namespace AGMInventorySystemWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for Requesters.xaml
    /// </summary>
    public partial class Requesters : Page
    {
        public Requesters()
        {
            InitializeComponent();
            DataGridBinding();
        }

        private void DataGridBinding()
        {
            try
            {
                RequesterDataGrid.ItemsSource = null;
                RequesterDataGrid.ItemsSource = BLServices.requesterBL().GetAll();

                TotalTextBlock.Text = RequesterDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FNameTextBox.Text == "") throw new ArgumentException("โปรดกรอก Name");
                if (LNameTextBox.Text == "") throw new ArgumentException("โปรดกรอก Last Name");
                if (PositionTextBox.Text == "") throw new ArgumentException("โปรดกรอก Position");

                var item = new requester_user
                {

                    name = FNameTextBox.Text,
                    lastname = LNameTextBox.Text,
                    position = PositionTextBox.Text,
                    status = true,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username,
                    title = titleCombobox.Text                    
                };

                var existed = BLServices.requesterBL().GetSingleByNameLName(FNameTextBox.Text, LNameTextBox.Text);
                if (existed != null) EditRequester(existed);
                else
                {
                    BLServices.requesterBL().Add(item);
                }

                DataGridBinding();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditRequester(requester_user existed)
        {
            try
            {
                if (existed != null)
                {
                    BLServices.requesterBL().Edit(new requester_user
                    {
                        name = FNameTextBox.Text,
                        lastname = LNameTextBox.Text,
                        position = PositionTextBox.Text,
                        status = true,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username,
                        title = titleCombobox.Text
                    });
                };
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearDetails()
        {
            FNameTextBox.Clear();
            LNameTextBox.Clear();
            PositionTextBox.Clear();
            titleCombobox.Text = "";
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearDetails();
        }

        private void RequesterDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (RequesterDataGrid.SelectedIndex < 0)
                    return;

                var selected = (requester_user)RequesterDataGrid.SelectedItem;
                if (selected != null)
                {
                    FNameTextBox.Text = selected.name;
                    LNameTextBox.Text = selected.lastname;
                    PositionTextBox.Text = selected.position;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RequesterDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการ Inactive Requester นี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (requester_user)RequesterDataGrid.SelectedItem;

                //if (BLServices.material_categoryBL().UsedforItem(model.categoryid)) throw new Exception("ไม่สามารถลบ category นี้ได้ เพราะมีการบันทึกข้อมูล Category นี้ใน Material แล้ว");

                BLServices.requesterBL().Updatestatus(model);

                DataGridBinding();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
