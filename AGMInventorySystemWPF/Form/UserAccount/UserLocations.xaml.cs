﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for UserLocations.xaml
    /// </summary>
    /// 
    public struct UserLocationDataGrid
    {
        public string locationid { get; set; }
        public string name { get; set; }
        public bool AssignStatus { get; set; }
    }

    public partial class UserLocations : Window
    {
        policy_user _user;

        public UserLocations(policy_user user)
        {
            InitializeComponent();

            _user = new policy_user();
            _user = user;

            BindingUserLocationDataGrid();
        }

        private void BindingUserLocationDataGrid()
        {
            try
            {
                var list = from r in BLServices.locationBL().GetAll()
                           join u in BLServices.user_locationBL().GetByUsername(_user.username)
                           on r.locationid equals u.locationid into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserLocationDataGrid
                           {
                               locationid = r.locationid,
                               name = r.name,
                               AssignStatus = rs == null ? false : true
                           };

                UserLocationDataGrid.ItemsSource = null;
                UserLocationDataGrid.ItemsSource = list.OrderBy(x => x.name);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UserLocationDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (UserLocationDataGrid.SelectedIndex < 0)
                    return;

                var model = (UserLocationDataGrid)UserLocationDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BLServices.user_locationBL()
                        .Add(new user_location
                        {
                            username = _user.username,
                            locationid = model.locationid,
                            createdate = DateTime.Now,
                            createuser = user_setting.User.username
                        });
                else
                    BLServices.user_locationBL().Delete(_user.username, model.locationid);

                BindingUserLocationDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

    }
}
