﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class Users : Page
    {
        public Users()
        {
            InitializeComponent();
            DataGridBinding();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                if (Helper.ActiveDirectoryHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบข้อมูลผู้ใช้ " + UsernameTextBox.Text + " ในระบบ");

                BLServices.policy_userBL()
                    .Add(new policy_user
                    {
                        username = UsernameTextBox.Text,
                        createuser = user_setting.User.username,
                        createdate = DateTime.Now
                    });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                UserAccountDataGrid.ItemsSource = null;
                UserAccountDataGrid.ItemsSource = BLServices.policy_userBL().GetAll();

                TotalTextBlock.Text = UserAccountDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            UsernameTextBox.Clear();
            AddButton.IsEnabled = false;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                if (Helper.ActiveDirectoryHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบข้อมูลผู้ใช้ " + UsernameTextBox.Text + " ในระบบ");

                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (policy_user)UserAccountDataGrid.SelectedItem;
                BLServices.policy_userBL().Delete(model.username);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddRoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var model = (policy_user)UserAccountDataGrid.SelectedItem;
                UserRoles window = new UserRoles(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
        
        private void AddLocaitonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var model = (policy_user)UserAccountDataGrid.SelectedItem;
                UserLocations window = new UserLocations(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
