﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for UserRoles.xaml
    /// </summary>
    /// 

    public struct UserRoleDataGrid
    {
        public Guid RoleID { get; set; }
        public string RoleName { get; set; }
        public bool AssignStatus { get; set; }
    }

    public partial class UserRoles : Window
    {
        policy_user _user;
        public UserRoles(policy_user user)
        {
            InitializeComponent();

            _user = new policy_user();
            _user = user;

            BindingUserRoleDataGrid();
        }

        private void BindingUserRoleDataGrid()
        {
            try
            {
                var list = from r in BLServices.policy_roleBL().GetAll()
                           join u in BLServices.policy_userroleBL().GetByUsername(_user.username)
                           on r.roleid equals u.roleid into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserRoleDataGrid
                           {
                               RoleID = r.roleid,
                               RoleName = r.rolename,
                               AssignStatus = rs == null ? false : true
                           };

                UserRoleDataGrid.ItemsSource = null;
                UserRoleDataGrid.ItemsSource = list.OrderBy(x => x.RoleName);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UserRoleDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (UserRoleDataGrid.SelectedIndex < 0)
                    return;

                var model = (UserRoleDataGrid)UserRoleDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BLServices.policy_userroleBL()
                        .Add(new policy_userrole
                        {
                            username = _user.username,
                            roleid = model.RoleID,
                            createdate = DateTime.Now,
                            createuser = user_setting.User.username
                        });
                else
                    BLServices.policy_userroleBL().Delete(_user.username, model.RoleID);

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Roles window = new Roles();
                window.ShowDialog();

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
