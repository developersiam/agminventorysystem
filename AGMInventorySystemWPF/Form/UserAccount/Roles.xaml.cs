﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for Roles.xaml
    /// </summary>
    public partial class Roles : Window
    {
        policy_role _role;
        public Roles()
        {
            InitializeComponent();
            DataGridBinding();
            _role = new policy_role();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (policy_role)RoleDataGrid.SelectedItem;
                BLServices.policy_roleBL().Delete(model.roleid);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                RoleNameTextBox.IsEnabled = false;

                _role = (policy_role)RoleDataGrid.SelectedItem;

                RoleNameTextBox.Text = _role.rolename;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                BLServices.policy_roleBL().Add(new policy_role
                {
                    roleid = Guid.NewGuid(),
                    rolename = RoleNameTextBox.Text,
                    createby = user_setting.User.username,
                    createdate = DateTime.Now,
                    modifieduser = user_setting.User.username,
                    modifieddate = DateTime.Now
                });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            RoleNameTextBox.Clear();
            AddButton.IsEnabled = true;
            EditButton.IsEnabled = false;
        }

        private void DataGridBinding()
        {
            try
            {
                RoleDataGrid.ItemsSource = null;
                RoleDataGrid.ItemsSource = BLServices.policy_roleBL()
                    .GetAll()
                    .OrderBy(x => x.rolename);

                TotalTextBlock.Text = RoleDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Role Name");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _role.rolename = RoleNameTextBox.Text;
                _role.modifieduser = user_setting.User.username;
                _role.modifieddate = DateTime.Now;

                BLServices.policy_roleBL().Edit(_role);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
