﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;

namespace AGMInventorySystemWPF.Form.Material
{

    public partial class UnitItem : Page
    {
        public UnitItem()
        {
            InitializeComponent();
            DataGridBinding();
        }
        private void DataGridBinding()
        {
            try
            {
                unitDataGrid.ItemsSource = null;
                unitDataGrid.ItemsSource = BLServices.material_unitBL().GetAll();

                TotalTextBlock.Text = unitDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (unitNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Unit name");

                var item = new material_unit
                {
                    unit_name = unitNameTextBox.Text,
                    unit_description = descripTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                };

                var existed = BLServices.material_unitBL().GetSingleByName(unitNameTextBox.Text);
                if (existed != null) EditUnit(existed);
                else
                {
                    BLServices.material_unitBL().Add(item);
                }

                DataGridBinding();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
         }

        private void ClearDetails()
        {
            unitNameTextBox.Clear();
            descripTextBox.Clear();
        }

        private void EditUnit(material_unit existed)
        {
            try
            {
                if (existed != null)
                {
                    BLServices.material_unitBL().Edit(new material_unit
                    {
                        unit_name = unitNameTextBox.Text,
                        unit_description = descripTextBox.Text,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });
                };
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearDetails();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (unitDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (material_unit)unitDataGrid.SelectedItem;

                if (BLServices.material_unitBL().UsedforItem(model.unitid)) throw new Exception("ไม่สามารถลบ unit name นี้ได้ เพราะมีการใช้ข้อมูล unit นี้ใน Material แล้ว");

                BLServices.material_unitBL().Delete(model.unitid);

                DataGridBinding();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void unitDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (unitDataGrid.SelectedIndex < 0)
                    return;

                var selected = (material_unit)unitDataGrid.SelectedItem;
                if (selected != null)
                {
                    unitNameTextBox.Text = selected.unit_name;
                    descripTextBox.Text = selected.unit_description;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
