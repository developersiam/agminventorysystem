﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;

namespace AGMInventorySystemWPF.Form.Material
{
    /// <summary>
    /// Interaction logic for Items.xaml
    /// </summary>
    public partial class Items : Page
    {       
        int selectedCat;
        int selectedBrand;
        int selectedUnit;
        int selecteditem;
        public Items()
        {
            InitializeComponent();
            DataBlinding();
        }

        private void DataBlinding()
        {
            try
            {
                //Category blinding
                CategoryCombobox.ItemsSource = null;
                CategoryCombobox.ItemsSource = BLServices.material_categoryBL().GetAll();

                //Unit blinding
                UnitCombobox.ItemsSource = null;
                UnitCombobox.ItemsSource = BLServices.material_unitBL().GetAll();

                ItemDataGrid.ItemsSource = null;
                //ItemDataGrid.ItemsSource = BLServices.material_itemBL().get

                TotalTextBlock.Text = ItemDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (Item_vm)ItemDataGrid.SelectedItem;

                if (BLServices.material_itemBL().UsedforPO(model.itemid)) throw new Exception("ไม่สามารถลบ item นี้ได้ เพราะมีการใช้ข้อมูลนี้ใน Purchase Order แล้ว");

                BLServices.material_itemBL().Delete(model.itemid);

                ShowItemDetails();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private void ClearDetails()
        {
            itemNameTextBox.Text = "";
            lotNoTextBox.Text = "";
            descriptionTextBox.Text = "";
            selecteditem = 0;

            //Unit blinding
            UnitCombobox.ItemsSource = null;
            UnitCombobox.ItemsSource = BLServices.material_unitBL().GetAll();

        }

        private void ShowItemDetails()
        {
            string brandname = "";
            string unitname = "";
            ItemDataGrid.ItemsSource = null;
            if (CategoryCombobox.SelectedIndex < 0) return;

            var cat = (material_category)CategoryCombobox.SelectedItem;
            if (cat == null) return;

            var allList = BLServices.material_itemBL().GetAll().Where(c => c.categoryid == cat.categoryid).ToList();
            if (allList != null || allList.Count()>0)
            {
                List<Item_vm> itemList = new List<Item_vm>();
                foreach (var i in allList)
                {
                    brandname = BLServices.material_brandBL().GetSingle(i.brandid).brand_name;
                    unitname = BLServices.material_unitBL().GetSingle(i.unitid).unit_name;

                    Item_vm e = new Item_vm()
                    {
                        itemid = i.itemid,
                        item_name = i.item_name,
                        item_description = i.item_description,
                        brandname = brandname,
                        unitname = unitname,
                        lotno = i.lotno,
                        brandid = i.brandid,
                        unitid = i.unitid,
                    };
                    itemList.Add(e);
                }
                ItemDataGrid.ItemsSource = itemList;
            }

            TotalTextBlock.Text = ItemDataGrid.Items.Count.ToString("N0");
        }

        private void CategoryCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedCat = 0;
            var cat = (material_category)CategoryCombobox.SelectedItem;
            if (cat == null) return;

            selectedCat = cat.categoryid;

            BrandCombobox.ItemsSource = null;
            BrandCombobox.ItemsSource = BLServices.material_brandBL().GetAll().Where(s => s.categoryid == selectedCat);
            selecteditem = 0;

            ShowItemDetails();
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (itemNameTextBox.Text == "") throw new ArgumentException("Please fill Item Name before add");
                if (CategoryCombobox.Text == "") throw new ArgumentException("Please select Category before add");
                if (BrandCombobox.Text == "") throw new ArgumentException("Please select Brand before add");
                if (UnitCombobox.Text == "") throw new ArgumentException("Please select Unit type before add");

                var item = new material_item
                {
                    categoryid = selectedCat,
                    brandid = selectedBrand,
                    unitid = selectedUnit,
                    item_name = itemNameTextBox.Text,
                    item_description = descriptionTextBox.Text,
                    lotno = lotNoTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                };

                if (selecteditem > 0)
                {
                    var existed = BLServices.material_itemBL().GetSingle(selecteditem);
                    if (existed != null) EditItem(existed);
                }
                else
                {
                    BLServices.material_itemBL().Add(item);
                }

                ShowItemDetails();
                ClearDetails();

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private void EditItem(material_item existed)
        {
            try
            {
                if (existed != null)
                {
                    BLServices.material_itemBL().Edit(new material_item
                    {
                        categoryid = selectedCat,
                        brandid = selectedBrand,
                        itemid = existed.itemid,
                        item_name = itemNameTextBox.Text,
                        item_description = descriptionTextBox.Text,
                        lotno = lotNoTextBox.Text,
                        unitid = selectedUnit,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });
                    selecteditem = 0;
                };
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BrandCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedBrand = 0;
            var brand = (material_brand)BrandCombobox.SelectedItem;
            if (brand == null) return;

            selectedBrand = brand.brandid;
            selecteditem = 0;
        }

        private void UnitCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedUnit = 0;
            var unit = (material_unit)UnitCombobox.SelectedItem;
            if (unit == null) return;

            selectedUnit = unit.unitid;
        }

        private void ItemDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (ItemDataGrid.SelectedIndex < 0)
                    return;

                var selected = (Item_vm)ItemDataGrid.SelectedItem;
                if (selected != null)
                {
                    //Brand
                    BrandCombobox.SelectedValue = selected.brandname;
                    selectedBrand = selected.brandid;

                    UnitCombobox.SelectedValue = selected.unitname;
                    selectedUnit = selected.unitid;

                    selecteditem = selected.itemid;
                    itemNameTextBox.Text = selected.item_name;
                    descriptionTextBox.Text = selected.item_description;
                    lotNoTextBox.Text = selected.lotno;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }


        }
    }
}
