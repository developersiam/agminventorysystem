﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;

namespace AGMInventorySystemWPF.Form.Material
{
    /// <summary>
    /// Interaction logic for Category.xaml
    /// </summary>
    public partial class Category : Page
    {
        public Category()
        {
            InitializeComponent();
            DataGridBinding();
        }
        private void DataGridBinding()
        {
            try
            {
                CategoryDataGrid.ItemsSource = null;
                CategoryDataGrid.ItemsSource = BLServices.material_categoryBL().GetAll();

                TotalTextBlock.Text = CategoryDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (catNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Category name");

                var item = new material_category
                {
                    category_name = catNameTextBox.Text,
                    category_description = descripTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                };

                var existed = BLServices.material_categoryBL().GetSingleByName(catNameTextBox.Text);
                if (existed != null) EditCat(existed);
                else
                {
                    BLServices.material_categoryBL().Add(item);
                }

                DataGridBinding();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditCat(material_category existed)
        {
            try
            {
                if (existed != null)
                {
                    BLServices.material_categoryBL().Edit(new material_category
                    {
                        category_name = catNameTextBox.Text,
                        category_description = descripTextBox.Text,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });
                };
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearDetails()
        {
            catNameTextBox.Clear();
            descripTextBox.Clear();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CategoryDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (material_category)CategoryDataGrid.SelectedItem;

                if (BLServices.material_categoryBL().UsedforItem(model.categoryid)) throw new Exception("ไม่สามารถลบ category นี้ได้ เพราะมีการบันทึกข้อมูล Category นี้ใน Material แล้ว");

                BLServices.material_categoryBL().Delete(model.categoryid);

                DataGridBinding();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearDetails();
        }

        private void CategoryDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (CategoryDataGrid.SelectedIndex < 0)
                    return;

                var selected = (material_category)CategoryDataGrid.SelectedItem;
                if (selected != null)
                {
                    catNameTextBox.Text = selected.category_name;
                    descripTextBox.Text = selected.category_description;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
