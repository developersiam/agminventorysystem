﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;

namespace AGMInventorySystemWPF.Form.Material
{
    public partial class BrandItem : Page
    {
        int selectedCat;
        int selectedBrand;
        public BrandItem()
        {
            InitializeComponent();
            DataGridBinding();
        }
        private void DataGridBinding()
        {
            try
            {
                //Category blinding
                CategoryCombobox.ItemsSource = null;
                CategoryCombobox.ItemsSource = BLServices.material_categoryBL().GetAll();

                BrandDataGrid.ItemsSource = null;
                //if (CategoryCombobox.SelectedIndex < 0) return;

                //var cat = (material_category)CategoryCombobox.SelectedItem;
                //if (cat == null) return;
                //BrandDataGrid.ItemsSource = BLServices.material_brandBL().GetAll().Where(s => s.categoryid == selectedCat);

                TotalTextBlock.Text = BrandDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (brandNameTextBox.Text == "" && CategoryCombobox.Text == "")
                    throw new ArgumentException("Please select Category and input Brand name");
               
                var item = new material_brand
                {
                    categoryid = selectedCat,
                    brand_name = brandNameTextBox.Text,
                    brand_description = descripTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                };

                if (selectedBrand > 0)
                {
                    var existed = BLServices.material_brandBL().GetSingle(selectedBrand);
                    if (existed != null) EditBrand(existed);
                    selectedBrand = 0;
                }
                else
                {
                    BLServices.material_brandBL().Add(item);
                }

                ShowBrandDetails();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShowBrandDetails()
        {
            BrandDataGrid.ItemsSource = null;
            if (CategoryCombobox.SelectedIndex < 0) return;

            var cat = (material_category)CategoryCombobox.SelectedItem;
            if (cat == null) return;
            BrandDataGrid.ItemsSource = BLServices.material_brandBL().GetAll().Where(s => s.categoryid == selectedCat);

            TotalTextBlock.Text = BrandDataGrid.Items.Count.ToString("N0");
        }

        private void EditBrand(material_brand existed)
        {
            try
            {
                if (existed != null)
                {
                    BLServices.material_brandBL().Edit(new material_brand
                    {
                        categoryid = selectedCat,
                        brandid = existed.brandid,
                        brand_name = brandNameTextBox.Text,
                        brand_description = descripTextBox.Text,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });
                };
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearDetails()
        {
            brandNameTextBox.Clear();
            descripTextBox.Clear();
            selectedBrand = 0;

            //Category blinding
            CategoryCombobox.ItemsSource = null;
            CategoryCombobox.ItemsSource = BLServices.material_categoryBL().GetAll();
        }
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearDetails();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BrandDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (material_brand)BrandDataGrid.SelectedItem;

                if (BLServices.material_brandBL().UsedforItem(model.brandid)) throw new Exception("ไม่สามารถลบ Brand นี้ได้ เพราะมีการบันทึกข้อมูลนี้ใน Material แล้ว");

                BLServices.material_brandBL().Delete(model.brandid);

                ShowBrandDetails();
                ClearDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BrandDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                selectedBrand = 0;
                if (BrandDataGrid.SelectedIndex < 0)
                    return;

                var selected = (material_brand)BrandDataGrid.SelectedItem;
                if (selected != null)
                {
                    selectedBrand = selected.brandid;               
                    brandNameTextBox.Text = selected.brand_name;
                    descripTextBox.Text = selected.brand_description;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CategoryCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedCat = 0;
            selectedBrand = 0;
            var cat = (material_category)CategoryCombobox.SelectedItem;
            if (cat == null) return;

            selectedCat = cat.categoryid;

            BrandDataGrid.ItemsSource = null;
            BrandDataGrid.ItemsSource = BLServices.material_brandBL().GetAll().Where(s => s.categoryid == selectedCat);

            TotalTextBlock.Text = BrandDataGrid.Items.Count.ToString("N0");
        }
    }
}
