﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using OfficeOpenXml;

namespace AGMInventorySystemWPF.Form.Material
{
    /// <summary>
    /// Interaction logic for Transactions.xaml
    /// </summary>
    public partial class Transactions : Page
    {
        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;
        List<sp_Material_TransactionByCropLocation_Result> _tmpRec;
        public Transactions()
        {
            InitializeComponent();

            locationCombobox.ItemsSource = null;
            locationCombobox.ItemsSource = BLServices.locationBL()
                .GetlstwithAll()
                .OrderBy(x => x.name);
        }

        private void locationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BlindingStock();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BlindingStock()
        {
            if (locationCombobox.SelectedIndex < 0) return;
            if (locationCombobox.SelectedValue == null) return;

            stockDataGrid.ItemsSource = null;
            _tmpRec = BLServices.material_transactionBL().GetMatStockByCropLocation(locationCombobox.SelectedValue.ToString())
                .OrderBy(x => x.item_name).ToList();
            stockDataGrid.ItemsSource = _tmpRec;

            TotalTextBlock.Text = stockDataGrid.Items.Count.ToString("N0");
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            BlindingStock();
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            if (stockDataGrid == null) return;

            System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string folderPath = ExportToExcel(folderDlg.SelectedPath, _tmpRec);
                Environment.SpecialFolder root = folderDlg.RootFolder;
                MessageBox.Show("Export ข้อมูลสำเร็จ.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                System.Diagnostics.Process.Start(folderPath);
            }

        }
        public string ExportToExcel(string folderName, List<sp_Material_TransactionByCropLocation_Result> exportList)
        {

            string locateName = Regex.Replace(locationCombobox.Text, @"[^0-9a-zA-Z\._]", "", RegexOptions.Compiled);

            string name = @"" + folderName + "\\StockSheet-" + locateName + "-" + DateTime.Now.Year + DateTime.Now.Month + ".xlsx";

            FileInfo info = new FileInfo(name);
            if (info.Exists) File.Delete(name);

            using (ExcelPackage pc = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pc.Workbook.Worksheets.Add("Material _Stock");
                ws.Cells["B1"].Value = "Location : " + locationCombobox.Text;
                ws.Cells["B2"].Value = "Created Date : " + DateTime.Now.ToString("dd/MM/yyyy");
                ws.Cells["B3"].LoadFromCollection(exportList, true);
                ws.Protection.IsProtected = false;

                ws.Cells["B3"].Value = "Location";
                ws.Cells["C3"].Value = "Brand";
                ws.Cells["D3"].Value = "Item";
                ws.Cells["E3"].Value = "Remain Amount";
                ws.Cells["F3"].Value = "Unit";

                using (ExcelRange col = ws.Cells[4, 5, 5 + exportList.Count, 5]) { col.Style.Numberformat.Format = "#,##0.00"; }

                pc.Save();
            } 
            return @"" + folderName;
                             
        }

        private void PutDataIntoCell(string cell, string value)
        {
            excelRange = excelWorkSheet.Range[cell];
            excelWorkSheet.Range[cell].Value = value;
        }

    }
}
