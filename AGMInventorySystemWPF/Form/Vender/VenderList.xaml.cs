﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Vender
{
    /// <summary>
    /// Interaction logic for VenderList.xaml
    /// </summary>
    public partial class VenderList : Page
    {
        public VenderList()
        {
            InitializeComponent();
            VenderDataGridBinding();
        }

        private void venderDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (venderDataGrid.SelectedIndex < 0)
                    return;

                var model = (vender)venderDataGrid.SelectedItem;
                if (model == null)
                    return;

                VenderEdit window = new VenderEdit(model);
                window.ShowDialog();
                VenderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void VenderDataGridBinding()
        {
            try
            {
                venderDataGrid.ItemsSource = null;
                venderDataGrid.ItemsSource = BLServices.venderBL()
                    .GetAll()
                    .OrderBy(x => x.venderid);

                TotalTextBlock.Text = venderDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                VenderAdd window = new VenderAdd();
                window.ShowDialog();
                VenderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (venderDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (vender)venderDataGrid.SelectedItem;
                if (model == null)
                    return;

                BLServices.venderBL().Delete(model.venderid);
                VenderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
