﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Vender
{
    /// <summary>
    /// Interaction logic for VenderEdit.xaml
    /// </summary>
    public partial class VenderEdit : Window
    {
        vender _vender;

        public VenderEdit(vender model)
        {
            InitializeComponent();

            _vender = new vender();
            _vender = model;

            VenderBinding();
        }

        private void VenderBinding()
        {
            try
            {
                venderidTextBox.Text = _vender.venderid;
                nameTextBox.Text = _vender.name;
                homeidTextBox.Text = _vender.homeid;
                soiTextBox.Text = _vender.soi;
                mooTextBox.Text = _vender.moo;
                roadTextBox.Text = _vender.road;
                tumbolTextBox.Text = _vender.tumbol;
                amphurTextBox.Text = _vender.amphur;
                provinceTextBox.Text = _vender.province;
                postalTextBox.Text = _vender.postal;
                telTextBox.Text = _vender.tel;
                faxTextBox.Text = _vender.fax;
                taxidTextBox.Text = _vender.taxid;
                ssidTextBox.Text = _vender.ssid;
                remarkTextBox.Text = _vender.remark;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (venderidTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก PO no.");

                if (nameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก location");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _vender.name = nameTextBox.Text;
                _vender.homeid = homeidTextBox.Text;
                _vender.soi = soiTextBox.Text;
                _vender.moo = mooTextBox.Text;
                _vender.road = roadTextBox.Text;
                _vender.tumbol = tumbolTextBox.Text;
                _vender.amphur = amphurTextBox.Text;
                _vender.province = provinceTextBox.Text;
                _vender.postal = postalTextBox.Text;
                _vender.tel = telTextBox.Text;
                _vender.fax = faxTextBox.Text;
                _vender.taxid = taxidTextBox.Text;
                _vender.ssid = ssidTextBox.Text;
                _vender.remark = remarkTextBox.Text;
                _vender.modifieddate = DateTime.Now;
                _vender.modifieduser = user_setting.User.username;

                BLServices.venderBL().Edit(_vender);

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            VenderBinding();
        }
    }
}
