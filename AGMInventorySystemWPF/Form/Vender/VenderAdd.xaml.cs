﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Vender
{
    /// <summary>
    /// Interaction logic for VenderAdd.xaml
    /// </summary>
    public partial class VenderAdd : Window
    {
        public VenderAdd()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (venderidTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก PO no.");

                if (nameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก location");

                BLServices.venderBL()
                    .Add(new vender
                    {
                        venderid = venderidTextBox.Text,
                        name = nameTextBox.Text,
                        homeid = homeidTextBox.Text,
                        soi = soiTextBox.Text,
                        moo = mooTextBox.Text,
                        road = roadTextBox.Text,
                        tumbol = tumbolTextBox.Text,
                        amphur = amphurTextBox.Text,
                        province = provinceTextBox.Text,
                        postal = postalTextBox.Text,
                        tel = telTextBox.Text,
                        fax = faxTextBox.Text,
                        taxid = taxidTextBox.Text,
                        ssid = ssidTextBox.Text,
                        remark = remarkTextBox.Text,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                venderidTextBox.Clear();
                nameTextBox.Clear();
                homeidTextBox.Clear();
                soiTextBox.Clear();
                mooTextBox.Clear();
                roadTextBox.Clear();
                tumbolTextBox.Clear();
                amphurTextBox.Clear();
                provinceTextBox.Clear();
                postalTextBox.Clear();
                telTextBox.Clear();
                faxTextBox.Clear();
                taxidTextBox.Clear();
                ssidTextBox.Clear();
                remarkTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

    }
}
