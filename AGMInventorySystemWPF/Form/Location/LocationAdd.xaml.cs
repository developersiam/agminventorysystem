﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Location
{
    /// <summary>
    /// Interaction logic for LocationAdd.xaml
    /// </summary>
    public partial class LocationAdd : Page
    {
        location _location;
        public LocationAdd()
        {
            InitializeComponent();

            _location = new location();

            DataGridBinding();
        }

        private void DataGridBinding()
        {
            try
            {
                locationDataGrid.ItemsSource = null;
                locationDataGrid.ItemsSource = BLServices.locationBL()
                    .GetAll()
                    .OrderBy(x => x.locationid);

                TotalTextBlock.Text = locationDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (locationDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (location)locationDataGrid.SelectedItem;
                BLServices.locationBL().Delete(model.locationid);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (locationDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                locationidTextBox.IsEnabled = false;

                _location = (location)locationDataGrid.SelectedItem;

                locationidTextBox.Text = _location.locationid;
                nameTextBox.Text = _location.name;
                descriptionTextBox.Text = _location.description;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (locationidTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก location id");

                if (nameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก name");

                BLServices.locationBL().Add(new location
                {
                    locationid = locationidTextBox.Text,
                    name = nameTextBox.Text,
                    description = descriptionTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            locationidTextBox.Clear();
            nameTextBox.Clear();
            descriptionTextBox.Clear();

            AddButton.IsEnabled = true;
            EditButton.IsEnabled = false;

            locationidTextBox.IsEnabled = true;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (nameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก name");

                if (descriptionTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก description");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _location.name = nameTextBox.Text;
                _location.modifieduser = user_setting.User.username;
                _location.modifieddate = DateTime.Now;

                BLServices.locationBL().Edit(_location);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
