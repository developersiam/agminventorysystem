﻿using AGMInventorySystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System.IO;

namespace AGMInventorySystemWPF.Form.Movement
{
    /// <summary>
    /// Interaction logic for IssueDetails.xaml
    /// </summary>
    public partial class IssueDetails : Window
    {
        short _crop;
        string _issue_no;
        string _locationid;
        int _issuetype = 0;
        List<sp_Material_TransactionByCropLocation_Result> _tmpRec;
        List<issue_details> _issuedetails;

        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;

        public IssueDetails(short crop, string issue_no)
        {
            _crop = crop;
            _issue_no = issue_no;
            _tmpRec = new List<sp_Material_TransactionByCropLocation_Result>();
            _issuedetails = new List<issue_details>();

            InitializeComponent();

            cropTextBox.Text = _crop.ToString();
            locationCombobox.ItemsSource = null;
            locationCombobox.ItemsSource = BLServices.locationBL().GetAll();

            supplierCombobox.ItemsSource = null;
            supplierCombobox.ItemsSource = BLServices.supplierBL().GetAll();

            issueTypeCombobox.ItemsSource = null;
            issueTypeCombobox.ItemsSource = BLServices.issue_typeBL().GetAll();

            issueDatePicker.SelectedDate = DateTime.Now;

            //------------ Edit issue details ------------
            if (!String.IsNullOrEmpty(_issue_no)) GetIssueDetails();
        }

        private void InitialBlinding()
        {
            supplierCombobox.ItemsSource = null;
            supplierCombobox.ItemsSource = BLServices.supplierBL().GetAll();
        }

        private void locationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (locationCombobox.SelectedIndex < 0) return;
            if (locationCombobox.SelectedValue == null) return;
            //Show remaining stock by location
            BlindingStock();
            stockText.Text = "All items in " + locationCombobox.Text + ". Please be careful before add Items for issue.";
        }

        private void issueTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (issueTypeCombobox.SelectedValue == null) return;
            //4 Issue Type 
            ValidateIssue();
        }

        private void BlindingStock()
        {
            string curr_location;

            //Blinding Data to combobox
            itemCombobox.ItemsSource = null;
            itemCombobox.ItemsSource = BLServices.material_itemBL().GetAll();

            curr_location = locationCombobox.SelectedValue == null ? _locationid : locationCombobox.SelectedValue.ToString();
            stockDataGrid.ItemsSource = null;
            _tmpRec = BLServices.material_transactionBL().GetMatStockByCropLocation(curr_location)
                .OrderBy(x => x.item_name).ToList();
            stockDataGrid.ItemsSource = _tmpRec;

            TotalTextBlock.Text = stockDataGrid.Items.Count.ToString("N0");
        }

        private void ValidateIssue()
        {
            int curr_issuetype;

            curr_issuetype = issueTypeCombobox.SelectedValue == null ? _issuetype : Convert.ToInt16(issueTypeCombobox.SelectedValue);
            if (curr_issuetype == 1) //ชำรุด
            {
                pathTextBox.IsReadOnly = false;
                PathButton.IsEnabled = true;
                supplierCombobox.IsEnabled = false;
                internalCombobox.IsEnabled = false;
                PrintButton.IsEnabled = false;
                InitialBlinding();

            }
            else if (curr_issuetype == 2) //สูญหาย
            {
                pathTextBox.IsReadOnly = true;
                PathButton.IsEnabled = false;
                pathTextBox.Text = "";
                supplierCombobox.IsEnabled = false;
                internalCombobox.IsEnabled = false;
                PrintButton.IsEnabled = false;
                InitialBlinding();
            }
            else if (curr_issuetype == 3) //จ่ายให้ supplier
            {
                pathTextBox.IsReadOnly = true;
                PathButton.IsEnabled = false;
                pathTextBox.Text = "";
                supplierCombobox.IsEnabled = true;
                internalCombobox.IsEnabled = false;
                PrintButton.IsEnabled = true;
            }
            else if (curr_issuetype == 4)//ใช้ภายในแผนก
            {
                pathTextBox.IsReadOnly = true;
                PathButton.IsEnabled = false;
                pathTextBox.Text = "";
                supplierCombobox.IsEnabled = false;
                internalCombobox.IsEnabled = true;
                PrintButton.IsEnabled = false;
                InitialBlinding();
            }
            else //อื่นๆ
            {
                pathTextBox.IsReadOnly = true;
                PathButton.IsEnabled = false;
                pathTextBox.Text = "";
                supplierCombobox.IsEnabled = false;
                internalCombobox.IsEnabled = false;
                PrintButton.IsEnabled = false;
                InitialBlinding();
            }
        }

        private void GetIssueDetails()
        {
            try
            {
                issuenoTextBox.Text = _issue_no;

                var existed = BLServices.issue_masterBL().GetSingle(_issue_no);
                if (existed != null)
                {
                    _locationid = existed.locationid;
                    _issuetype = existed.issuetype;

                    cropTextBox.Text = existed.crop.ToString();
                    //movementstatusCombobox.SelectedValue = existed.receivedstatus;
                    locationCombobox.SelectedValue = existed.locationid;
                    issueDatePicker.SelectedDate = existed.issuedate;
                    issueTypeCombobox.SelectedValue = existed.issuetype;
                    pathTextBox.Text = existed.issuepicturepath;
                    supplierCombobox.SelectedValue = existed.issuesupplier == 0 ? null : existed.issuesupplier;
                    internalCombobox.Text = existed.issueInternalreason == "" ? null : existed.issueInternalreason.ToString();
                    remarkTextBox.Text = existed.remark;

                    //Blinding to item stock
                    BlindingStock();
                    //Disable all tools
                    DisableAll();
                    //Blinding to datagrid
                    GetItemDetails();
                    ValidateIssue();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DisableAll()
        {
            issueDatePicker.IsEnabled = false;
            locationCombobox.IsEnabled = false;
            issueTypeCombobox.IsEnabled = false;
            internalCombobox.IsEnabled = false;

            AddItemButton.IsEnabled = true;
        }

        private void stockDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (stockDataGrid.SelectedIndex < 0)
                    return;

                var selected = (sp_Material_TransactionByCropLocation_Result)stockDataGrid.SelectedItem;
                if (selected != null)
                {
                    itemCombobox.SelectedValue = selected.itemid;
                    itemCombobox.SelectedItem = selected.item_name;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PathButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Filter = "picture files (*.jpg)|*.jpg|All files (*.*)|*.*";
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    pathTextBox.Text = fileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (issueDatePicker.Text == "") throw new ArgumentException("Please select Issue Date before add");
                if (locationCombobox.SelectedIndex < 0) throw new ArgumentException("Please select location for issue");
                if (issueTypeCombobox.SelectedIndex < 0) throw new ArgumentException("Please select Type for issue");
                if (Convert.ToInt16(issueTypeCombobox.SelectedValue) == 1 && string.IsNullOrEmpty(pathTextBox.Text)) throw new ArgumentException("Please check your picture path");
                if (Convert.ToInt16(issueTypeCombobox.SelectedValue) == 3 && supplierCombobox.SelectedIndex < 0) throw new ArgumentException("Please select supplier for issue"); //สูญหาย
                if (Convert.ToInt16(issueTypeCombobox.SelectedValue) == 4 && internalCombobox.SelectedIndex < 0) throw new ArgumentException("Please select internal issue");//สูญหาย

                if (String.IsNullOrEmpty(issuenoTextBox.Text))
                {
                    //for master
                    AddIssueMaster();

                    //for details
                    if (IssueDataGrid.Items.Count > 0)
                    {
                        //AddIssueDetails();
                    }
                }
                else
                {
                    BLServices.issue_masterBL().Edit(new issue_master
                    {
                        issueno = issuenoTextBox.Text,
                        crop = _crop,
                        issuepicturepath = pathTextBox.Text,
                        issuesupplier = supplierCombobox.SelectedValue == null ? 0 : Convert.ToInt16(supplierCombobox.SelectedValue),
                        issueInternalreason = string.IsNullOrEmpty(internalCombobox.Text) ? "" : internalCombobox.Text,
                        remark = remarkTextBox.Text,
                        requesteddate = DateTime.Now,
                        requesteduser = user_setting.User.username
                    });

                }

                EnableItems();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private void AddIssueDetails()
        {
            BLServices.issue_detailsBL().Add(new issue_details
            {
                issueno = _issue_no,
                itemid = Convert.ToInt32(itemCombobox.SelectedValue),
                issuedetailsid = Guid.NewGuid(),
                issue_quantity = Convert.ToInt32(qtyTextBox.Text),
                remark = remarkTextBox.Text,
                modifieddate = DateTime.Now,
                modifieduser = user_setting.User.username
            });

            //Remove from transaction by location
            RemoveTransactionItem(Convert.ToInt32(itemCombobox.SelectedValue.ToString()), Convert.ToInt32(qtyTextBox.Text));
        }

        private void AddIssueMaster()
        {
            _issue_no = BLServices.issue_masterBL()
                        .Add(new issue_master
                        {
                            issueno = "",
                            issuedate = Convert.ToDateTime(issueDatePicker.SelectedDate),
                            crop = _crop,
                            locationid = locationCombobox.SelectedValue.ToString(),
                            issuetype = Convert.ToInt16(issueTypeCombobox.SelectedValue),
                            issuepicturepath = pathTextBox.Text,
                            issuesupplier = supplierCombobox.SelectedValue == null ? 0 : Convert.ToInt16(supplierCombobox.SelectedValue),
                            issueInternalreason = internalCombobox.SelectedValue == null ? "" : internalCombobox.Text,
                            remark = remarkTextBox.Text,
                            requesteddate = DateTime.Now,
                            requesteduser = user_setting.User.username
                        });

            issuenoTextBox.Text = _issue_no;
        }

        private void EnableItems()
        {
            //disable details part
            issuenoTextBox.Text = _issue_no;
            itemCombobox.ItemsSource = null;
            itemCombobox.ItemsSource = BLServices.material_itemBL().GetAll();
            locationCombobox.IsEnabled = false;

            AddItemButton.IsEnabled = true;
            qtyTextBox.IsEnabled = true;
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            if (itemCombobox.SelectedIndex < 0) throw new ArgumentException("Please select Item for issue");
            if (qtyTextBox.Text == "") throw new ArgumentException("โปรดระบุ quantity");
            if (Helper.RegularExpressionHelper.IsNumericCharacter(qtyTextBox.Text) == false) throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

            //Check this items and location in Material_transaction before ADD
            if (CheckAvailableItem(Convert.ToInt16(itemCombobox.SelectedValue.ToString()))) AddIssueDetails();
            else MessageBox.Show("Item นี้ใน Stock ไม่เพียงพอต่อการตัดจ่าย กรุณาเช็คจำนวนการตัดจ่าย", "Information", MessageBoxButton.OK, MessageBoxImage.Warning);


            GetItemDetails();
        }

        private bool CheckAvailableItem(int itemid)
        {
            var available = false;

            var stock = BLServices.material_transactionBL().GetMatStockByCropLocation(locationCombobox.SelectedValue.ToString())
                        .Where(i => i.itemid == itemid);
            if (stock.Count() > 0)
            {
                foreach (var i in stock)
                {
                    if (i.RunningTotal >= Convert.ToInt32(qtyTextBox.Text)) available = true;
                }
            }
            return available;
        }

        private void RemoveTransactionItem(int itemid, int itemQty)
        {

            var itemdetail = BLServices.material_itemBL().GetSingle(itemid);
            if (itemdetail == null) return;

            var itemtransaction = BLServices.material_transactionBL().GetStockByItemid(itemdetail.itemid);
            if (itemtransaction == null) return;

            BLServices.material_transactionBL().Out(new material_transaction
            {
                crop = Convert.ToInt16(cropTextBox.Text),
                refid = _issue_no,
                itemid = itemdetail.itemid,
                unitid = itemdetail.unitid,
                unitprice = (decimal)itemtransaction[0].Average_unitPrice,
                quantity = itemQty,
                locationid = locationCombobox.SelectedValue.ToString(),
                transactiontype = "Out",
                modifieddate = DateTime.Now,
                modifieduser = user_setting.User.username
            });
        }

        private void GetItemDetails()
        {
            //bliding selected item to data grid
            if (String.IsNullOrEmpty(issuenoTextBox.Text)) return;

            IssueDataGrid.ItemsSource = null;
            _issuedetails = BLServices.issue_detailsBL().GetIssueDetails(_issue_no);
            if (_issuedetails != null || _issuedetails.Count() > 0)
            {
                var issuelst = Helper.issue_master_helper.GetIssueDetailsByIssueNo(_issue_no).OrderByDescending(x => x.issue_quantity);

                IssueDataGrid.ItemsSource = issuelst;
                TotalTextBlock2.Text = IssueDataGrid.Items.Count.ToString("N0");
            }
            //If user doesn't add any items yet, open add item function 
            else EnableItems();
            //Binding item by location
            BlindingStock();
        }

        private void pathTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb != null) tb.SelectAll();
        }

        private void pathTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb != null) tb.SelectAll();
        }

        private void issueDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (issueDatePicker.SelectedDate > DateTime.Now)
            {
                MessageBox.Show("ไม่สามารถตัดจ่ายล่วงหน้าได้, กรุณาเลือกวันที่ใหม่", "Information", MessageBoxButton.OK, MessageBoxImage.Warning);
                issueDatePicker.SelectedDate = DateTime.Now;
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            PrintIssue();
        }

        private void PrintIssue()
        {
            try
            {
                string t;
                DateTime issuedate;
                excelWorkBook = excelApplication.Workbooks.Open(@"C:\AGMInventory\Seed Distribute form.xlsx");
                excelWorkSheet = excelWorkBook.ActiveSheet;
                excelWorkSheet.PageSetup.Zoom = false;
                excelWorkSheet.PageSetup.FitToPagesWide = 1;
                excelApplication.Visible = true;

                //วันที่จ่ายเมล็ดพันธุ์(Date distribution) :   
                issuedate = Convert.ToDateTime(issueDatePicker.SelectedDate) != null ? Convert.ToDateTime(issueDatePicker.SelectedDate) : DateTime.Now;
                PutDataIntoCell("A7", "วันที่จ่ายเมล็ดพันธุ์(Date distribution) : " + issuedate.ToString("dd/MMM/yyyy"));
                PutDataIntoCell("G10", supplierCombobox.Text);
                //item.material_item.material_brand.brand_name
                // เวอร์จิเนีย (FCV) 
                // เบอร์เล่ย์ (Burley) 

                //Get brand name
                //t = _issuedetails.FirstOrDefault().material_item.material_brand.brand_name;
                //if (t.Contains("FCV")) PutDataIntoCell("A6", "เวอร์จิเนีย (FCV)"); else PutDataIntoCell("A6", "เบอร์เล่ย์ (Burley)");


                int row = 10;
                int i = 1;
                foreach (var item in _issuedetails)
                {
                    PutDataIntoCell("A" + row, i.ToString());
                    PutDataIntoCell("B" + row, item.material_item.item_name.ToString());
                    PutDataIntoCell("C" + row, issueDatePicker.SelectedDate.ToString());
                    PutDataIntoCell("D" + row, item.issue_quantity.ToString());
                    PutDataIntoCell("E" + row, item.material_item.lotno.ToString());
                    row++;
                    i++;
                }
                excelApplication.Quit();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PutDataIntoCell(string cell, string value)
        {
            excelRange = excelWorkSheet.Range[cell];
            excelWorkSheet.Range[cell].Value = value;
        }

        private void SearchItemTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var resultList = new List<sp_Material_TransactionByCropLocation_Result>();
                if (string.IsNullOrEmpty(SearchItemTextBox.Text))
                    resultList = _tmpRec
                        .OrderBy(x => x.item_name)
                        .ToList();

                resultList = _tmpRec
                    .Where(x => x.item_name.Contains(SearchItemTextBox.Text))
                    .OrderBy(x => x.item_name)
                    .ToList();

                stockDataGrid.ItemsSource = null;
                stockDataGrid.ItemsSource = resultList;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SearchItemTextBox.Clear();
                stockDataGrid.ItemsSource = null;
                stockDataGrid.ItemsSource = _tmpRec
                    .OrderBy(x=>x.item_name)
                    .ToList();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}

