﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Movement
{
    /// <summary>
    /// Interaction logic for Supplier.xaml
    /// </summary>
    public partial class Supplier : Page
    {
        private int _supplierid = 0;
        public Supplier()
        {
            InitializeComponent();
            BlindingGrid();
        }

        private void BlindingGrid()
        {
            try
            {
                issueDataGrid.ItemsSource = null;
                issueDataGrid.ItemsSource = BLServices.supplierBL()
                    .GetAll()
                    .OrderBy(x => x.supplier_name);

                TotalTextBlock.Text = issueDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {          
            try
            {
                var existed = new supplier() ;
                if (string.IsNullOrEmpty(supplierTextBox.Text)) throw new ArgumentException("โปรดกรอกชื่อ Supplier");

                if (_supplierid == 0) existed = BLServices.supplierBL().GetSingleByName(supplierTextBox.Text);
                else existed = BLServices.supplierBL().GetSingle(_supplierid);

                if (existed == null)
                {
                    BLServices.supplierBL().Add(new supplier
                    {
                        supplier_name = supplierTextBox.Text,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });
                }

                BlindingGrid();
                supplierTextBox.Text = "";
                _supplierid = 0;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        private void issueDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                _supplierid = 0;
                if (issueDataGrid.SelectedIndex < 0)
                    return;

                //AddButton.IsEnabled = false;
                var selected = (supplier)issueDataGrid.SelectedItem;
                if (selected != null)
                {
                    _supplierid = selected.supplierid;
                    supplierTextBox.Text = selected.supplier_name;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _supplierid = 0;
                if (issueDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (supplier)issueDataGrid.SelectedItem;
                _supplierid = model.supplierid;
                BLServices.supplierBL().Delete(_supplierid);

                BlindingGrid();
                _supplierid = 0;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(supplierTextBox.Text)) throw new ArgumentException("โปรดกรอกชื่อ Supplier");
            if (_supplierid == 0) throw new ArgumentException("กรุณา double click เลือก Supplier จากในตาราง ก่อน Edit");

            var existed = BLServices.supplierBL().GetSingle(_supplierid);
            if (existed != null)
            {
                BLServices.supplierBL().Edit(new supplier
                {
                    supplierid = _supplierid,
                    supplier_name = supplierTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });
            }

            BlindingGrid();
            supplierTextBox.Text = "";
            _supplierid = 0;
        }
    }
}
