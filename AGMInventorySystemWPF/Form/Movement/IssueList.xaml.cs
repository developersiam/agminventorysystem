﻿using AGMInventorySystemBL;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AGMInventorySystemEntities;

namespace AGMInventorySystemWPF.Form.Movement
{
    /// <summary>
    /// Interaction logic for IssueList.xaml
    /// </summary>
    public partial class IssueList : Page
    {
        public IssueList()
        {
            InitializeComponent();

            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = DateTime.Now.Year;

            LocationCombobox.ItemsSource = null;
            LocationCombobox.ItemsSource = BLServices.locationBL()
                .GetlstwithAll()
                .OrderBy(x => x.name);
            LocationCombobox.SelectedIndex = -1;

            BlidingDatagrid();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (CropCombobox.SelectedIndex < 0)
                throw new ArgumentException("Please select crop for Create new movement");

            IssueDetails window = new IssueDetails(Convert.ToInt16(CropCombobox.SelectedValue),"");
            window.ShowDialog();
            BlidingDatagrid();
        }

        private void BlidingDatagrid()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (LocationCombobox.SelectedIndex < 0)
                    return;

                issueDataGrid.ItemsSource = null;
                var location = (location)LocationCombobox.SelectedItem;
                var issuelst = Helper.issue_master_helper.GetissueListByCrop(Convert.ToInt16(CropCombobox.SelectedValue));

                if (location.name == "All")
                    issueDataGrid.ItemsSource = issuelst.OrderByDescending(x => x.issueno);
                else
                    issueDataGrid.ItemsSource = issuelst.Where(x => x.locationid == location.locationid)
                                                .OrderByDescending(x => x.issueno);


                TotalTextBlock.Text = issueDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LocationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BlidingDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }
        private void IssueDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (issueDataGrid.SelectedIndex < 0)
                    return;

                var model = (issue_master_vm)issueDataGrid.SelectedItem;

                IssueDetails window = new IssueDetails(Convert.ToInt16(CropCombobox.SelectedValue), model.issueno);
                window.ShowDialog();
                //MMDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
