﻿using AGMInventorySystemBL;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace AGMInventorySystemWPF.Form.Movement
{
    public partial class ReturnList : Page
    {
        public object BLservice { get; private set; }

        public ReturnList()
        {
            InitializeComponent();

            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = DateTime.Now.Year;

            BlidingDatagrid();
        }

        private void BlidingDatagrid()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                ReturnDataGrid.ItemsSource = null;
                //var location = (location)LocationCombobox.SelectedItem;
                var issuelst = Helper.return_master_helper.GetreturnListByCrop(Convert.ToInt16(CropCombobox.SelectedValue));

                ReturnDataGrid.ItemsSource = issuelst.OrderByDescending(x => x.returnno);

                //if (location.name == "All")
                //    issueDataGrid.ItemsSource = issuelst.OrderByDescending(x => x.issueno);
                //else
                //    issueDataGrid.ItemsSource = issuelst.Where(x => x.locationid == location.locationid)
                //                                .OrderByDescending(x => x.issueno);


                TotalTextBlock.Text = ReturnDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("Please select crop for Create new movement");

                ReturnDetails window = new ReturnDetails(Convert.ToInt16(CropCombobox.SelectedValue), "");
                window.ShowDialog();
                BlidingDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReturnDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReturnDataGrid.SelectedIndex < 0)
                    return;

                var model = (return_master_vm)ReturnDataGrid.SelectedItem;

                ReturnDetails window = new ReturnDetails(Convert.ToInt16(CropCombobox.SelectedValue), model.returnno);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReturnDataGrid.SelectedIndex < 0)
                    return;

                var model = (return_master_vm)ReturnDataGrid.SelectedItem;
                var rt_details = BLServices.return_detailsBL().GetRTDetails(model.returnno);
                if (rt_details == null || rt_details.Count() == 0) BLServices.return_masterBL().Delete(model);
                else MessageBox.Show("Return No. นี้ มีการรับคืน Item แล้ว ไม่สามารถลบได้", "Information", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
