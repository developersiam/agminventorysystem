﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;

namespace AGMInventorySystemWPF.Form.Movement
{
    /// <summary>
    /// Interaction logic for MovementDetails.xaml
    /// </summary>
    public partial class MovementDetails : Window
    {
        short _crop;
        int selectedCat;
        string _movementNo;
        public MovementDetails(short crop, string movement_no)
        {
            try
            {
                _crop = crop;
                _movementNo = movement_no;

                InitializeComponent();

                fromlocationCombobox.ItemsSource = null;
                fromlocationCombobox.ItemsSource = BLServices.locationBL()
                    .GetAll()
                    .OrderBy(x => x.name);

                tolocationCombobox.ItemsSource = null;
                tolocationCombobox.ItemsSource = BLServices.locationBL()
                    .GetAll()
                    .OrderBy(x => x.name);

                movementstatusCombobox.ItemsSource = null;
                movementstatusCombobox.ItemsSource = user_setting.TaskStatus;

                if (!String.IsNullOrEmpty(_movementNo)) GetMovementDetails();
                else
                {
                    Blindingfromlocation();

                    movementstatusCombobox.SelectedIndex = 0;

                    mmDatePicker.SelectedDate = DateTime.Now;
                    cropTextBox.Text = _crop.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void Blindingfromlocation()
        {
            var locatedLst = BLServices.user_locationBL().GetByUsername(user_setting.User.username);
            var fromList = new List<location>();
            foreach (var i in locatedLst)
            {
                var vm = new location
                {
                    name = i.location.name,
                    locationid = i.locationid,
                };
                fromList.Add(vm);
            };

            fromlocationCombobox.ItemsSource = null;
            fromlocationCombobox.ItemsSource = fromList;
        }
        private void GetMovementDetails()
        {
            try
            {
                movementnoTextBox.Text = _movementNo;
                var existed = BLServices.movement_masterBL().GetSingle(_movementNo);
                if (existed != null)
                {
                    cropTextBox.Text = existed.crop.ToString();
                    movementstatusCombobox.SelectedValue = existed.receivedstatus;
                    fromlocationCombobox.SelectedValue = existed.fromlocationid;
                    tolocationCombobox.SelectedValue = existed.tolocationid;
                    mmDatePicker.SelectedDate = existed.movementdate;
                    remarkTextBox.Text = existed.remark;

                    if (existed.receivedstatus == "Complete") DisableAll();
                    else EnableItems();

                    //Blinding to datagrid
                    GetItemDetails();
                    //Check Authen
                    CheckAuthen(existed);
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DisableAll()
        {
            mmDatePicker.IsEnabled = false;
            fromlocationCombobox.IsEnabled = false;
            tolocationCombobox.IsEnabled = false;
            movementstatusCombobox.IsEnabled = false;
            remarkTextBox.IsReadOnly = true;

            createButton.IsEnabled = false;
            AddItemButton.IsEnabled = false;
        }
        private void EnableItems()
        {
            movementnoTextBox.Text = _movementNo;
            categoryCombobox.ItemsSource = null;
            categoryCombobox.ItemsSource = BLServices.material_categoryBL().GetAll().OrderBy(x => x.category_name);

            AddItemButton.IsEnabled = true;
            approveddateCheckBox.IsEnabled = true;
        }

        private void CheckAuthen(movement_master existed)
        {
            try
            {
                //For Approver
                approveddateCheckBox.IsEnabled = user_setting.UserRoles.Where(x => x.rolename == "Approver1").Count() > 0 ? true : false;
                if (existed.approveddate != null)
                {
                    approveddateTextBox.Text = existed.approveddate.ToString();
                    approveddateCheckBox.IsChecked = true;
                }
                else
                {
                    approveddateTextBox.Clear();
                    approveddateCheckBox.IsChecked = false;
                }

                // Receiver
                var locatedLst = BLServices.user_locationBL().GetByUsername(user_setting.User.username);
                if (existed.fromlocationid != locatedLst.FirstOrDefault().locationid)
                {
                    createButton.IsEnabled = false;
                    receiviedButton.IsEnabled = true;
                }
                else
                {
                    createButton.IsEnabled = true;
                    receiviedButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        private void GetItemDetails()
        {
            //bliding selected item to data grid
            if (String.IsNullOrEmpty(movementnoTextBox.Text)) return;

            MoveDataGrid.ItemsSource = null;
            var details = Helper.movement_master_helper.GetMovementDetailsByMovementNo(_movementNo).OrderByDescending(x => x.quantity);
            if (details.Count() > 0)
            {
                MoveDataGrid.ItemsSource = details;
                TotalTextBlock.Text = MoveDataGrid.Items.Count.ToString("N0");
            }
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            //Add to movement master
            if (mmDatePicker.Text == "") throw new ArgumentException("Please select Movement Date before add");
            if (fromlocationCombobox.SelectedIndex < 0) throw new ArgumentException("Please select from location before add");
            if (tolocationCombobox.SelectedIndex < 0) throw new ArgumentException("Please select to location before add");
            if (movementstatusCombobox.SelectedIndex < 0) throw new ArgumentException("Please select movement status before add");

            if (String.IsNullOrEmpty(movementnoTextBox.Text))
                _movementNo = BLServices.movement_masterBL()
                        .Add(new movement_master
                        {
                            movementno = "",
                            movementdate = Convert.ToDateTime(mmDatePicker.SelectedDate),
                            crop = _crop,
                            fromlocationid = fromlocationCombobox.SelectedValue.ToString(),
                            tolocationid = tolocationCombobox.SelectedValue.ToString(),
                            receivedstatus = movementstatusCombobox.SelectedValue.ToString(),
                            remark = remarkTextBox.Text,
                            createdate = DateTime.Now,
                            createduser = user_setting.User.username,
                            modifieddate = DateTime.Now,
                            modifieduser = user_setting.User.username

                        });
            else
                BLServices.movement_masterBL().Edit(new movement_master
                {
                    movementno = movementnoTextBox.Text,
                    movementdate = Convert.ToDateTime(mmDatePicker.SelectedDate),
                    crop = _crop,
                    fromlocationid = fromlocationCombobox.SelectedValue.ToString(),
                    tolocationid = tolocationCombobox.SelectedValue.ToString(),
                    receivedstatus = movementstatusCombobox.SelectedValue.ToString(),
                    remark = remarkTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });

            EnableItems();
        }

        private void categoryCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedCat = 0;
            var cat = (material_category)categoryCombobox.SelectedItem;
            if (cat == null) return;

            selectedCat = cat.categoryid;

            itemCombobox.ItemsSource = null;
            itemCombobox.ItemsSource = BLServices.material_itemBL().GetAll().Where(s => s.categoryid == selectedCat);

        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            if (itemCombobox.SelectedIndex < 0)
                throw new ArgumentException("โปรดระบุ item");

            if (quantityTextBox.Text == "")
                throw new ArgumentException("โปรดระบุ quantity");

            if (Helper.RegularExpressionHelper.IsNumericCharacter(quantityTextBox.Text) == false)
                throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

            //Check this items and location in Material_transaction before ADD
            if (CheckAvailableItem(Convert.ToInt16(itemCombobox.SelectedValue.ToString())))
            {
                BLServices.movement_detailsBL()
                    .Add(new movement_details
                    {
                        movementno = _movementNo,
                        itemid = Convert.ToInt32(itemCombobox.SelectedValue.ToString()),
                        quantity = Convert.ToInt32(quantityTextBox.Text),
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });

                //Remove from transaction by location
                RemoveTransactionItem(Convert.ToInt32(itemCombobox.SelectedValue.ToString()), Convert.ToInt32(quantityTextBox.Text));
            }
            else MessageBox.Show("Item นี้ใน Stock ไม่เพียงพอสำหรับการ Movement", "Information", MessageBoxButton.OK, MessageBoxImage.Warning);


            GetItemDetails();

        }

        private bool CheckAvailableItem(int itemid)
        {
            var available = false;

            var stock = BLServices.material_transactionBL()
                .GetMatStockByCropLocation(fromlocationCombobox.SelectedValue.ToString())
                .Where(i => i.itemid == itemid)
                .ToList();

            if (stock.Count() > 0)
            {
                foreach (var i in stock)
                {
                    if (i.RunningTotal >= Convert.ToInt32(quantityTextBox.Text))
                        available = true;
                }
            }

            return available;
        }

        private void approveddateCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (movementnoTextBox.Text == "") throw new ArgumentException("Please fill Movement no. before approve");
            if (user_setting.UserRoles.Where(x => x.rolename == "Approver1").Count() <= 0 || user_setting.UserRoles.Where(x => x.rolename == "Approver2").Count() <= 0) throw new ArgumentException("user account ของท่านไม่สามารถ approve Movement ได้");

            var existed = BLServices.movement_masterBL().GetSingle(_movementNo);
            if (existed != null)
            {
                if (existed.approveddate == null)
                {
                    BLServices.movement_masterBL().Approve(_movementNo, user_setting.User.username);
                    MessageBox.Show("ทำการ Approve Movement no. " + _movementNo + " เรียบร้อยแล้ว", "Completed", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    BLServices.movement_masterBL().CancelApprove(_movementNo);
                    MessageBox.Show("ยกเลิกการ Approve Movement no. " + _movementNo + " เรียบร้อยแล้ว", "Completed", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                GetMovementDetails();
            }
        }

        private void receiviedButton_Click(object sender, RoutedEventArgs e)
        {
            if (movementnoTextBox.Text == "") throw new ArgumentException("Please fill Movement no. before approve");
            //Update received info to Movement master 
            var existed = BLServices.movement_masterBL().GetSingle(_movementNo);
            if (existed != null)
            {
                if (existed.receiveddate != null) throw new ArgumentException("Movement no. " + _movementNo + " นี้ ทำการ Received เรียบร้อยแล้ว");

                if (existed.approveddate != null && existed.receiveddate == null)
                {
                    BLServices.movement_masterBL().Received(_movementNo, user_setting.User.username);

                    //Add transaction by location
                    AddTransactionItem(_movementNo);

                    MessageBox.Show("ทำการ Received Movement no. " + _movementNo + " เรียบร้อยแล้ว", "Completed", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else throw new ArgumentException("Movement no. " + _movementNo + " นี้ ยังไม่ได้ทำการ approve จึงไม่สามารถ Receive ได้");

            }

        }

        private void AddTransactionItem(string mmNo)
        {
            material_item item = new material_item();
            var itemLst = BLServices.movement_detailsBL().GetByMovementNo(mmNo);
            if (itemLst.Count() > 0)
            {
                foreach (var i in itemLst)
                {
                    item = BLServices.material_itemBL().GetSingle(i.itemid);
                    if (item != null)
                    {
                        //We need to use the same average price while movment this item
                        //find Transaction
                        var lastTransaction = BLServices.material_transactionBL().GetTransactionlistByRefId(movementnoTextBox.Text).Where(s => s.crop == Convert.ToInt16(cropTextBox.Text)
                                               && s.itemid == item.itemid && s.locationid == fromlocationCombobox.SelectedValue.ToString() && s.transactiontype == "Out" && s.quantity == i.quantity * -1).FirstOrDefault();
                        if (lastTransaction == null) return;

                        BLServices.material_transactionBL().In(new material_transaction
                        {
                            crop = Convert.ToInt16(cropTextBox.Text),
                            refid = mmNo,
                            itemid = item.itemid,
                            unitid = lastTransaction.unitid,
                            unitprice = (decimal)lastTransaction.unitprice,
                            quantity = i.quantity,
                            locationid = tolocationCombobox.SelectedValue.ToString(),
                            transactiontype = "In",
                            modifieddate = DateTime.Now,
                            modifieduser = user_setting.User.username
                        });
                    }
                }
            }
        }
        private void RemoveTransactionItem(int itemid, int itemQty)
        {

            var itemdetail = BLServices.material_itemBL().GetSingle(itemid);
            if (itemdetail == null) return;

            var itemtransaction = BLServices.material_transactionBL().GetStockByItemid(itemdetail.itemid);
            if (itemtransaction == null) return;

            BLServices.material_transactionBL().Out(new material_transaction
            {
                crop = Convert.ToInt16(cropTextBox.Text),
                refid = _movementNo,
                itemid = itemdetail.itemid,
                unitid = itemdetail.unitid,
                unitprice = (decimal)itemtransaction[0].Average_unitPrice,
                quantity = itemQty,
                locationid = fromlocationCombobox.SelectedValue.ToString(),
                transactiontype = "Out",
                modifieddate = DateTime.Now,
                modifieduser = user_setting.User.username
            });
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (MoveDataGrid.SelectedIndex < 0)
                return;

            var model = (movement_details_vm)MoveDataGrid.SelectedItem;
            if (model == null)
                return;

            //Check Approved in movement master
            var chekApproved = BLServices.movement_masterBL().GetSingle(movementnoTextBox.Text);
            if (chekApproved.approveddate != null) throw new ArgumentException("Movement นี้ Approved เรียบร้อยแล้ว จึงไม่สามารถแก้ไข Item ได้");

            var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (msg == MessageBoxResult.No)
                return;

            BLServices.movement_detailsBL().Delete(model.movementdetailsno);

            //Get Item back to fromlocation
            GetitemBacktoFromlocation(model.itemid, model.quantity);
            GetItemDetails();
        }

        private void GetitemBacktoFromlocation(int itemid, int quantity)
        {
            //find Transaction
            var lastTransaction = BLServices.material_transactionBL().GetTransactionlistByRefId(movementnoTextBox.Text).Where(s => s.crop == Convert.ToInt16(cropTextBox.Text)
                                   && s.itemid == itemid && s.locationid == fromlocationCombobox.SelectedValue.ToString() && s.transactiontype == "Out" && s.quantity == quantity * -1).FirstOrDefault();
            if (lastTransaction == null) return;


            var itemdetail = BLServices.material_itemBL().GetSingle(itemid);
            if (itemdetail == null) return;

            BLServices.material_transactionBL().In(new material_transaction
            {
                crop = Convert.ToInt16(cropTextBox.Text),
                refid = movementnoTextBox.Text,
                itemid = itemdetail.itemid,
                unitid = itemdetail.unitid,
                unitprice = lastTransaction.unitprice,
                quantity = lastTransaction.quantity * -1,
                locationid = fromlocationCombobox.SelectedValue.ToString(),
                transactiontype = "In",
                modifieddate = DateTime.Now,
                modifieduser = user_setting.User.username
            });
        }
    }
}
