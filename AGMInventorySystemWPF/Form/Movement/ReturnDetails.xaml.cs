﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;

namespace AGMInventorySystemWPF.Form.Movement
{
    /// <summary>
    /// Interaction logic for ReturnDetails.xaml
    /// </summary>
    /// 

    public struct ReturnType
    {
        public string ReturnTypeName { get; set; }
    }

    public partial class ReturnDetails : Window
    {
        string _returnno;
        short _crop;
        int _itemid;
        decimal _limit_qty;
        string _location;
        List<return_details> _returndetails;
        public ReturnDetails(short crop, string returnno)
        {
            InitializeComponent();
            _returnno = returnno;
            _crop = crop;

            cropTextBox.Text = _crop.ToString();
            mmDatePicker.SelectedDate = DateTime.Now;

            var returnTypeList = new List<ReturnType>();
            returnTypeList.Add(new ReturnType { ReturnTypeName = "Supplier" });
            returnTypeList.Add(new ReturnType { ReturnTypeName = "Trial" });
            returnTypeList.Add(new ReturnType { ReturnTypeName = "Internal Used" });
            returnTypeCombobox.ItemsSource = returnTypeList;

            //------------ Edit return details ------------
            if (!String.IsNullOrEmpty(_returnno))
                GetRTDetails();
        }

        private void GetRTDetails()
        {
            try
            {
                returnnoTextBox.Text = _returnno;

                var existed = BLServices.return_masterBL().GetSingle(_returnno);
                if (existed != null)
                {
                    _location = existed.tolocationid;
                    cropTextBox.Text = existed.crop.ToString();
                    locationTextBox.Text = existed.location.name;
                    mmDatePicker.SelectedDate = existed.returndate;
                    remarkTextBox.Text = existed.remark;
                    issueNoCombobox.SelectedValue = existed.issueno;
                    supplierTextBox.Text = existed.issue_master.issuesupplier == 0 ? "" :
                        BLServices.supplierBL()
                        .GetSingle((int)existed.issue_master.issuesupplier)
                        .supplier_name
                        .ToString();
                    returnTypeCombobox.SelectedValue = existed.returntype;

                    DisableAll();
                    GetItemDetails();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DisableAll()
        {
            mmDatePicker.IsEnabled = false;
            returnTypeCombobox.IsEnabled = false;
            issueNoCombobox.IsEnabled = false;
            AddItemButton.IsEnabled = true;
        }

        private void stockDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (stockDataGrid.SelectedIndex < 0)
                    return;

                var selected = (issue_details_vm)stockDataGrid.SelectedItem;
                if (selected != null)
                {
                    itemTextBox.Text = selected.itemname.ToString();
                    descriptionTextBox.Text = BLServices.material_itemBL()
                        .GetSingle(selected.itemid)
                        .item_description
                        .Replace(System.Environment.NewLine, " ");

                    //----binding selected item------
                    _itemid = selected.itemid;
                    //------ Find the qty summary of this item on this issue no.
                    //_limit_qty = selected.issue_quantity;
                    _limit_qty = BLServices.issue_detailsBL().GetItemQtybyIssueNo(issueNoCombobox.Text, _itemid);
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal allow_qty_to_return = 0;
                if (string.IsNullOrEmpty(itemTextBox.Text))
                    throw new Exception("Please select Item for return");

                if (qtyTextBox.Text == "")
                    throw new Exception("โปรดระบุจำนวน");

                if (RegularExpressionHelper.IsNumericCharacter(qtyTextBox.Text) == false)
                    throw new Exception("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                //Check return qty not over issue qty
                if (Convert.ToInt64(qtyTextBox.Text) > _limit_qty)
                {
                    MessageBoxHelper.Warning("ไม่สามารถคืน Item นี้ได้ เนื่องจากจำนวนที่คืนไม่ถูกต้อง");
                    itemTextBox.Text = "";
                    qtyTextBox.Text = "";
                    return;
                }
                //Check summary of this issue How many return of them before
                //---- Return summary of this issue no. --------
                var lastReturnInfo = BLServices.return_detailsBL().GetRTSummaryByissueNo(issueNoCombobox.SelectedValue.ToString(), _itemid);
                //---- Qty of items which can return -----
                allow_qty_to_return = _limit_qty - lastReturnInfo;
                if (Convert.ToInt64(qtyTextBox.Text) > allow_qty_to_return)
                {
                    MessageBoxHelper.Warning("Item นี้มีการคืนครบจำนวนตาม issue นี้แล้ว ไม่สามารถคืนได้อีก");
                    itemTextBox.Text = "";
                    qtyTextBox.Text = "";
                    return;
                }

                //-----Allow return item------
                AddReturnDetails();
                GetItemDetails();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GetItemDetails()
        {
            try
            {
                //bliding selected item to data grid
                if (String.IsNullOrEmpty(returnnoTextBox.Text))
                    return;

                ReturnDataGrid.ItemsSource = null;
                _returndetails = BLServices.return_detailsBL().GetRTDetails(_returnno);

                if (_returndetails != null || _returndetails.Count() > 0)
                {
                    var returnlst = Helper.return_master_helper.GetreturnListByreturnNo(_returnno).OrderByDescending(d => d.return_quantity);

                    ReturnDataGrid.ItemsSource = returnlst;
                    TotalTextBlock2.Text = ReturnDataGrid.Items.Count.ToString("N0");
                }
                else
                {
                    //If user doesn't add any items yet, open add item function 
                    EnableItems();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddReturnDetails()
        {
            try
            {
                BLServices.return_detailsBL().Add(new return_details
                {
                    returnno = _returnno,
                    itemid = _itemid,
                    returndetailsid = Guid.NewGuid(),
                    return_quantity = Convert.ToInt32(qtyTextBox.Text),
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });

                //Return Item back to the location
                AddTransactionItem(Convert.ToInt32(qtyTextBox.Text));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddTransactionItem(int v2)
        {
            try
            {
                var itemdetail = BLServices.material_itemBL().GetSingle(_itemid);
                if (itemdetail == null)
                    return;

                //find unit price in transaction
                var itemtransaction = BLServices.material_transactionBL().GetStockByItemid(itemdetail.itemid);
                if (itemtransaction == null)
                    return;

                BLServices.material_transactionBL().In(new material_transaction
                {
                    crop = Convert.ToInt16(cropTextBox.Text),
                    refid = _returnno,
                    itemid = itemdetail.itemid,
                    unitid = itemdetail.unitid,
                    unitprice = (decimal)itemtransaction[0].Average_unitPrice,
                    quantity = v2,
                    locationid = _location,
                    transactiontype = "In",
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void issueNoCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string _issue_no;
                if (issueNoCombobox.SelectedIndex < 0)
                    return;

                _issue_no = issueNoCombobox.SelectedValue.ToString();
                var issue_d = BLServices.issue_masterBL().GetSingle(_issue_no);
                if (issue_d == null)
                    return;
                supplierTextBox.Text = issue_d.issuesupplier == 0 ? "" : BLServices.supplierBL().GetSingle((int)issue_d.issuesupplier).supplier_name.ToString();
                locationTextBox.Text = BLServices.locationBL().GetSingle(issue_d.locationid).name.ToString();
                _location = issue_d.locationid;

                //Add to stockDataGrid
                stockDataGrid.ItemsSource = null;
                var _issuedetails = BLServices.issue_detailsBL().GetIssueDetails(_issue_no);
                if (_issuedetails == null)
                    return;
                var issuelst = Helper.issue_master_helper.GetIssueDetailsByIssueNo(_issue_no).OrderByDescending(x => x.issue_quantity);

                stockDataGrid.ItemsSource = issuelst;
                TotalTextBlock.Text = stockDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mmDatePicker.Text == "")
                    throw new Exception("Please select Return Date before add");

                if (issueNoCombobox.SelectedIndex < 0)
                    throw new Exception("Please select Issue no for return");

                if (returnTypeCombobox.SelectedIndex < 0)
                    throw new Exception("Please select Return Type");

                if (String.IsNullOrEmpty(returnnoTextBox.Text))
                {
                    //for master
                    AddReturnMaster();
                }
                else
                {
                    BLServices.return_masterBL().Edit(new return_master
                    {
                        returnno = returnnoTextBox.Text,
                        issueno = issueNoCombobox.Text,
                        crop = _crop,
                        remark = remarkTextBox.Text,
                        requesteddate = DateTime.Now,
                        requesteduser = user_setting.User.username
                    });
                }
                EnableItems();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EnableItems()
        {
            //disable details part
            returnnoTextBox.Text = _returnno;
            AddItemButton.IsEnabled = true;
            qtyTextBox.IsEnabled = true;
            issueNoCombobox.IsReadOnly = true;
        }

        private void AddReturnMaster()
        {
            try
            {
                _returnno = BLServices.return_masterBL()
                .Add(new return_master
                {
                    returnno = "",
                    returndate = Convert.ToDateTime(mmDatePicker.SelectedDate),
                    crop = _crop,
                    tolocationid = _location,
                    issueno = issueNoCombobox.SelectedValue.ToString(),
                    returntype = returnTypeCombobox.SelectedValue == null ? "" : returnTypeCombobox.Text,
                    remark = remarkTextBox.Text,
                    requesteddate = DateTime.Now,
                    requesteduser = user_setting.User.username
                });

                returnnoTextBox.Text = _returnno;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void returnTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //Add issue no by type to issue combobox
                issueNoCombobox.ItemsSource = null;

                if (returnTypeCombobox.SelectedIndex == 0)
                    issueNoCombobox.ItemsSource = BLServices.issue_masterBL()
                        .GetissuenoByType(3)
                        .OrderByDescending(x=>x.issueno)
                        .ToList();
                else
                    issueNoCombobox.ItemsSource = BLServices.issue_masterBL()
                        .GetissuenoByType(4)
                        .OrderByDescending(x => x.issueno)
                        .ToList();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}

