﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;

namespace AGMInventorySystemWPF.Form.Movement
{
    /// <summary>
    /// Interaction logic for MovementList.xaml
    /// </summary>
    public partial class MovementList : Page
    {
        string _userlocation;
        public MovementList()
        {
            InitializeComponent();
            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = DateTime.Now.Year;

            locationCombobox.ItemsSource = null;
            locationCombobox.ItemsSource = BLServices.locationBL()
                .GetAll()
                .OrderBy(x => x.name);

            //Show only user location related
            Blindingfromlocation();

            //show All Button
            if (user_setting.UserRoles.Where(x => x.rolename == "Admin").Count() > 0) ShowAllButton.IsEnabled = true;

            //mmBlindingdata();
            BlidingDatagrid();

        }

        private void BlidingDatagrid()
        {
            try
            {
                MoveDataGrid.ItemsSource = null;
                MoveDataGrid.ItemsSource = Helper.movement_master_helper.GetmovementListByCropUserLocation(Convert.ToInt16(CropCombobox.SelectedValue), _userlocation)
                    .OrderByDescending(x => x.movementno);

                TotalTextBlock.Text = MoveDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private void mmBlindingdata()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (CropCombobox.SelectedValue == null)
                    return;

                if (locationCombobox.SelectedIndex < 0) return;
                if (locationCombobox.SelectedValue == null) return;

                if (fromlocationCombobox.SelectedIndex < 0) return;
                if (fromlocationCombobox.SelectedValue == null) return;

                MoveDataGrid.ItemsSource = null;
                MoveDataGrid.ItemsSource = Helper.movement_master_helper.GetmovementListByCrop(Convert.ToInt16(CropCombobox.SelectedValue),locationCombobox.SelectedValue.ToString())
                    .OrderByDescending(x => x.movementno);

                TotalTextBlock.Text = MoveDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Blindingfromlocation()
        {
            var locatedLst = BLServices.user_locationBL().GetByUsername(user_setting.User.username);

            if (locatedLst.Count() == 0) return;
            //Get First of user location
            _userlocation = locatedLst.FirstOrDefault().locationid;
            var fromList = new List<location>();
            foreach(var i in locatedLst)
            {
                var vm = new location
                {
                    name = i.location.name,
                    locationid = i.locationid,                   
                };
                fromList.Add(vm);
            };

            fromlocationCombobox.ItemsSource = null;
            fromlocationCombobox.ItemsSource = fromList;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("Please select crop for Create new movement");

                MovementDetails window = new MovementDetails(Convert.ToInt16(CropCombobox.SelectedValue),"");
                window.ShowDialog();
                BlidingDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BlidingDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MMDataGridBinding()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0) return;
                if (CropCombobox.SelectedValue == null) return;

                if (fromlocationCombobox.SelectedIndex < 0) return;
                if (fromlocationCombobox.SelectedValue == null) return;

                MoveDataGrid.ItemsSource = null;
                MoveDataGrid.ItemsSource = Helper.movement_master_helper.GetmovementListBy2Location(Convert.ToInt16(CropCombobox.SelectedValue), fromlocationCombobox.SelectedValue.ToString()
                    , locationCombobox.SelectedValue.ToString()).OrderByDescending(x => x.movementno);
                //MoveDataGrid.ItemsSource = Helper.movement_master_helper.GetmovementListByCrop(Convert.ToInt16(CropCombobox.SelectedValue), fromlocationCombobox.SelectedValue.ToString())
                //    .OrderByDescending(x => x.movementno);

                TotalTextBlock.Text = MoveDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MoveDataGrid.SelectedIndex < 0)
                    return;

                var model = (movement_master_vm)MoveDataGrid.SelectedItem;
                if (model == null)
                    return;

                //Check receiving
                if (model.receiveddate != null) throw new ArgumentException("Movement นี้ สิ้นสุดแล้ว ไม่สามารถลบออกจากระบบได้");

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BLServices.movement_masterBL().Delete(model.movementno);
                BlidingDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MovementDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MoveDataGrid.SelectedIndex < 0)
                    return;

                var model = (movement_master_vm)MoveDataGrid.SelectedItem;

                MovementDetails window = new MovementDetails(Convert.ToInt16(CropCombobox.SelectedValue),model.movementno);
                window.ShowDialog();
                //MMDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void locationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MMDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            locationCombobox.SelectedValue = null;
            fromlocationCombobox.SelectedValue = null;

            BlidingDatagrid();
        }

        private void ShowAllButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0) return;
                if (CropCombobox.SelectedValue == null) return;

                locationCombobox.SelectedValue = null;

                MoveDataGrid.ItemsSource = null;
                MoveDataGrid.ItemsSource = Helper.movement_master_helper.GetAllmovementList(Convert.ToInt16(CropCombobox.SelectedValue))
                    .OrderByDescending(x => x.movementno);

                TotalTextBlock.Text = MoveDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void fromlocationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MoveDataGrid.ItemsSource = null;
                MoveDataGrid.ItemsSource = Helper.movement_master_helper.GetmovementListByCrop(Convert.ToInt16(CropCombobox.SelectedValue), fromlocationCombobox.SelectedValue.ToString())
                    .OrderByDescending(x => x.movementno);

                TotalTextBlock.Text = MoveDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
