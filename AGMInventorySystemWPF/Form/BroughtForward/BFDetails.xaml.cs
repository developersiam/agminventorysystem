﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemBL;

namespace AGMInventorySystemWPF.Form.BroughtForward
{
    /// <summary>
    /// Interaction logic for BFDetails.xaml
    /// </summary>
    public partial class BFDetails : Window
    {
        short _crop;
        string _bf_no;
        string _locationid;
        List<sp_Material_TransactionByCropLocation_Result> _tmpRec;
        List<broughtforward_d> _bfdetails;
        public BFDetails(short crop, string bfno)
        {
            _crop = crop;
            _bf_no = bfno;
            _tmpRec = new List<sp_Material_TransactionByCropLocation_Result>();
            _bfdetails = new List<broughtforward_d>();
            InitializeComponent();

            cropTextBox.Text = _crop.ToString();
            locationCombobox.ItemsSource = null;
            locationCombobox.ItemsSource = BLServices.locationBL().GetAll();

            unitPriceTextBox.Text = "0.00";

            BFDatePicker.SelectedDate = DateTime.Now;

            //------------ Edit issue details ------------
            if (!String.IsNullOrEmpty(_bf_no)) GetBFDetails();
        }

        private void GetBFDetails()
        {
            try
            {
                BFTextBox.Text = _bf_no;

                var existed = BLServices.brought_forwardBL().GetSingle(_bf_no);
                if (existed != null)
                {
                    _locationid = existed.locationid;
                    cropTextBox.Text = existed.crop.ToString();
                    locationCombobox.SelectedValue = existed.locationid;
                    BFDatePicker.SelectedDate = existed.requesteddate;
                    remarkTextBox.Text = existed.remark;

                    //Blinding to item stock
                    BlindingStock();
                    //Disable all tools
                    DisableAll();
                    //Blinding to datagrid
                    GetItemDetails();
                }
            }
            catch(Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DisableAll()
        {
            BFDatePicker.IsEnabled = false;
            locationCombobox.IsEnabled = false;
            AddItemButton.IsEnabled = true;
            createButton.IsEnabled = false;
        }

        private void locationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Show remaining stock by location
            BlindingStock();
            stockText.Text = "All items in " + locationCombobox.Text + ". Please be careful before add Items for brought forward.";
        }

        private void BlindingStock()
        {
            if (locationCombobox.SelectedIndex < 0) return;
            if (locationCombobox.SelectedValue == null) return;

            //Blinding Data to combobox
            itemCombobox.ItemsSource = null;
            itemCombobox.ItemsSource = BLServices.material_itemBL().GetAll();

            var location = (location)locationCombobox.SelectedItem;

            stockDataGrid.ItemsSource = null;
            _tmpRec = BLServices.material_transactionBL()
                .GetMatStockByCropLocation(location.locationid)
                .OrderBy(x => x.item_name)
                .ToList();
            stockDataGrid.ItemsSource = _tmpRec;

            TotalTextBlock.Text = stockDataGrid.Items.Count.ToString("N0");
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BFDatePicker.Text == "") throw new ArgumentException("Please select Brought Forward Date before add");
                if (locationCombobox.SelectedIndex < 0) throw new ArgumentException("Please select location for issue");

                var msg = MessageBox.Show("ท่านต้องการสร้าง New Brought Forward ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                if (String.IsNullOrEmpty(BFTextBox.Text))
                {
                    //for master
                    AddBF();

                }
                else
                {
                    //BLServices.issue_masterBL().Edit(new issue_master
                    //{
                    //    issueno = issuenoTextBox.Text,
                    //    crop = _crop,
                    //    issuepicturepath = pathTextBox.Text,
                    //    issuesupplier = supplierCombobox.SelectedValue == null ? 0 : Convert.ToInt16(supplierCombobox.SelectedValue),
                    //    issueInternalreason = internalCombobox.SelectedValue == null ? "" : internalCombobox.SelectedValue.ToString(),
                    //    remark = remarkTextBox.Text,
                    //    requesteddate = DateTime.Now,
                    //    requesteduser = user_setting.User.username
                    //});

                }

                EnableItems();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddBF()
        {
            _bf_no = BLServices.brought_forwardBL().Add(new broughtforward
                        {
                            broughtforwardid = "",
                            requesteddate = Convert.ToDateTime(BFDatePicker.SelectedDate),
                            crop = _crop,
                            locationid = locationCombobox.SelectedValue.ToString(),
                            remark = remarkTextBox.Text,
                            modifieddate = DateTime.Now,
                            modifieduser = user_setting.User.username
                        });

            BFTextBox.Text = _bf_no;
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            if (itemCombobox.SelectedIndex < 0) throw new ArgumentException("Please select Item for issue");
            if (qtyTextBox.Text == "") throw new ArgumentException("โปรดระบุ quantity");
            if (unitPriceTextBox.Text == "") throw new ArgumentException("โปรดระบุ Unit Price");
            if (Helper.RegularExpressionHelper.IsNumericCharacter(qtyTextBox.Text) == false) throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");
            if (Helper.RegularExpressionHelper.IsDecimalCharacter(unitPriceTextBox.Text) == false) throw new ArgumentException("Unit Price จะต้องเป็นตัวเลขเท่านั้น");

            var msg = MessageBox.Show("ท่านต้องการเพิ่ม Items Brought Forward ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (msg == MessageBoxResult.No)
                return;

            AddIssueDetails();
            GetItemDetails();
        }

        private void GetItemDetails()
        {
            //bliding selected item to data grid
            if (String.IsNullOrEmpty(BFTextBox.Text)) return;

            BFDataGrid.ItemsSource = null;
            _bfdetails = BLServices.brought_forward_d_BL().GetBFDetails(_bf_no);
            if (_bfdetails != null || _bfdetails.Count() > 0)
            {
                var bflst = Helper.broughtforward_helper.GetBFDetailsByBFNo(_bf_no)
                    .OrderByDescending(x => x.quantity)
                    .ToList();
                BFDataGrid.ItemsSource = bflst;
                TotalTextBlock2.Text = BFDataGrid.Items.Count.ToString("N0");
            }
            //If user doesn't add any items yet, open add item function 
            else EnableItems();
            //Binding item by location
            BlindingStock();
        }

        private void EnableItems()
        {
            //disable details part
            BFTextBox.Text = _bf_no;
            itemCombobox.ItemsSource = null;
            itemCombobox.ItemsSource = BLServices.material_itemBL().GetAll();
            locationCombobox.IsEnabled = false;

            AddItemButton.IsEnabled = true;
            qtyTextBox.IsEnabled = true;
            unitPriceTextBox.IsEnabled = true;
        }

        private void AddIssueDetails()
        {
            BLServices.brought_forward_d_BL().Add(new broughtforward_d {
                broughtforwardid = _bf_no,
                itemid = Convert.ToInt32(itemCombobox.SelectedValue),
                broughtforward_d_id = Guid.NewGuid(),
                quantity = Convert.ToInt32(qtyTextBox.Text),
                unitprice = Convert.ToDecimal(unitPriceTextBox.Text),
                modifieddate = DateTime.Now,
                modifieduser = user_setting.User.username
            });

            AddTransactionItem(Convert.ToInt32(itemCombobox.SelectedValue.ToString()), Convert.ToInt32(qtyTextBox.Text), Convert.ToDecimal(unitPriceTextBox.Text));
        }

        private void AddTransactionItem(int itemid, int qty, decimal unitP)
        {
            var itemdetail = BLServices.material_itemBL().GetSingle(itemid);
            if (itemdetail == null) return;

            BLServices.material_transactionBL().In(new material_transaction
            {
                crop = Convert.ToInt16(cropTextBox.Text),
                refid = _bf_no,
                itemid = itemdetail.itemid,
                unitid = itemdetail.unitid,
                unitprice = unitP,
                quantity = qty,
                locationid = locationCombobox.SelectedValue.ToString(),
                transactiontype = "In",
                modifieddate = DateTime.Now,
                modifieduser = user_setting.User.username
            });
        }

        private void stockDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (stockDataGrid.SelectedIndex < 0)
                    return;

                var selected = (sp_Material_TransactionByCropLocation_Result)stockDataGrid.SelectedItem;
                if (selected != null)
                {
                    itemCombobox.SelectedValue = selected.itemid;
                    itemCombobox.SelectedItem = selected.item_name;
                };

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
