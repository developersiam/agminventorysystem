﻿using AGMInventorySystemBL;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AGMInventorySystemEntities;

namespace AGMInventorySystemWPF.Form.BroughtForward
{
    /// <summary>
    /// Interaction logic for BFList.xaml
    /// </summary>
    public partial class BFList : Page
    {
        public BFList()
        {
            InitializeComponent();

            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = DateTime.Now.Year;

            LocationCombobox.ItemsSource = null;
            LocationCombobox.ItemsSource = BLServices.locationBL()
                .GetlstwithAll()
                .OrderBy(x => x.name);
            LocationCombobox.SelectedIndex = -1;

            BlidingDatagrid();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (CropCombobox.SelectedIndex < 0)
                throw new ArgumentException("Please select crop for Create new movement");

            BFDetails window = new BFDetails(Convert.ToInt16(CropCombobox.SelectedValue), "");
            window.ShowDialog();
            BlidingDatagrid();
        }
        private void BFDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BFDataGrid.SelectedIndex < 0)
                    return;

                var model = (brought_forward_vm)BFDataGrid.SelectedItem;

                BFDetails window = new BFDetails(Convert.ToInt16(CropCombobox.SelectedValue), model.broughtforwardid);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void BlidingDatagrid()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (LocationCombobox.SelectedIndex < 0)
                    return;

                BFDataGrid.ItemsSource = null;
                var location = (location)LocationCombobox.SelectedItem;
                var BFlst = Helper.broughtforward_helper.GetBFListByCrop(Convert.ToInt16(CropCombobox.SelectedValue));

                if (location.name == "All")
                    BFDataGrid.ItemsSource = BFlst.OrderByDescending(x => x.broughtforwardid);
                else
                    BFDataGrid.ItemsSource = BFlst.Where(x => x.locationid == location.locationid)
                                                    .OrderByDescending(x => x.broughtforwardid);

                TotalTextBlock.Text = BFDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LocationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BlidingDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
