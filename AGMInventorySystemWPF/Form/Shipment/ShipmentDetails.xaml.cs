﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Shipment
{
    /// <summary>
    /// Interaction logic for ShipmentDetails.xaml
    /// </summary>
    public partial class ShipmentDetails : Window
    {
        shipment_master _shipmentmaster;
        shipment_details _shipmentdetails;
        List<shipment_details> _shipmentdetailsList;
        List<purchaseorder_details> _podetailsList;
        int _poItemQuantity;
        int _shippedItems;


        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;

        public ShipmentDetails(shipment_master model)
        {
            try
            {
                InitializeComponent();

                _shipmentmaster = new shipment_master();
                _shipmentdetails = new shipment_details();
                _shipmentdetailsList = new List<shipment_details>();
                _podetailsList = new List<purchaseorder_details>();

                _shipmentmaster = model;
                _podetailsList = BLServices.purchaseorder_detailsBL().GetByPono(_shipmentmaster.pono);

                itemComboBox.ItemsSource = null;
                itemComboBox.ItemsSource = BLServices.purchaseorder_detailsBL()
                    .GetByPono(_shipmentmaster.pono)
                    .OrderBy(x => x.material_item.item_name);

                shipmentMasterBinding();
                shipmentDetailsDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void shipmentMasterBinding()
        {
            try
            {
                _shipmentmaster = BLServices.shipment_masterBL().GetSingle(_shipmentmaster.shipmentno);

                shipmentTextBlock.Text = _shipmentmaster.shipmentno;
                shipmentstatusLabel.Content = _shipmentmaster.shipmentstatus;
                shipmentstatusLabel.Background = _shipmentmaster.shipmentstatus == "Complete"
                    ? Brushes.LightGreen : Brushes.LightPink;

                approveddate1CheckBox.IsChecked = _shipmentmaster.approveddate1 != null ? true : false;
                PrintShipmentButton.IsEnabled = _shipmentmaster.approveddate1 != null ? true : false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void shipmentDetailsDataGridBinding()
        {
            try
            {
                _shipmentdetailsList = BLServices.shipment_detailsBL().GetByShipmentno(_shipmentmaster.shipmentno);

                shipmentdetailsDataGrid.ItemsSource = null;
                shipmentdetailsDataGrid.ItemsSource = _shipmentdetailsList.OrderBy(x => x.material_item.item_name);

                TotalTextBlock.Text = _shipmentdetailsList.Count.ToString("N0") + " items.";

                _shipmentmaster = BLServices.shipment_masterBL().GetSingle(_shipmentmaster.shipmentno);
                PrintShipmentButton.IsEnabled = _shipmentmaster.approveddate1 != null ? true : false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void shipmentQtyTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (EditButton.IsEnabled == true)
                {
                    poQtyTextBox.Text = _poItemQuantity.ToString("N0") + "/" + (_shippedItems - _shipmentdetails.shipment_quantity).ToString("N0");
                    BalanceQtyTextBox.Text = (_poItemQuantity - (_shippedItems - _shipmentdetails.shipment_quantity) - Convert.ToInt32(shipmentQtyTextBox.Text)).ToString("N0");

                    if ((_shippedItems - _shipmentdetails.shipment_quantity) + Convert.ToInt32(shipmentQtyTextBox.Text) > _poItemQuantity)
                        throw new ArgumentException("จำนวน item ที่ระบุใน shipment นี้ เกินจากจำนวนที่ระบุในใบ PO");
                }
                else
                {
                    poQtyTextBox.Text = _poItemQuantity.ToString("N0") + "/" + _shippedItems.ToString("N0");
                    BalanceQtyTextBox.Text = (_poItemQuantity - _shippedItems - Convert.ToInt32(shipmentQtyTextBox.Text)).ToString("N0");

                    if (_shippedItems + Convert.ToInt32(shipmentQtyTextBox.Text) > _poItemQuantity)
                        throw new ArgumentException("จำนวน item ที่ระบุใน shipment นี้ เกินจากจำนวนที่ระบุในใบ PO");
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            try
            {
                itemComboBox.IsEnabled = true;
                itemComboBox.SelectedIndex = -1;
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;

                _shipmentdetails = null;

                shipmentQtyTextBox.Clear();
                shipmentDetailsDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (itemComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ item");

                if (shipmentQtyTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ shipment qty");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(shipmentQtyTextBox.Text) == false)
                    throw new ArgumentException("shipment qty จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                BLServices.shipment_detailsBL()
                    .Add(new shipment_details
                    {
                        shipmentdetailsid = Guid.NewGuid(),
                        shipmentno = _shipmentmaster.shipmentno,
                        itemid = Convert.ToInt32(itemComboBox.SelectedValue.ToString()),
                        shipment_quantity = Convert.ToInt32(shipmentQtyTextBox.Text),
                        received_quantity = 0,
                        unitprice = _podetailsList
                        .SingleOrDefault(x => x.itemid == Convert.ToInt32(itemComboBox.SelectedValue.ToString()))
                        .unitprice,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    }, _shipmentmaster);

                shipmentDetailsDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                if (itemComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ item");

                if (shipmentQtyTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ shipment qty");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(shipmentQtyTextBox.Text) == false)
                    throw new ArgumentException("shipment qty จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                _shipmentdetails.itemid = Convert.ToInt16(itemComboBox.SelectedValue.ToString());
                _shipmentdetails.unitprice = _podetailsList
                        .SingleOrDefault(x => x.itemid == Convert.ToInt32(itemComboBox.SelectedValue.ToString()))
                        .unitprice;
                _shipmentdetails.shipment_quantity = Convert.ToInt32(shipmentQtyTextBox.Text);
                _shipmentdetails.modifieddate = DateTime.Now;
                _shipmentdetails.modifieduser = user_setting.User.username;

                BLServices.shipment_detailsBL().Edit(_shipmentdetails);
                shipmentDetailsDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentdetailsDataGrid.SelectedIndex < 0)
                    return;

                var model = (shipment_details)shipmentdetailsDataGrid.SelectedItem;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BLServices.shipment_detailsBL().Delete(model.shipmentdetailsid);
                shipmentDetailsDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintShipmentButton_Click(object sender, RoutedEventArgs e)
        {
            Print();
        }

        private void Print()
        {
            try
            {
                excelWorkBook = excelApplication.Workbooks.Open(@"C:\AGMInventory\SH-Form.xlsx");
                excelWorkSheet = excelWorkBook.ActiveSheet;
                excelWorkSheet.PageSetup.Zoom = false;
                excelWorkSheet.PageSetup.FitToPagesWide = 1;
                excelApplication.Visible = true;

                PutDataIntoCell("D7", _shipmentmaster.location.name);
                PutDataIntoCell("D8", _shipmentmaster.address);
                PutDataIntoCell("S7", _shipmentmaster.shipmentno);
                PutDataIntoCell("S8", _shipmentmaster.shipmentdate.ToString("dd/MM/yyyy"));
                PutDataIntoCell("S11", _shipmentmaster.pono);

                PutDataIntoCell("D11", _shipmentmaster.purchaseorder_master.vender.name);

                int row = 16;
                foreach (var item in _shipmentdetailsList)
                {
                    PutDataIntoCell("B" + row, item.itemid.ToString());
                    PutDataIntoCell("C" + row, item.shipment_quantity.ToString());
                    PutDataIntoCell("D" + row, item.material_item.material_unit.unit_name.ToString());
                    PutDataIntoCell("E" + row, item.material_item.item_name.ToString());
                    PutDataIntoCell("S" + row, item.unitprice.ToString());
                    row++;
                }

                if (_shipmentmaster.remark != null) PutDataIntoCell("E45", _shipmentmaster.remark);

                excelApplication.Quit();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PutDataIntoCell(string cell, string value)
        {
            excelRange = excelWorkSheet.Range[cell];
            excelWorkSheet.Range[cell].Value = value;
        }

        private void approveddate1CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.UserRoles.Where(x => x.rolename == "Approver1").Count() <= 0)
                    throw new ArgumentException("user account ของท่านไม่สามารถ approve PO นี้ได้");

                if (_shipmentmaster.approveddate1 == null)
                    BLServices.shipment_masterBL().Approve(_shipmentmaster.shipmentno, user_setting.User.username);
                else
                    BLServices.shipment_masterBL().CancelApprove(_shipmentmaster.shipmentno);

                shipmentDetailsDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
                approveddate1CheckBox.IsChecked = _shipmentmaster.approveddate1 == null ? false : true;
            }
        }

        private void ItemInfoBinding()
        {
            try
            {
                if (itemComboBox.SelectedIndex < 0)
                {
                    poQtyTextBox.Clear();
                    BalanceQtyTextBox.Clear();
                    shipmentQtyTextBox.Clear();
                    return;
                }

                var item = (purchaseorder_details)itemComboBox.SelectedItem;

                _shippedItems = Convert.ToInt32(BLServices.shipment_detailsBL()
                    .GetByPono(_shipmentmaster.pono)
                    .Where(x => x.itemid == item.itemid)
                    .Sum(x => x.shipment_quantity));

                _poItemQuantity = _podetailsList
                    .SingleOrDefault(x => x.itemid == item.itemid)
                    .quantity;

                if (EditButton.IsEnabled == true)
                {
                    poQtyTextBox.Text = _poItemQuantity.ToString("N0") + "/" + (_shippedItems - _shipmentdetails.shipment_quantity).ToString("N0");
                    BalanceQtyTextBox.Text = (_poItemQuantity - (_shippedItems - _shipmentdetails.shipment_quantity) - _shipmentdetails.shipment_quantity).ToString("N0");
                    shipmentQtyTextBox.Text = _shipmentdetails.shipment_quantity.ToString("N0");
                }
                else
                {
                    poQtyTextBox.Text = _poItemQuantity.ToString("N0") + "/" + _shippedItems.ToString("N0");
                    BalanceQtyTextBox.Text = (_poItemQuantity - _shippedItems).ToString("N0");
                    shipmentQtyTextBox.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void itemComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ItemInfoBinding();
        }
    }
}
