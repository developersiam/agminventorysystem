﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Form.UserAccount;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Shipment
{
    /// <summary>
    /// Interaction logic for ShipmentReceiving.xaml
    /// </summary>
    /// 
    public partial class ShipmentReceiving : Page
    {
        List<shipment_master> _shipmentMasterlist;
        List<shipment_details> _shipmentDetailsList;

        public ShipmentReceiving()
        {
            InitializeComponent();

            _shipmentMasterlist = new List<shipment_master>();
            _shipmentDetailsList = new List<shipment_details>();

            cropComboBox.ItemsSource = null;
            cropComboBox.ItemsSource = user_setting.Crops;
            cropComboBox.SelectedValue = DateTime.Now.Year.ToString();
        }

        private void shipmentnoDataBinding()
        {
            try
            {
                approveddate2TextBox.Clear();
                shippedQtyTextBox.Clear();
                receivedQtyTextBox.Clear();
                notReceivedQtyTextBox.Clear();
                locationTextBox.Clear();
                receivedStatusTextBox.Clear();
                shipmentstatusTextBox.Clear();
                receivedStatusTextBox.Background = Brushes.White;
                shipmentstatusTextBox.Background = Brushes.White;
                shipmentdetailsDataGrid.ItemsSource = null;

                if (cropComboBox.SelectedIndex < 0)
                    return;

                var item = (InventoryCrop)cropComboBox.SelectedItem;

                if (user_setting.UserLocations.Count() == 0) return;

                var userlocation = user_setting.UserLocations.FirstOrDefault().locationid;
                var list = BLServices.shipment_masterBL()
                    .GetByCropLocation(Convert.ToInt16(item.crop),
                    user_setting.UserLocations.FirstOrDefault().locationid)
                    .ToList();
                _shipmentMasterlist = list
                    .Where(x => x.receivedstatus == isReceivedCheckBox.IsChecked &&
                    x.approveddate1 != null)
                    .ToList();

                shipmentnoComboBox.ItemsSource = null;
                shipmentnoComboBox.ItemsSource = _shipmentMasterlist.OrderByDescending(x => x.shipmentno);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void shipmentDetailsDataBinding()
        {
            try
            {
                if (shipmentnoComboBox.SelectedIndex < 0)
                    return;

                var selected = (shipment_master)shipmentnoComboBox.SelectedItem;

                _shipmentDetailsList = BLServices.shipment_detailsBL().GetByShipmentno(selected.shipmentno);

                shipmentdetailsDataGrid.ItemsSource = null;
                shipmentdetailsDataGrid.ItemsSource = _shipmentDetailsList
                    .Select(x => new shipment_details_vm
                    {
                        shipmentdetailsid = x.shipmentdetailsid,
                        shipmentno = x.shipmentno,
                        itemid = x.itemid,
                        shipment_quantity = x.shipment_quantity,
                        received_quantity = x.received_quantity,
                        unitprice = x.unitprice,
                        remark = x.remark,
                        receivedstatus = x.receivedstatus,
                        receiveddate = x.receiveddate,
                        receiveduser = x.receiveduser,
                        modifieddate = x.modifieddate,
                        modifieduser = x.modifieduser,
                        material_item = x.material_item,
                        CancelReceivedButtonIsEanbled = x.received_quantity != x.shipment_quantity ? false : true,
                        ReceivedButtonIsEanbled = x.received_quantity == x.shipment_quantity ? false : true
                    }).ToList()
                .OrderBy(x => x.itemid);

                TotalTextBlock.Text = _shipmentDetailsList.Count.ToString("N0");
                shippedQtyTextBox.Text = _shipmentDetailsList.Sum(x => x.shipment_quantity).ToString("N0");
                receivedQtyTextBox.Text = _shipmentDetailsList.Sum(x => x.received_quantity).ToString("N0");
                notReceivedQtyTextBox.Text = _shipmentDetailsList.Sum(x => x.shipment_quantity - x.received_quantity).ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void shipmentMasterDataBinding()
        {
            try
            {
                if (shipmentnoComboBox.SelectedIndex < 0)
                    return;

                var selected = (shipment_master)shipmentnoComboBox.SelectedItem;
                var shipmentmaster = BLServices.shipment_masterBL().GetSingle(selected.shipmentno);

                locationTextBox.Text = shipmentmaster.location.name;
                approveddate2TextBox.Text = Convert.ToDateTime(shipmentmaster.approveddate2).ToString("dd/MMM/yyyy");
                receivedStatusTextBox.Text = shipmentmaster.receivedstatus == true ? "Complete" : "Waiting";
                receivedStatusTextBox.Background = shipmentmaster.receivedstatus == true ? Brushes.LightGreen : Brushes.LightPink;

                shipmentstatusTextBox.Text = shipmentmaster.shipmentstatus;
                shipmentstatusTextBox.Background = shipmentmaster.shipmentstatus == "Complete"
                    ? Brushes.LightGreen : Brushes.LightPink;

                FinishButton.Visibility = shipmentmaster.receivedstatus == true ? Visibility.Collapsed : Visibility.Visible;
                UnFinishButton.Visibility = shipmentmaster.receivedstatus == false ? Visibility.Collapsed : Visibility.Visible;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void shipmentnoComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            shipmentMasterDataBinding();
            shipmentDetailsDataBinding();
        }

        private void cropComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            shipmentnoDataBinding();
        }

        //private void remarkOnDataGridButton_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBoxHelper.Exception(ex);
        //    }
        //}

        private void isReceivedCheckBox_Click(object sender, RoutedEventArgs e)
        {
            shipmentnoDataBinding();
        }

        private void receivedItemOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentdetailsDataGrid.SelectedIndex < 0)
                    return;

                var selected = (shipment_details_vm)shipmentdetailsDataGrid.SelectedItem;
                var item = _shipmentDetailsList.SingleOrDefault(x => x.shipmentdetailsid == selected.shipmentdetailsid);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูล item ที่เลือก");

                var msg = MessageBox.Show("ท่านต้องการรับ item นี้เข้าระบบใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BLServices.shipment_detailsBL().Receive(
                    item.shipmentdetailsid,
                    item.shipment_quantity,
                    user_setting.User.username);

                var shipmentmaster = BLServices.shipment_masterBL().GetSingle(selected.shipmentno);
                BLServices.material_transactionBL().In(new material_transaction
                {
                    crop = shipmentmaster.crop,
                    refid = shipmentmaster.shipmentno,
                    itemid = item.itemid,
                    unitid = item.material_item.unitid,
                    unitprice = selected.unitprice,
                    quantity = selected.shipment_quantity,
                    locationid = shipmentmaster.locationid,
                    transactiontype = "In",
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });

                shipmentMasterDataBinding();
                shipmentDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void cancelReceivedItemOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentdetailsDataGrid.SelectedIndex < 0)
                    return;

                var selected = (shipment_details_vm)shipmentdetailsDataGrid.SelectedItem;
                var item = _shipmentDetailsList.SingleOrDefault(x => x.shipmentdetailsid == selected.shipmentdetailsid);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูล item ที่เลือก");

                var msg = MessageBox.Show("ท่านต้องยกเลิกการรับ item นี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BLServices.shipment_detailsBL()
                    .CancelReceive(item.shipmentdetailsid, user_setting.User.username);

                var shipmentmaster = BLServices.shipment_masterBL().GetSingle(selected.shipmentno);
                BLServices.material_transactionBL().Out(new material_transaction
                {
                    crop = shipmentmaster.crop,
                    refid = shipmentmaster.shipmentno,
                    itemid = item.itemid,
                    unitid = item.material_item.unitid,
                    unitprice = selected.unitprice,
                    quantity = selected.shipment_quantity,
                    locationid = shipmentmaster.locationid,
                    transactiontype = "Out",
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.username
                });

                shipmentMasterDataBinding();
                shipmentDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentnoComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ shipment");

                var shipmentmaster = (shipment_master)shipmentnoComboBox.SelectedItem;
                if (shipmentmaster == null)
                    throw new ArgumentException("โปรดระบุ shipment");

                var msg = MessageBox.Show("ท่านต้องการ finish shipmentno# " +
                    shipmentmaster.shipmentno + " นี้ใช่หรือไม่?", "confirm",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (msg == MessageBoxResult.No)
                    return;

                BLServices.shipment_masterBL()
                    .ReceiveList(shipmentmaster.shipmentno, user_setting.User.username);

                //foreach (var item in _shipmentDetailsList)
                //{
                //    BLServices.shipment_detailsBL()
                //        .Receive(item.shipmentdetailsid, item.shipment_quantity, user_setting.User.username);

                //    BLServices.material_transactionBL()
                //        .In(new material_transaction
                //        {
                //            crop = shipmentmaster.crop,
                //            refid = shipmentmaster.shipmentno,
                //            itemid = item.itemid,
                //            unitid = item.material_item.unitid,
                //            unitprice = item.unitprice,
                //            quantity = item.shipment_quantity,
                //            locationid = shipmentmaster.locationid,
                //            transactiontype = "In",
                //            modifieddate = DateTime.Now,
                //            modifieduser = user_setting.User.username
                //        });
                //}

                ////if (_shipmentDetailsList.Where(x => x.receivedstatus == false).Count() > 0)
                ////    throw new ArgumentException("ยังมี item บางรายการที่ยังไม่ได้รับเข้าระบบ ไม่สามารถ finish ได้");

                //BLServices.shipment_masterBL()
                //    .ChangeReceivedStatus(shipmentmaster.shipmentno, true, user_setting.User.username);

                shipmentMasterDataBinding();
                shipmentDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UnFinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentnoComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ shipment");

                var shipmentmaster = (shipment_master)shipmentnoComboBox.SelectedItem;
                if (shipmentmaster == null)
                    throw new ArgumentException("โปรดระบุ shipment");

                var msg = MessageBox.Show("ท่านต้องการ unfinish shipmentno# " +
                    shipmentmaster.shipmentno + " นี้ใช่หรือไม่?", "confirm",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (msg == MessageBoxResult.No)
                    return;

                BLServices.shipment_masterBL()
                    .CancelReceiveList(shipmentmaster.shipmentno, user_setting.User.username);

                //foreach (var item in _shipmentDetailsList)
                //{
                //    BLServices.shipment_detailsBL()
                //        .CancelReceive(item.shipmentdetailsid, user_setting.User.username);

                //    BLServices.material_transactionBL().Out(new material_transaction
                //    {
                //        crop = shipmentmaster.crop,
                //        refid = shipmentmaster.shipmentno,
                //        itemid = item.itemid,
                //        unitid = item.material_item.unitid,
                //        unitprice = item.unitprice,
                //        quantity = item.shipment_quantity,
                //        locationid = shipmentmaster.locationid,
                //        transactiontype = "Out",
                //        modifieddate = DateTime.Now,
                //        modifieduser = user_setting.User.username
                //    });
                //}

                ////if (_shipmentDetailsList.Where(x => x.receivedstatus == false).Count() > 0)
                ////    throw new ArgumentException("ยังมี item บางรายการที่ยังไม่ได้รับเข้าระบบ ไม่สามารถ finish ได้");

                //BLServices.shipment_masterBL()
                //    .ChangeReceivedStatus(shipmentmaster.shipmentno, false, user_setting.User.username);

                shipmentMasterDataBinding();
                shipmentDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void POnoComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
