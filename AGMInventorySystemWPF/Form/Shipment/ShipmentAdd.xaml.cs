﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Shipment
{
    /// <summary>
    /// Interaction logic for ShipmentAdd.xaml
    /// </summary>
    public partial class ShipmentAdd : Window
    {
        purchaseorder_master _pomaster;

        public ShipmentAdd(purchaseorder_master po)
        {
            try
            {
                InitializeComponent();

                _pomaster = new purchaseorder_master();
                _pomaster = po;

                ponoTextBlock.Text = _pomaster.pono;
                //locationTextBox.Text = _pomaster.location.name;
                BlindingLocationCombobox(_pomaster.locationid);

                shipmentdateDatePicker.SelectedDate = DateTime.Now;

                requestuserTextBox.Text = user_setting.User.username;
                requestdateDatePicker.SelectedDate = DateTime.Now;

                shipmentnoTextBox.Text = BLServices.shipment_masterBL().GetNewShipmentno(_pomaster.crop);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentnoTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ shipment no.");

                if (shipmentdateDatePicker.Text == "")
                    throw new ArgumentException("โปรดระบุ shipment date.");

                if (requestuserTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ requested user.");

                if (requestdateDatePicker.Text == "")
                    throw new ArgumentException("โปรดระบุ requested date.");

                if (tolocationCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ Location.");

                BLServices.shipment_masterBL()
                    .Add(new shipment_master
                    {
                        pono = _pomaster.pono,
                        crop = _pomaster.crop,
                        shipmentno = shipmentnoTextBox.Text,
                        shipmentdate = Convert.ToDateTime(shipmentdateDatePicker.SelectedDate),
                        locationid = tolocationCombobox.SelectedValue.ToString(),
                        address = addressTextBox.Text,
                        shipmentstatus = "Waiting",
                        requesteddate = Convert.ToDateTime(requestdateDatePicker.SelectedDate),
                        requesteduser = requestuserTextBox.Text,
                        remark = remarkTextBox.Text,
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                shipmentnoTextBox.Text = BLServices.shipment_masterBL().GetNewShipmentno(_pomaster.crop);
                shipmentdateDatePicker.SelectedDate = DateTime.Now;
                requestdateDatePicker.SelectedDate = DateTime.Now;
                requestuserTextBox.Text = user_setting.User.username;
                remarkTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void BlindingLocationCombobox(string locationid)
        {
            if (string.IsNullOrEmpty(locationid)) return;
            tolocationCombobox.ItemsSource = null;
            if (locationid == "03")
            {
                tolocationCombobox.ItemsSource = BLServices.locationBL().GetAll()
                    .OrderBy(x => x.name);
                tolocationCombobox.SelectedIndex = -1;
            }
            else
            {
                tolocationCombobox.ItemsSource = BLServices.locationBL()
                        .GetlstwithAll().OrderBy(x => x.name).Where(x => x.locationid == locationid);
                tolocationCombobox.SelectedIndex = 0;
            }
        }

    }
}
