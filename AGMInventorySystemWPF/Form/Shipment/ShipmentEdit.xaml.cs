﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Shipment
{
    /// <summary>
    /// Interaction logic for ShipmentEdit.xaml
    /// </summary>
    public partial class ShipmentEdit : Window
    {
        shipment_master _shipment;
        public ShipmentEdit(shipment_master model)
        {
            try
            {
                InitializeComponent();
                _shipment = new shipment_master();
                _shipment = model;

                
                ShipmentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ShipmentBinding()
        {
            try
            {
                ponoTextBlock.Text = _shipment.pono;
                shipmentnoTextBox.Text = _shipment.shipmentno;
                shipmentdateDatePicker.SelectedDate = _shipment.shipmentdate;

                shipmentstatusTextBox.Text = _shipment.shipmentstatus;
                shipmentstatusTextBox.Background = _shipment.shipmentstatus == "Complete"
                    ? Brushes.LightGreen : Brushes.LightPink;

                locationTextBox.Text = _shipment.location.name;
                addressTextBox.Text = _shipment.address;
                requestuserTextBox.Text = _shipment.requesteduser;
                requestdateDatePicker.SelectedDate = _shipment.requesteddate;
                remarkTextBox.Text = _shipment.remark;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentnoTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ shipment no.");

                if (shipmentdateDatePicker.Text == "")
                    throw new ArgumentException("โปรดระบุ shipment date.");

                if (requestuserTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ requested user.");

                if (requestdateDatePicker.Text == "")
                    throw new ArgumentException("โปรดระบุ requested date.");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _shipment.shipmentdate = Convert.ToDateTime(shipmentdateDatePicker.SelectedDate);
                _shipment.locationid = _shipment.locationid;
                _shipment.address = addressTextBox.Text;
                _shipment.requesteduser = requestuserTextBox.Text;
                _shipment.requesteddate = Convert.ToDateTime(requestdateDatePicker.SelectedDate);
                _shipment.remark = remarkTextBox.Text;
                _shipment.modifieddate = DateTime.Now;
                _shipment.modifieduser = user_setting.User.username;

                BLServices.shipment_masterBL().Edit(_shipment);

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ShipmentBinding();
        }
    }
}
