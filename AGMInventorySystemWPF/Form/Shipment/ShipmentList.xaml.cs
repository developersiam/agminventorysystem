﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.Shipment
{
    /// <summary>
    /// Interaction logic for ShipmentList.xaml
    /// </summary>
    public partial class ShipmentList : Window
    {
        purchaseorder_master _pomaster;
        List<purchaseorder_master> _polist;
        short _selectedcrop;
        string _selectedlocationID;
        string _selectedPOno;

        public ShipmentList(short? crop, string locationid, string POno)
        {
            try
            {
                InitializeComponent();

                _pomaster = new purchaseorder_master();
                _polist = new List<purchaseorder_master>();


                _selectedcrop = (short)crop;
                _selectedlocationID = locationid;
                _selectedPOno = POno;

                cropComboBox.ItemsSource = null;
                cropComboBox.ItemsSource = user_setting.Crops;
                if (_selectedcrop > 0)
                {
                    cropComboBox.ItemsSource = user_setting.Crops.Where(c => c.crop == _selectedcrop);
                    cropComboBox.SelectedIndex = 0;
                    //cropComboBox.SelectedValuePath = _selectedcrop.ToString(); 
                }
                else cropComboBox.SelectedValue = DateTime.Now.Year.ToString();

                locationCombobox.ItemsSource = null;
                locationCombobox.ItemsSource = BLServices.locationBL()
                    .GetlstwithAll().OrderBy(x => x.name);

                if (string.IsNullOrEmpty(_selectedlocationID)) locationCombobox.SelectedIndex = -1;
                else
                {
                    locationCombobox.ItemsSource = BLServices.locationBL()
                        .GetlstwithAll().Where(x => x.locationid == _selectedlocationID).OrderBy(x => x.name);
                    locationCombobox.SelectedIndex = 0;
                }

                if (!string.IsNullOrEmpty(_selectedPOno)) ShipmentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShipmentDataGridBinding()
        {
            try
            {
                if (poComboBox.SelectedIndex < 0)
                    return;

                _pomaster = (purchaseorder_master)poComboBox.SelectedItem;

                locationTextBox.Text = _pomaster.location.name;

                postatusTextBox.Text = _pomaster.postatus;
                postatusTextBox.Background = _pomaster.postatus == "Complete"
                    ? Brushes.LightGreen : Brushes.LightPink;

                approveddate1CheckBox.IsChecked = _pomaster.approveddate1 != null ? true : false;
                approveddate2CheckBox.IsChecked = _pomaster.approveddate2 != null ? true : false;
                approveddate1TextBox.Text = _pomaster.approveddate1.ToString();
                approveddate2TextBox.Text = _pomaster.approveddate2.ToString();

                var shipmentlist = Helper.shipment_master_helper.GetByPono(_pomaster.pono);
                shipmentDataGrid.ItemsSource = null;
                shipmentDataGrid.ItemsSource = shipmentlist.OrderByDescending(x => x.shipmentno);
                TotalTextBlock.Text = shipmentDataGrid.Items.Count.ToString("N0");

                var detailsList = BLServices.shipment_detailsBL()
                    .GetByPono(_pomaster.pono);

                var poQty = BLServices.purchaseorder_detailsBL()
                    .GetByPono(_pomaster.pono)
                    .Sum(x => x.quantity);

                var shippedQty = detailsList
                    .Sum(x => x.shipment_quantity);

                poQtyTextBox.Text = poQty.ToString("N0");
                shippedQtyTextBox.Text = shippedQty.ToString("N0");
                BalanceQtyTextBox.Text = (poQty - shippedQty).ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        private void POListDataBinding()
        {
            try
            {
                if (cropComboBox.SelectedIndex < 0)
                    return;

                if (locationCombobox.SelectedIndex < 0)
                    return;

                var _crop = (InventoryCrop)cropComboBox.SelectedItem;
                var _location = (location)locationCombobox.SelectedItem;

                _polist = BLServices.purchaseorder_masterBL()
                    .GetByCrop(Convert.ToInt16(_crop.crop));

                poComboBox.ItemsSource = null;

                if (_location.name == "All")
                    poComboBox.ItemsSource = _polist.OrderByDescending(x => x.pono);
                else
                    if (string.IsNullOrEmpty(_selectedPOno))
                    {
                        poComboBox.ItemsSource = _polist
                        .Where(x => x.locationid == _location.locationid)
                        .OrderByDescending(x => x.pono);
                    }
                    else
                    {
                        poComboBox.ItemsSource = _polist
                        .Where(x => x.locationid == _location.locationid && x.pono == _selectedPOno)
                        .OrderByDescending(x => x.pono);

                        poComboBox.SelectedIndex = 0;
                    }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void shipmentDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (shipmentDataGrid.SelectedIndex < 0)
                    return;

                var model = (shipment_master_vm)shipmentDataGrid.SelectedItem;
                if (model == null)
                    return;

                ShipmentEdit window = new ShipmentEdit(BLServices.shipment_masterBL()
                    .GetSingle(model.shipmentno));

                window.ShowDialog();
                ShipmentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShipmentDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentDataGrid.SelectedIndex < 0)
                    return;

                var model = (shipment_master_vm)shipmentDataGrid.SelectedItem;

                ShipmentDetails window = new ShipmentDetails(BLServices.shipment_masterBL().GetSingle(model.shipmentno));
                window.ShowDialog();
                ShipmentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shipmentDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (shipment_master_vm)shipmentDataGrid.SelectedItem;
                if (model == null)
                    return;

                BLServices.shipment_masterBL().Delete(model.shipmentno);
                ShipmentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ShipmentDataGridBinding();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_pomaster.pono == null) return;
                ShipmentAdd window = new ShipmentAdd(_pomaster);
                window.ShowDialog();
                ShipmentDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void poComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ShipmentDataGridBinding();
        }

        private void cropComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            POListDataBinding();
        }

        private void locationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            POListDataBinding();
        }
    }
}
