﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.PurchaseOrder
{
    /// <summary>
    /// Interaction logic for POEdit.xaml
    /// </summary>
    public partial class POEdit : Window
    {
        purchaseorder_master _po;

        public POEdit(purchaseorder_master model)
        {
            try
            {
                InitializeComponent();

                _po = model;

                venderCombobox.ItemsSource = null;
                venderCombobox.ItemsSource = BLServices.venderBL()
                    .GetAll()
                    .OrderBy(x => x.name);

                locationCombobox.ItemsSource = null;
                locationCombobox.ItemsSource = BLServices.locationBL()
                    .GetAll()
                    .OrderBy(x => x.name);

                deliverystatusCombobox.ItemsSource = null;
                deliverystatusCombobox.ItemsSource = user_setting.TaskStatus;

                requesterCombobox.ItemsSource = null;
                requesterCombobox.ItemsSource = BLServices.requesterBL().GetAll().OrderBy(x => x.name);

                POBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void POBinding()
        {
            try
            {
                ponoTextBox.Text = _po.pono;
                podateDatePicker.SelectedDate = _po.podate;
                invoicenoTextBox.Text = _po.invoice_no;
                purchaserequisitionnoTextBox.Text = _po.purchaserequisitionno;
                purchaserequisitiondateDatePicker.SelectedDate = _po.purchaserequisitiondate;
                venderCombobox.SelectedValue = _po.venderid;
                locationCombobox.SelectedValue = _po.locationid;
                paymenttermTextBox.Text = _po.paymentterm;
                contactpersonTextBox.Text = _po.contact_person;
                deliveryamtTextBox.Text = Convert.ToDecimal(_po.delivery_amt).ToString("N2");
                totalamtTextBox.Text = Convert.ToDecimal(_po.total_amt).ToString("N2");
                faxdateDatePicker.SelectedDate = _po.faxdate;
                deliverystatusCombobox.SelectedValue = _po.deliverystatus;
                requesterCombobox.SelectedValue = _po.requesteduser;
                noteTextBox.Text = _po.noted;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ponoTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก PO no.");

                if (podateDatePicker.Text == "")
                    throw new ArgumentException("โปรดกรอก po date.");

                if (venderCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดกรอก vender.");

                if (locationCombobox.Text == "")
                    throw new ArgumentException("โปรดกรอก location");

                if (deliverystatusCombobox.Text == "")
                    throw new ArgumentException("โปรดกรอก delivery status");

                if (deliveryamtTextBox.Text.Length > 0 && Helper.RegularExpressionHelper.IsDecimalCharacter(deliveryamtTextBox.Text) == false)
                    throw new ArgumentException("delivery amt จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (totalamtTextBox.Text.Length > 0 && Helper.RegularExpressionHelper.IsDecimalCharacter(totalamtTextBox.Text) == false)
                    throw new ArgumentException("total amt จะต้องเป็นตัวเลขเท่านั้น");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _po.podate = Convert.ToDateTime(podateDatePicker.SelectedDate);
                _po.invoice_no = invoicenoTextBox.Text;
                _po.purchaserequisitionno = purchaserequisitionnoTextBox.Text;
                _po.purchaserequisitiondate = purchaserequisitiondateDatePicker.SelectedDate;
                _po.venderid = venderCombobox.SelectedValue.ToString();
                _po.locationid = locationCombobox.SelectedValue.ToString();
                _po.paymentterm = paymenttermTextBox.Text;
                _po.delivery_amt = Convert.ToDecimal(deliveryamtTextBox.Text);
                _po.total_amt = Convert.ToDecimal(totalamtTextBox.Text);
                _po.contact_person = contactpersonTextBox.Text;
                _po.faxdate = faxdateDatePicker.SelectedDate;
                _po.deliverystatus = deliverystatusCombobox.SelectedValue.ToString();
                _po.modifieddate = DateTime.Now;
                _po.modifieduser = user_setting.User.username;
                _po.noted = noteTextBox.Text;
                _po.requesteduser = Convert.ToInt32(requesterCombobox.SelectedValue);
;
                BLServices.purchaseorder_masterBL().Edit(_po);

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            POBinding();
        }
    }
}
