﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using AGMInventorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.PurchaseOrder
{
    /// <summary>
    /// Interaction logic for POList.xaml
    /// </summary>
    public partial class POList : Page
    {
        public POList()
        {
            InitializeComponent();

            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = DateTime.Now.Year;

            locationCombobox.ItemsSource = null;
            locationCombobox.ItemsSource = BLServices.locationBL()
                .GetlstwithAll().OrderBy(x => x.name);
            locationCombobox.SelectedIndex = -1;

            PODataGridBinding();
        }

        private void PODataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PODataGrid.SelectedIndex < 0)
                    return;

                var model = (purchaseorder_master_vm)PODataGrid.SelectedItem;
                if (model == null)
                    return;

                POEdit window = new POEdit(BLServices.purchaseorder_masterBL()
                    .GetSingle(model.pono));

                window.ShowDialog();
                PODataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PODetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PODataGrid.SelectedIndex < 0)
                    return;

                var model = (purchaseorder_master_vm)PODataGrid.SelectedItem;
                PODetails p = new PODetails(BLServices.purchaseorder_masterBL().GetSingle(model.pono));
                p.ShowDialog();
                PODataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PODataGridBinding()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (locationCombobox.SelectedIndex < 0)
                    return;

                var crop = (InventoryCrop)CropCombobox.SelectedItem;
                var location = (location)locationCombobox.SelectedItem;
                var polist = Helper.purchaseorder_master_helper
                    .GetByCrop(Convert.ToInt16(CropCombobox.SelectedValue));

                PODataGrid.ItemsSource = null;
                if (location.name == "All")
                    PODataGrid.ItemsSource = polist.OrderByDescending(x => x.pono);
                else
                    PODataGrid.ItemsSource = polist
                            .Where(x => x.locationid == location.locationid)
                            .OrderByDescending(x => x.pono);

                TotalTextBlock.Text = PODataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PODataGridBinding();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือกปีการผลิตจากตัวเลือก");

                POAdd window = new POAdd(Convert.ToInt16(CropCombobox.SelectedValue));
                window.ShowDialog();
                PODataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PODataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (purchaseorder_master_vm)PODataGrid.SelectedItem;
                if (model == null)
                    return;

                BLServices.purchaseorder_masterBL().Delete(model.pono);
                PODataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            PODataGridBinding();
        }

        private void locationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PODataGridBinding();
        }
    }
}
