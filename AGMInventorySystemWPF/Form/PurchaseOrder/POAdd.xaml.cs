﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form.PurchaseOrder
{
    /// <summary>
    /// Interaction logic for POAdd.xaml
    /// </summary>
    public partial class POAdd : Window
    {
        short _crop;

        public POAdd(short crop)
        {
            try
            {
                InitializeComponent();

                _crop = crop;

                venderCombobox.ItemsSource = null;
                venderCombobox.ItemsSource = BLServices.venderBL()
                    .GetAll()
                    .OrderBy(x => x.name);

                locationCombobox.ItemsSource = null;
                locationCombobox.ItemsSource = BLServices.locationBL()
                    .GetAll()
                    .OrderBy(x => x.name);

                podateDatePicker.SelectedDate = DateTime.Now;

                requesterCombobox.ItemsSource = null;
                requesterCombobox.ItemsSource = BLServices.requesterBL().GetAll().OrderBy(x => x.name);

                ponoTextBox.Text = BLServices.purchaseorder_masterBL().GetNewPono(crop);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ponoTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก PO no.");

                if (podateDatePicker.Text == "")
                    throw new ArgumentException("โปรดกรอก po date.");

                if (venderCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดกรอก vender.");

                if (locationCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดกรอก location");

                if (deliveryamtTextBox.Text.Length > 0 && Helper.RegularExpressionHelper.IsDecimalCharacter(deliveryamtTextBox.Text) == false)
                    throw new ArgumentException("delivery amt จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (totalamtTextBox.Text.Length > 0 && Helper.RegularExpressionHelper.IsDecimalCharacter(totalamtTextBox.Text) == false)
                    throw new ArgumentException("total amt จะต้องเป็นตัวเลขเท่านั้น");

                if (requesterCombobox.SelectedIndex < 0) throw new ArgumentException("โปรดเลือก requester");

                BLServices.purchaseorder_masterBL()
                    .Add(new purchaseorder_master
                    {
                        pono = "",
                        podate = Convert.ToDateTime(podateDatePicker.SelectedDate),
                        invoice_no = invoicenoTextBox.Text,
                        crop = _crop,
                        purchaserequisitionno = purchaserequisitionnoTextBox.Text,
                        purchaserequisitiondate = purchaserequisitiondateDatePicker.SelectedDate,
                        venderid = venderCombobox.SelectedValue.ToString(),
                        locationid = locationCombobox.SelectedValue.ToString(),
                        paymentterm = paymenttermTextBox.Text,
                        delivery_amt = deliveryamtTextBox.Text == "" ? 0 : Convert.ToDecimal(deliveryamtTextBox.Text),
                        total_amt = totalamtTextBox.Text == "" ? 0 : Convert.ToDecimal(totalamtTextBox.Text),
                        contact_person = contactpersonTextBox.Text,
                        faxdate = faxdateDatePicker.SelectedDate,
                        postatus = "Waiting",
                        deliverystatus = "Waiting",
                        requesteddate = DateTime.Now,
                        //requesteduser = user_setting.User.username,
                        requesteduser = Convert.ToInt32(requesterCombobox.SelectedValue),
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username,
                        noted = noteTextBox.Text
                    });

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ponoTextBox.Text = BLServices.purchaseorder_masterBL().GetNewPono(_crop);

                invoicenoTextBox.Clear();
                purchaserequisitionnoTextBox.Clear();
                paymenttermTextBox.Clear();
                deliveryamtTextBox.Clear();
                totalamtTextBox.Clear();
                contactpersonTextBox.Clear();
                noteTextBox.Clear();

                podateDatePicker.SelectedDate = DateTime.Now;
                purchaserequisitiondateDatePicker.SelectedDate = DateTime.Now;
                faxdateDatePicker.SelectedDate = DateTime.Now;

                venderCombobox.SelectedIndex = -1;
                locationCombobox.SelectedIndex = -1;
                requesterCombobox.SelectedItem = -1;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
