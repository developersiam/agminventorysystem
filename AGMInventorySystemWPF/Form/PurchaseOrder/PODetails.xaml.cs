﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using AGMInventorySystemBL;
using AGMInventorySystemWPF.ViewModel;
using AGMInventorySystemWPF.Helper;

namespace AGMInventorySystemWPF.Form.PurchaseOrder
{

    public partial class PODetails : Window
    {
        purchaseorder_master _pomaster;
        purchaseorder_details _podetails;
        List<purchaseorder_details> _podetailsList;
        int selectedCat;
        int selectedBrand;

        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;

        public PODetails(purchaseorder_master model)
        {
            try
            {

                InitializeComponent();

                _pomaster = new purchaseorder_master();
                _podetails = new purchaseorder_details();
                _podetailsList = new List<purchaseorder_details>();

                _pomaster = model;
                ponoTextBlock.Text = _pomaster.pono;
                locationTextBlock.Text = _pomaster.location.name;

                //Category blinding
                CategoryCombobox.ItemsSource = null;
                CategoryCombobox.ItemsSource = BLServices.material_categoryBL()
                    .GetAll()
                    .OrderBy(x => x.category_name)
                    .ToList();

                itemCombobox.ItemsSource = null;
                itemCombobox.ItemsSource = BLServices.material_itemBL()
                    .GetAll()
                    .OrderBy(x => x.item_name)
                    .ToList();

                PODetailsDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PODetailsDataGrideBinding()
        {
            try
            {
                _podetailsList = BLServices.purchaseorder_detailsBL().GetByPono(_pomaster.pono);
                var itemList = new List<purchase_order_details_vm>();
                itemList = _podetailsList
                    .Select(i => new purchase_order_details_vm
                    {
                        podetailsid = i.podetailsid,
                        pono = i.pono,
                        itemid = i.itemid,
                        item_name = i.material_item.item_name,
                        brand_name = i.material_item.material_brand.brand_name,
                        unit_name = i.material_item.material_unit.unit_name,
                        quantity = i.quantity,
                        unitprice = i.unitprice,
                        vatprice = i.vatprice,
                        totalprice = i.totalprice,
                        lotno = i.material_item.lotno
                    })
                    .ToList();

                podetailsDataGride.ItemsSource = null;
                podetailsDataGride.ItemsSource = itemList;

                TotalTextBlock.Text = _podetailsList.Count.ToString("N0") + " items.";
                TotalPriceTextBlock.Text = _podetailsList.Sum(x => x.totalprice).ToString("N2") + "฿";
                TotalVatPriceTextBlock.Text = _podetailsList.Sum(x => x.vatprice).ToString("N2") + "฿";

                _pomaster = BLServices.purchaseorder_masterBL().GetSingle(_pomaster.pono);
                PrintPOButton.IsEnabled = _pomaster.approveddate2 != null ? true : false;

                approveddate1CheckBox.IsEnabled = user_setting.UserRoles.Where(x => x.rolename == "Approver1").Count() > 0 ? true : false;
                approveddate2CheckBox.IsEnabled = user_setting.UserRoles.Where(x => x.rolename == "Approver2").Count() > 0 ? true : false;

                if (_pomaster.approveddate1 != null)
                {
                    approveddate1TextBox.Text = _pomaster.approveddate1.ToString();
                    approveddate1CheckBox.IsChecked = true;
                }
                else
                {
                    approveddate1TextBox.Clear();
                    approveddate1CheckBox.IsChecked = false;
                }

                if (_pomaster.approveddate2 != null)
                {
                    approveddate2TextBox.Text = _pomaster.approveddate2.ToString();
                    approveddate2CheckBox.IsChecked = true;
                }
                else
                {
                    approveddate2TextBox.Clear();
                    approveddate2CheckBox.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShipmentButton_Click(object sender, RoutedEventArgs e)
        {
            if (_pomaster == null) return;
            if (_podetailsList == null) return;

            var window = new Shipment.ShipmentList(_pomaster.crop, _pomaster.locationid, _pomaster.pono);
            window.ShowDialog();
        }

        private void CategoryCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedCat = 0;
            var cat = (material_category)CategoryCombobox.SelectedItem;
            if (cat == null) return;

            selectedCat = cat.categoryid;

            BrandCombobox.ItemsSource = null;
            BrandCombobox.ItemsSource = BLServices.material_brandBL().GetAll().Where(s => s.categoryid == selectedCat);

            itemCombobox.ItemsSource = null;
        }

        private void BrandCombobox_DropDownClosed(object sender, EventArgs e)
        {
            selectedBrand = 0;
            var brand = (material_brand)BrandCombobox.SelectedItem;
            if (brand == null) return;

            selectedBrand = brand.brandid;

            itemCombobox.ItemsSource = null;
            itemCombobox.ItemsSource = BLServices.material_itemBL().GetAll().Where(s => s.categoryid == selectedCat && s.brandid == selectedBrand).OrderByDescending(s => s.item_name);

        }

        private void approveddate1CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.UserRoles.Where(x => x.rolename == "Approver1").Count() <= 0)
                    throw new ArgumentException("user account ของท่านไม่สามารถ approve PO นี้ได้");

                if (_pomaster.approveddate1 == null)
                    BLServices.purchaseorder_masterBL().Approve(_pomaster.pono, user_setting.User.username);
                else
                    BLServices.purchaseorder_masterBL().CancelApprove(_pomaster.pono);

                PODetailsDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
                approveddate1CheckBox.IsChecked = _pomaster.approveddate1 == null ? false : true;
            }
        }


        private void approveddate2CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.UserRoles.Where(x => x.rolename == "Approver2").Count() <= 0)
                    throw new ArgumentException("user account ของท่านไม่สามารถ approve PO นี้ได้");

                if (_pomaster.approveddate2 == null)
                    BLServices.purchaseorder_masterBL().FinalApprove(_pomaster.pono, user_setting.User.username);
                else
                    BLServices.purchaseorder_masterBL().CancelFinalApprove(_pomaster.pono);

                PODetailsDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
                approveddate1CheckBox.IsChecked = _pomaster.approveddate2 == null ? false : true;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DecimalValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"^*[0-9\.]+$");
            //Regex regex = new Regex(@"[\d]");
            var test = regex.IsMatch(e.Text);
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void ClearForm()
        {
            try
            {
                quantityTextBox.Clear();
                unitpriceTextBox.Clear();
                totalpriceTextBox.Clear();
                vatpriceTextBox.Clear();
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;

                itemCombobox.SelectedValue = null;
                CategoryCombobox.SelectedValue = null;
                BrandCombobox.SelectedValue = null;

                _podetails = null;

                PODetailsDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (itemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ item");

                if (quantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ quantity");

                if (unitpriceTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ unit price");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(quantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsDecimalCharacter(unitpriceTextBox.Text) == false)
                    throw new ArgumentException("Unit price จะต้องเป็นตัวเลขทศนิยม");

                BLServices.purchaseorder_detailsBL()
                    .Add(new purchaseorder_details
                    {
                        podetailsid = Guid.NewGuid(),
                        pono = _pomaster.pono,
                        itemid = Convert.ToInt32(itemCombobox.SelectedValue.ToString()),
                        quantity = Convert.ToInt32(quantityTextBox.Text),
                        unitprice = Convert.ToDecimal(unitpriceTextBox.Text),
                        totalprice = Convert.ToDecimal(totalpriceTextBox.Text),
                        vatprice = Convert.ToDecimal(vatpriceTextBox.Text),
                        modifieddate = DateTime.Now,
                        modifieduser = user_setting.User.username
                    });

                PODetailsDataGrideBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (podetailsDataGride.SelectedIndex < 0)
                    return;

                _podetails = (purchase_order_details_vm)podetailsDataGride.SelectedItem;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                itemCombobox.SelectedValue = _podetails.itemid;
                quantityTextBox.Text = _podetails.quantity.ToString();
                unitpriceTextBox.Text = _podetails.unitprice.ToString();
                totalpriceTextBox.Text = _podetails.totalprice.ToString("N2");
                vatpriceTextBox.Text = _podetails.vatprice.ToString("N2");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                if (itemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ item");

                if (quantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ quantity");

                if (unitpriceTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ unit price");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(quantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsDecimalCharacter(unitpriceTextBox.Text) == false)
                    throw new ArgumentException("Unit price จะต้องเป็นตัวเลขทศนิยม");

                _podetails.itemid = Convert.ToInt32(itemCombobox.SelectedValue.ToString());
                _podetails.quantity = Convert.ToInt32(quantityTextBox.Text);
                _podetails.unitprice = Convert.ToDecimal(unitpriceTextBox.Text);
                _podetails.totalprice = Convert.ToDecimal(totalpriceTextBox.Text);
                _podetails.vatprice = Convert.ToDecimal(vatpriceTextBox.Text);
                _podetails.modifieddate = DateTime.Now;
                _podetails.modifieduser = user_setting.User.username;

                BLServices.purchaseorder_detailsBL().Edit(_podetails);
                PODetailsDataGrideBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (podetailsDataGride.SelectedIndex < 0)
                    return;

                var model = (purchase_order_details_vm)podetailsDataGride.SelectedItem;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BLServices.purchaseorder_detailsBL().Delete(model.podetailsid);
                PODetailsDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void quantityTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            CalculatePrice();
        }

        private void unitpriceTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            CalculatePrice();
        }

        private void CalculatePrice()
        {
            try
            {
                if (unitpriceTextBox.Text == "")
                    return;

                if (quantityTextBox.Text == "")
                    return;

                //var totalprice = Convert.ToDecimal(Convert.ToInt32(quantityTextBox.Text) * Convert.ToDecimal(unitpriceTextBox.Text));
                //var vatprice = Convert.ToDecimal(totalprice * Convert.ToDecimal(0.07)) + totalprice;

                var vatprice = Convert.ToDecimal(Convert.ToInt32(quantityTextBox.Text) * Convert.ToDecimal(unitpriceTextBox.Text));
                var totalprice = Convert.ToDecimal(vatprice) - (Convert.ToDecimal(vatprice) * 7 / 107);

                totalpriceTextBox.Text = totalprice.ToString("N2");
                vatpriceTextBox.Text = vatprice.ToString("N2");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintPOButton_Click(object sender, RoutedEventArgs e)
        {
            PrintPO();
        }

        private void PrintPO()
        {
            try
            {
                string address1;
                string address2;
                decimal exc_unitprice = 0;
                decimal exc_totalprice = 0;
                address1 = _pomaster.vender.homeid;
                if (_pomaster.vender.soi != null) address1 += " soi. " + _pomaster.vender.soi;
                if (_pomaster.vender.moo != null) address1 += " moo " + _pomaster.vender.moo;
                if (_pomaster.vender.road != null) address1 += " Rd." + _pomaster.vender.road;
                if (_pomaster.vender.tumbol != null) address1 += " T." + _pomaster.vender.tumbol;

                address2 = _pomaster.vender.amphur + " " + _pomaster.vender.province + " " + _pomaster.vender.postal;

                excelWorkBook = excelApplication.Workbooks.Open(@"C:\AGMInventory\PO-Form.xlsx");
                excelWorkSheet = excelWorkBook.ActiveSheet;
                excelWorkSheet.PageSetup.Zoom = false;
                excelWorkSheet.PageSetup.FitToPagesWide = 1;
                excelApplication.Visible = true;

                PutDataIntoCell("E7", _pomaster.vender.name.ToString());
                PutDataIntoCell("T7", _pomaster.pono.ToString());
                PutDataIntoCell("T8", _pomaster.podate.ToString("dd/MM/yyyy"));
                PutDataIntoCell("E8", address1);
                PutDataIntoCell("E9", address2);

                PutDataIntoCell("E10", _pomaster.venderid.ToString());
                PutDataIntoCell("E11", _pomaster.contact_person.ToString());
                PutDataIntoCell("T10", _pomaster.purchaserequisitionno.ToString());
                if (_pomaster.purchaserequisitiondate != null) PutDataIntoCell("T11", _pomaster.purchaserequisitiondate.GetValueOrDefault().ToString("dd/MM/yyyy"));

                int row = 13;
                foreach (var item in _podetailsList)
                {
                    exc_unitprice = item.unitprice - (item.unitprice * 7 / 107);
                    exc_totalprice = exc_unitprice * item.quantity;

                    PutDataIntoCell("B" + row, item.itemid.ToString());
                    PutDataIntoCell("C" + row, item.quantity.ToString());
                    PutDataIntoCell("D" + row, item.material_item.material_unit.unit_name.ToString());
                    PutDataIntoCell("E" + row, item.material_item.item_name.ToString());
                    PutDataIntoCell("S" + row, item.unitprice.ToString("N2"));
                    PutDataIntoCell("T" + row, item.vatprice.ToString("N2"));
                    row++;
                }

                if (_pomaster.faxdate != null) PutDataIntoCell("E47", Convert.ToDateTime(_pomaster.faxdate).ToString("dd/MM/yyyy"));
                //if ((_pomaster.deliverydate != null) || (_pomaster.deliverydate == Convert.ToDateTime("{01-Jan-01 12:00:00 AM}"))) PutDataIntoCell("E47", _pomaster.deliverydate.ToString("dd/MM/yyyy"));
                PutDataIntoCell("E49", _pomaster.requesteddate.ToString("dd/MM/yyyy"));
                PutDataIntoCell("D50", _pomaster.paymentterm.ToString());
                PutDataIntoCell("T49", _pomaster.delivery_amt.ToString());
                PutDataIntoCell("T47", _podetailsList.Sum(x => x.totalprice).ToString()); //AMOUNT
                PutDataIntoCell("E42", _pomaster.noted);
                PutDataIntoCell("B57", "( " + _pomaster.requester_user.title + " " + _pomaster.requester_user.name + " " + _pomaster.requester_user.lastname + " )");
                PutDataIntoCell("B58", _pomaster.requester_user.position);

                //excelWorkSheet.PrintOutEx();
                //excelWorkBook.Close(0);
                excelApplication.Quit();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PutDataIntoCell(string cell, string value)
        {
            excelRange = excelWorkSheet.Range[cell];
            excelWorkSheet.Range[cell].Value = value;
        }
    }
}
