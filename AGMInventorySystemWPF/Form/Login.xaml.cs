﻿using AGMInventorySystemBL;
using AGMInventorySystemEntities;
using AGMInventorySystemWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGMInventorySystemWPF.Form
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                LoginButton.IsEnabled = false;
                UsernameTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginProcess();
        }

        private void LoginProcess()
        {
            try
            {
                if (UsernameTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกชื่อผู้ใช้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (PasswordTextBox.Password.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกรหัสผ่าน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Helper.ActiveDirectoryHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ โปรดตรวจสอบกับทางแผนกไอที");

                if (Helper.ActiveDirectoryHelper.ActiveDirectoryAuthenticate(UsernameTextBox.Text, PasswordTextBox.Password) == false)
                    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");

                var userAccount = BLServices.policy_userBL().GetSingle(UsernameTextBox.Text);
                if (userAccount == null)
                    throw new ArgumentException("บัญชีผู้ใช้งานของท่านยังไม่ได้ลงทะเบียนเข้าใช้งานระบบ โปรดติดต่อแผนกไอทีเพื่อทำการเพิ่มสิทธิ์การใช้งานระบบ");

                /// store login information.
                /// 
                user_setting.User = userAccount;
                user_setting.UserRoles = BLServices.policy_userroleBL()
                    .GetByUsername(userAccount.username)
                    .Select(x => new policy_role
                    {
                        roleid = x.roleid,
                        rolename = x.policy_role.rolename
                    }).ToList();

                user_setting.UserLocations = BLServices.user_locationBL()
                    .GetByUsername(userAccount.username)
                    .Select(x => new location
                    {
                        locationid = x.locationid,
                        name = x.location.name
                    }).ToList();

                this.NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            LoginProcess();
        }
    }
}
