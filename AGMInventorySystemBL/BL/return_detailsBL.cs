﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGMInventorySystemBL.BL
{
    public interface Ireturn_detailsBL
    {
        List<return_details> GetRTDetails(string rtno);
        void Add(return_details model);
        decimal GetRTSummaryByissueNo(string issueno, int itemid);
    }
    public class return_detailsBL : Ireturn_detailsBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public return_detailsBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<return_details> GetRTDetails(string rtno)
        {
            return _uow.return_detailsRepository
                .Get(x => x.returnno == rtno,
                null,
                x => x.material_item,
                x => x.material_item.material_brand,
                x => x.material_item.material_unit);
        }
        public void Add(return_details model)
        {
            try
            {
                _uow.return_detailsRepository.Add(model);
                _uow.Save();

                //material_transaction
                _uow.material_transactionRepository.Add(new material_transaction());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public decimal GetRTSummaryByissueNo(string issueno, int itemid)
        {
            try
            {
                decimal tmp_returnqty = 0;
                var rt_master = _uow.return_masterRepository
                .Get(x => x.issueno == issueno).ToList();

                if (rt_master.Count() == 0) return tmp_returnqty;

                foreach (var i in rt_master)
                {
                    var rt_details = _uow.return_detailsRepository.Get(d => d.returnno == i.returnno && d.itemid == itemid);
                    if (rt_details.Count() > 0)
                    {
                        foreach (var d in rt_details)
                        {
                            tmp_returnqty += Convert.ToDecimal(d.return_quantity);
                        }
                    }
                };
                return tmp_returnqty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
