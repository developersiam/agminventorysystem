﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;

namespace AGMInventorySystemBL.BL
{
    public interface Imovement_detailsBL
    {
        void Add(movement_details model);
        //void Edit(movement_master model);
        void Delete(int movementdetailsNo);
        List<movement_details> GetByMovementNo(string movementNo);
        movement_details GetSingle(int movementNoDetailsno);
    }
    public class movement_detailsBL : Imovement_detailsBL
    {
        IAGMInventorySystemUnitOfWork _uow;

        public movement_detailsBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public movement_details GetSingle(int movementNoDetailsno)
        {
            return _uow.movement_detailsRepository
                .GetSingle(x => x.movementdetailsno == movementNoDetailsno
                           ,x => x.material_item);
        }
        public List<movement_details> GetByMovementNo(string movementNo)
        {
            return _uow.movement_detailsRepository
                .Get(x => x.movementno == movementNo);
        }
        public void Add(movement_details item)
        {
            try
            {
                //Movement_details
                _uow.movement_detailsRepository.Add(item);
                _uow.Save();

                //material_transaction
               _uow.material_transactionRepository.Add(new material_transaction());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(int item)
        {
            try
            {
                var model = GetSingle(item);
                _uow.movement_detailsRepository.Remove(model);
                _uow.Save();

                //material_transaction



            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

    }
}
