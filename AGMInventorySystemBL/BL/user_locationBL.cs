﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Iuser_locationBL
    {
        void Add(user_location model);
        void Delete(string username, string locationid);
        List<user_location> GetByUsername(string username);
        user_location GetSingle(string username, string locationid);
    }

    public class user_locationBL : Iuser_locationBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public user_locationBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(user_location model)
        {
            try
            {
                var list = GetByUsername(model.username);
                if (list.Where(x => x.locationid == model.locationid).Count() > 0)
                    throw new ArgumentException("มีการกำหนด location นี้ซ้ำแล้วในระบบ");

                //User only belong to one location && delete all location
                if (list.Count() > 0)
                {
                    foreach(var i in list)
                    {
                        Delete(i.username, i.locationid);
                    }
                }
                    
                 model.createdate = DateTime.Now;

                _uow.user_locationRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, string locationid)
        {
            try
            {
                var model = GetSingle(username, locationid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _uow.user_locationRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<user_location> GetByUsername(string username)
        {
            return _uow.user_locationRepository
                .Get(x => x.username == username,
                null,
                x => x.location)
                .ToList();
        }

        public user_location GetSingle(string username, string locationid)
        {
            return _uow.user_locationRepository
               .GetSingle(x => x.username == username &&
               x.locationid == locationid);
        }
    }
}
