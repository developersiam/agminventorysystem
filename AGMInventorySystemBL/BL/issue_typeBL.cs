﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;

namespace AGMInventorySystemBL.BL
{
    public interface Iissue_typeBL
    {
        issue_type GetSingle(int issuetypeid);
        List<issue_type> GetAll();
    }
    public class issue_typeBL : Iissue_typeBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public issue_typeBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<issue_type> GetAll()
        {
            return _uow.issue_typeRepository.Get().ToList();
        }
        public issue_type GetSingle(int issuetypeid)
        {
            return _uow.issue_typeRepository.GetSingle(x => x.issuetypeid == issuetypeid);
        }

    }
}
