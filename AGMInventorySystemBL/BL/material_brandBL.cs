﻿using AGMInventorySystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using AGMInventorySystemEntities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Imaterial_brandBL
    {
        void Add(material_brand model);
        void Edit(material_brand model);
        void Delete(int brandid);
        material_brand GetSingle(int brandid);
        List<material_brand> GetAll();
        bool UsedforItem(int bandid);
        material_brand GetSingleByName(string brandname);
        string GetBrandNameByItem(int itemid);       
    }

    public class material_brandBL : Imaterial_brandBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public material_brandBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<material_brand> GetAll()
        {
            return _uow.material_brandRepository.Get().ToList();
        }
        public void Add(material_brand model)
        {
            try
            {
                if (_uow.material_brandRepository.Get(x => x.brand_name == model.brand_name).Count() > 0)
                    throw new ArgumentException("This Brand already existed");

                model.modifieddate = DateTime.Now;

                _uow.material_brandRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(int brandid)
        {
            try
            {
                var model = GetSingle(brandid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล Brand นี้ในระบบ");

                _uow.material_brandRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public material_brand GetSingle(int brandid)
        {
            return _uow.material_brandRepository
                .GetSingle(x => x.brandid == brandid);
        }

        public void Edit(material_brand item)
        {
            try
            {
                var editModel = GetSingle(item.brandid);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.brand_name = item.brand_name;
                editModel.brand_description = item.brand_description;
                editModel.modifieduser = item.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.material_brandRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public material_brand GetSingleByName(string brand_name)
        {
            return _uow.material_brandRepository
                .GetSingle(x => x.brand_name == brand_name);
        }
        public bool UsedforItem(int brandid)
        {
            try
            {
                bool used = false;

                var editModel = GetSingle(brandid);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var alreadyinItem = _uow.material_itemRepository.Get(x => x.brandid == brandid).ToList();
                if (alreadyinItem.Count() > 0) used = true;

                return used;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetBrandNameByItem(int itemid)
        {
            try
            {
                string bandname = "";
                int bandid = 0;
                var itemlst = _uow.material_itemRepository.Get(x => x.itemid == itemid);
                if (itemlst.Count() > 0) bandid = itemlst.FirstOrDefault().brandid;
                if (bandid != 0)
                {
                    var bandlst = _uow.material_brandRepository.Get(s => s.brandid == bandid);
                    bandname = bandlst.FirstOrDefault().brand_name;
                }
                
                return bandname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
