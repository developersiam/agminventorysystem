﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;

namespace AGMInventorySystemBL.BL
{
    public interface ISupplierBL
    {
        supplier GetSingle(int supplierid);
        supplier GetSingleByName(string supplierName);
        List<supplier> GetAll();
        void Add(supplier model);
        void Delete(int supplierid);
        void Edit(supplier model);
    }
    public class supplierBL : ISupplierBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public supplierBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<supplier> GetAll()
        {
            return _uow.supplierRepository.Get().ToList();
        }
        public supplier GetSingle(int supplierid)
        {
            return _uow.supplierRepository.GetSingle(x => x.supplierid == supplierid);
        }
        public supplier GetSingleByName(string name)
        {
            return _uow.supplierRepository.GetSingle(x => x.supplier_name == name);
        }

        public void Add(supplier model)
        {
            try
            {

                var list = _uow.supplierRepository.Get();

                if (list.Where(x => x.supplier_name == model.supplier_name).Count() > 0)
                    throw new ArgumentException("Supplier name dupplicated");

                model.modifieddate = DateTime.Now;

                _uow.supplierRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(int supplierid)
        {
            try
            {
                var model = GetSingle(supplierid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var list = _uow.issue_masterRepository.Get(x => x.issuesupplier == supplierid);
                if (list.Count() > 0)
                    throw new ArgumentException("Supplier " + model.supplier_name + " ถูกนำไปใช้อ้างอิงในรายการ Issued แล้ว จึงไม่สามารถลบได้");

                _uow.supplierRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(supplier item)
        {
            try
            {
                var editModel = GetSingle(item.supplierid);
                //var editModel = GetSingleByName(item.supplier_name);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");
              
                editModel.supplier_name = item.supplier_name;
                editModel.modifieduser = item.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.supplierRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
