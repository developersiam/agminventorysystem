﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface IlocationBL
    {
        void Add(location model);
        void Edit(location model);
        void Delete(string locationid);
        location GetSingle(string locationid);
        List<location> GetAll();
        List<location> GetlstwithAll();
    }

    public class locationBL : IlocationBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public locationBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(location model)
        {
            try
            {
                var validation = new DomainValidation.IslocationValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = _uow.locationRepository.Get();

                if (list.Where(x => x.locationid == model.locationid).Count() > 0)
                    throw new ArgumentException("location id dupplicated");

                if (list.Where(x => x.name == model.name).Count() > 0)
                    throw new ArgumentException("location name dupplicated");

                model.modifieddate = DateTime.Now;

                _uow.locationRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string locationid)
        {
            try
            {
                var model = GetSingle(locationid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var list = _uow.purchaseorder_masterRepository.Get(x => x.locationid == locationid);
                if (list.Count() > 0)
                    throw new ArgumentException("location " + model.name + " ถูกนำไปใช้อ้างอิงในรายการข้อมูลออเดอร์แล้ว ไม่สามารถลบได้");

                _uow.locationRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(location model)
        {
            try
            {
                var validation = new DomainValidation.IslocationValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var edit = GetSingle(model.locationid);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                edit.name = model.name;
                edit.description = model.description;
                edit.modifieduser = model.modifieduser;
                edit.modifieddate = DateTime.Now;

                _uow.locationRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<location> GetAll()
        {
            return _uow.locationRepository.Get().ToList();
        }

        public location GetSingle(string locationid)
        {
            return _uow.locationRepository.GetSingle(x => x.locationid == locationid);
        }
        public List<location> GetlstwithAll()
        {
            var lst = _uow.locationRepository.Get().ToList();
            lst.Add(new location { locationid = "00", name = "All" });

            return lst;
        }
    }
}
