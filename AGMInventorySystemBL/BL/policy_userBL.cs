﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL
{
    public interface Ipolicy_userBL
    {
        void Add(policy_user model);
        void Delete(string username);
        policy_user GetSingle(string username);
        List<policy_user> GetAll();
    }

    public class policy_userBL : Ipolicy_userBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public policy_userBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(policy_user model)
        {
            try
            {
                if (_uow.policy_userRepository
                    .Get(x => x.username == model.username)
                    .Count() > 0)
                    throw new ArgumentException("username " + model.username + " นี้มีซ้ำแล้วในระบบ");

                model.createdate = DateTime.Now;

                _uow.policy_userRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username)
        {
            try
            {
                var model = GetSingle(username);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.policy_userrole.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบผู้ใช้นี้ได้เนื่องจากยังมีข้อมูลการกำหนดสิทธิ์การใช้งานระบบอยู่ โปรดลบสิทธิ์การใช้งานระบบออกก่อน");

                _uow.policy_userRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<policy_user> GetAll()
        {
            return _uow.policy_userRepository.Get().ToList();
        }

        public policy_user GetSingle(string username)
        {
            return _uow.policy_userRepository
                .GetSingle(x => x.username == username);
        }
    }
}
