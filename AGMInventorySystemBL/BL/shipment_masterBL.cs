﻿using AGMInventorySystemDAL.EDMX;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Ishipment_masterBL
    {
        void Add(shipment_master model);
        void Edit(shipment_master model);
        void Delete(string shipmentno);
        void Approve(string shipmentno, string approveduser);
        void FinalApprove(string shipmentno, string approveduser);
        void CancelApprove(string shipmentno);
        void CancelFinalApprove(string shipmentno);
        void ChangeReceivedStatus(string shipmentno, bool receivdestatus, string receiveduser);
        void ReceiveList(string shipmentno, string receiveUser);
        void CancelReceiveList(string shipmentno, string receiveUser);
        string GetNewShipmentno(short crop);
        shipment_master GetSingle(string shipmentno);
        List<shipment_master> GetAll();
        List<shipment_master> GetByCrop(short crop);
        List<shipment_master> GetByCropLocation(short crop, string locationid);
        List<shipment_master> GetByLocation(string locationid);
        List<shipment_master> GetByPono(string pono);
    }

    public class shipment_masterBL : Ishipment_masterBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public shipment_masterBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(shipment_master model)
        {
            try
            {
                string _shipmentno = "";

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var pomaster = _uow.purchaseorder_masterRepository.GetSingle(x => x.pono == model.pono);
                if (pomaster.approveddate1 == null || pomaster.approveddate2 == null)
                    throw new ArgumentException("This pono# " +
                        model.pono + " was not accepted by approver2.");

                _shipmentno = GetNewShipmentno(model.crop);

                model.shipmentno = _shipmentno;
                model.shipmentstatus = "Waiting";
                model.modifieddate = DateTime.Now;

                _uow.shipment_masterRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Approve(string shipmentno, string approveduser)
        {
            try
            {
                var model = GetSingle(shipmentno);

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model.shipment_details.Count() <= 0)
                    throw new ArgumentException("ไม่พบรายการ item ภายใน shipment นี้จึงไม่สามารถ approve ได้");

                model.approveddate1 = DateTime.Now;
                model.approveduser1 = approveduser;
                model.shipmentstatus = "Approved 1";

                _uow.shipment_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelApprove(string shipmentno)
        {
            try
            {
                var model = GetSingle(shipmentno);

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.receiveddate != null || model.receiveduser != null)
                    throw new ArgumentException("This shipment document was received by area manager already." +
                        "The system cannot cancel approve this shipment.");

                model.approveddate1 = null;
                model.shipmentstatus = "Waiting";

                _uow.shipment_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelFinalApprove(string shipmentno)
        {
            try
            {
                var model = GetSingle(shipmentno);

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.receiveddate != null || model.receiveduser != null)
                    throw new ArgumentException("This shipment document was received by area manager already." +
                        "The system cannot cancel approve this shipment.");

                model.approveddate2 = null;
                model.shipmentstatus = "Approved 1";

                _uow.shipment_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeReceivedStatus(string shipmentno, bool receivdestatus, string receiveduser)
        {
            try
            {
                var model = GetSingle(shipmentno);

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (receivdestatus == true)
                {
                    model.shipmentstatus = "Complete";
                    model.receivedstatus = true;
                    model.receiveduser = receiveduser;
                    model.receiveddate = DateTime.Now;
                    model.modifieduser = receiveduser;
                    model.modifieddate = DateTime.Now;
                }
                else
                {
                    model.shipmentstatus = "Approved 2";
                    model.receivedstatus = false;
                    model.receiveduser = null;
                    model.receiveddate = null;
                    model.modifieduser = receiveduser;
                    model.modifieddate = DateTime.Now;
                }

                _uow.shipment_masterRepository.Update(model);
                _uow.Save();

                /// หากมีการเปลี่ยนสถานะเป็น Finish ทุก shipment แล้ว ให้เปลี่ยนสถานะ PO ให้เป็น Finish ด้วย
                /// 
                var po = _uow.purchaseorder_masterRepository
                    .GetSingle(x => x.pono == model.pono, x => x.shipment_master);

                if (po.shipment_master.Where(x => x.receivedstatus == false).Count() <= 0)
                    po.postatus = "Complete";
                else
                    po.postatus = "Approved 2";

                _uow.purchaseorder_masterRepository.Update(po);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string shipmentno)
        {
            try
            {
                var model = GetSingle(shipmentno);

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.approveddate1 != null)
                    throw new ArgumentException("shipmentno " + shipmentno + " นี้ได้รับการอนุมัติโดย first approver แล้ว ไม่สามารถลบได้");

                if (model.shipment_details.Count() > 0)
                    throw new ArgumentException("มีรายการ item จำนวน " + model.shipment_details.Count + " รายการ ภายใน shipment นี้จึงไม่สามารถลบข้อมูลนี้ได้");

                _uow.shipment_masterRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(shipment_master model)
        {
            try
            {
                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var edit = GetSingle(model.shipmentno);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (edit.approveddate1 != null)
                    throw new ArgumentException("shipmentno " + edit.shipmentno + " นี้ได้รับการอนุมัติโดย first approver แล้ว ไม่สามารถแก้ไขได้");

                edit.shipmentdate = model.shipmentdate;
                edit.crop = model.crop;
                edit.locationid = model.locationid;
                edit.address = model.address;
                edit.shipmentstatus = model.shipmentstatus;
                edit.remark = model.remark;
                edit.receiveddate = model.receiveddate;
                edit.receivedstatus = model.receivedstatus;
                edit.receiveduser = model.receiveduser;
                edit.requesteddate = model.requesteddate;
                edit.requesteduser = model.requesteduser;
                edit.modifieduser = model.modifieduser;
                edit.modifieddate = DateTime.Now;

                _uow.shipment_masterRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FinalApprove(string shipmentno, string approveduser)
        {
            try
            {
                var model = GetSingle(shipmentno);

                var validation = new DomainValidation.Isshipment_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model.approveddate1 == null)
                    throw new ArgumentException("This shipmentno (#" + model.shipmentno + ") not approved by first approver.");

                model.approveddate2 = DateTime.Now;
                model.approveduser2 = approveduser;
                model.shipmentstatus = "Approved 2";

                _uow.shipment_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<shipment_master> GetAll()
        {
            return _uow.shipment_masterRepository.Get();
        }

        public List<shipment_master> GetByCrop(short crop)
        {
            return _uow.shipment_masterRepository
                .Get(x => x.crop == crop,
                null,
                x => x.location);
        }

        public List<shipment_master> GetByCropLocation(short crop, string locationid)
        {
            return _uow.shipment_masterRepository
                .Get(x => x.crop == crop &&
                x.locationid == locationid,
                null,
                x => x.location);
        }

        public List<shipment_master> GetByLocation(string locationid)
        {
            return _uow.shipment_masterRepository
                .Get(x => x.locationid == locationid,
                null,
                x => x.location);
        }

        public List<shipment_master> GetByPono(string pono)
        {
            return _uow.shipment_masterRepository
                .Get(x => x.pono == pono,
                null,
                x => x.shipment_details,
                x => x.location);
        }

        public string GetNewShipmentno(short crop)
        {
            try
            {
                /// Generate pono.
                /// 
                int _max;
                string _shipmentno;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.shipmentno.Substring(9, 3))) + 1;

                _shipmentno = "AGM-SP" + crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(3, '0');

                return _shipmentno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public shipment_master GetSingle(string shipmentno)
        {
            return _uow.shipment_masterRepository
                .GetSingle(x => x.shipmentno == shipmentno,
                x => x.shipment_details,
                x => x.location);
        }

        public void ReceiveList(string shipmentno, string receiveUser)
        {
            try
            {
                var list = _uow.shipment_detailsRepository
                        .Get(x => x.shipmentno == shipmentno
                        , null
                        , x => x.shipment_master
                        , x => x.material_item);
                if (list.Count() <= 0)
                    throw new ArgumentException("ไม่พบข้อมูลรายการ item ที่จะทำการ receive");

                var shipmentMaster = _uow.shipment_masterRepository
                    .GetSingle(x => x.shipmentno == shipmentno);
                if (shipmentMaster == null)
                    throw new ArgumentException("ไม่พบ shipmentno : " + shipmentno + " นี้ในระบบ");

                if (shipmentMaster.receiveddate != null)
                    throw new ArgumentException("shipmentno นี้ถูก receive ไปแล้ว (received date = " +
                        shipmentMaster.receiveddate + ")");

                foreach (var item in list)
                {
                    // Update receive info in shipment details list.
                    item.received_quantity = item.shipment_quantity;
                    item.receivedstatus = true;
                    item.receiveduser = receiveUser;
                    item.receiveddate = DateTime.Now;
                    _uow.shipment_detailsRepository.Update(item);

                    // Add receive info in transaction table.
                    _uow.material_transactionRepository
                        .Add(new material_transaction
                        {
                            crop = item.shipment_master.crop,
                            refid = item.shipment_master.shipmentno,
                            itemid = item.itemid,
                            unitid = item.material_item.unitid,
                            unitprice = item.unitprice,
                            quantity = item.shipment_quantity,
                            locationid = item.shipment_master.locationid,
                            transactiontype = "In",
                            transactiondate = DateTime.Now,
                            modifieddate = DateTime.Now,
                            modifieduser = receiveUser
                        });
                }

                // Update received and shipment status in shipment master.
                shipmentMaster.shipmentstatus = "Complete";
                shipmentMaster.receivedstatus = true;
                shipmentMaster.receiveduser = receiveUser;
                shipmentMaster.receiveddate = DateTime.Now;
                shipmentMaster.modifieduser = receiveUser;
                shipmentMaster.modifieddate = DateTime.Now;
                _uow.shipment_masterRepository.Update(shipmentMaster);

                // หากมีการเปลี่ยนสถานะเป็น Finish ทุก shipment แล้ว ให้เปลี่ยนสถานะ PO ให้เป็น Finish ด้วย
                var po = _uow.purchaseorder_masterRepository
                    .GetSingle(x => x.pono == shipmentMaster.pono,
                    x => x.shipment_master);
                if (po == null)
                    throw new ArgumentException("ไม่พบข้อมูล pono ของ shipment นี้ในระบบ");

                if (po.shipment_master
                    .Where(x => x.receivedstatus == false)
                    .Count() <= 0)
                    po.postatus = "Complete";
                else
                    po.postatus = "Approved 2";

                _uow.purchaseorder_masterRepository.Update(po);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelReceiveList(string shipmentno, string receiveUser)
        {
            AGM_Inventory_SystemEntities _context = new AGM_Inventory_SystemEntities();
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var list = _uow.shipment_detailsRepository
                        .Get(x => x.shipmentno == shipmentno
                        , null
                        , x => x.shipment_master
                        , x => x.material_item);
                    if (list.Count() <= 0)
                        throw new ArgumentException("ไม่พบข้อมูลรายการ item ที่จะทำการ cancel receive");

                    var shipmentMaster = _uow.shipment_masterRepository
                        .GetSingle(x => x.shipmentno == shipmentno);
                    if (shipmentMaster == null)
                        throw new ArgumentException("ไม่พบ shipmentno : " + shipmentno + " นี้ในระบบ");

                    if (shipmentMaster.receiveddate == null)
                        throw new ArgumentException("shipmentno นี้ยังไม่ถูก receive");

                    foreach (var item in list)
                    {
                        //Update receive info in a shipment details.
                        item.received_quantity = 0;
                        item.receivedstatus = false;
                        item.remark = "last cancel receive item at " + DateTime.Now;
                        _uow.shipment_detailsRepository.Update(item);
                        _uow.Save();

                        //Add cancel receive info in the transaction.
                        _uow.material_transactionRepository
                            .Add(new material_transaction
                            {
                                crop = item.shipment_master.crop,
                                refid = item.shipment_master.shipmentno,
                                itemid = item.itemid,
                                unitid = item.material_item.unitid,
                                unitprice = item.unitprice,
                                quantity = item.shipment_quantity,
                                locationid = item.shipment_master.locationid,
                                transactiontype = "Out",
                                transactiondate = DateTime.Now,
                                modifieddate = DateTime.Now,
                                modifieduser = receiveUser
                            });
                        _uow.Save();
                    }

                    // Update received and shipment status in shipment master.
                    shipmentMaster.shipmentstatus = "Approved 2";
                    shipmentMaster.receivedstatus = false;
                    shipmentMaster.receiveduser = null;
                    shipmentMaster.receiveddate = null;
                    shipmentMaster.modifieduser = receiveUser;
                    shipmentMaster.modifieddate = DateTime.Now;
                    _uow.shipment_masterRepository.Update(shipmentMaster);
                    _uow.Save();

                    // หากมีการเปลี่ยนสถานะเป็น Finish ทุก shipment แล้ว ให้เปลี่ยนสถานะ PO ให้เป็น Finish ด้วย
                    var po = _uow.purchaseorder_masterRepository
                        .GetSingle(x => x.pono == shipmentMaster.pono,
                        x => x.shipment_master);

                    po.postatus = "Approved 2";
                    _uow.purchaseorder_masterRepository.Update(po);
                    _uow.Save();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
    }
}
