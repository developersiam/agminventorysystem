﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;

namespace AGMInventotySystemBL
{
    public interface IrequesterBL
    {
        List<requester_user> GetAll();
        requester_user GetSingleByNameLName(string Name, string LName);
        void Add(requester_user item);
        void Edit(requester_user item);
        void Updatestatus(requester_user item);
    }
    public class requesterBL : IrequesterBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public requesterBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<requester_user> GetAll()
        {
            return _uow.requester_userRepository.Get().Where(x => x.status == true).ToList();
        }

        public requester_user GetSingleByNameLName(string Name, string LName)
        {
            return _uow.requester_userRepository
                .GetSingle(x => x.name == Name && x.lastname == LName);
        }
        public void Add(requester_user item)
        {
            try
            {
                var validation = new DomainValidation.Isrequester_userValid().Validate(item);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                item.modifieddate = DateTime.Now;

                _uow.requester_userRepository.Add(item);
                _uow.Save();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(requester_user item)
        {
            try
            {
                var editModel = GetSingleByNameLName(item.name,item.lastname);
                if (editModel == null) throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.title = item.title;
                editModel.name = item.name;
                editModel.lastname = item.lastname;
                editModel.position = item.position;
                editModel.status = true;
                editModel.modifieduser = item.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.requester_userRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Updatestatus(requester_user item)
        {
            try
            {
                var editModel = GetSingleByNameLName(item.name, item.lastname);
                if (editModel == null) throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.title = item.title;
                editModel.name = item.name;
                editModel.lastname = item.lastname;
                editModel.position = item.position;
                editModel.status = false;
                editModel.modifieduser = item.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.requester_userRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
