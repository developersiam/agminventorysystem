﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL
{
    public interface Ipolicy_roleBL
    {
        void Add(policy_role model);
        void Edit(policy_role model);
        void Delete(Guid id);
        policy_role GetSingle(Guid id);
        List<policy_role> GetAll();
    }

    public class policy_roleBL : Ipolicy_roleBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public policy_roleBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(policy_role model)
        {
            try
            {
                if (_uow.policy_roleRepository
                    .Get(x => x.rolename == model.rolename)
                    .Count() > 0)
                    throw new ArgumentException("role " + model.rolename + " นี้มีซ้ำแล้วในระบบ");

                model.createdate = DateTime.Now;
                model.modifieddate = DateTime.Now;
                model.modifieduser = model.createby;

                _uow.policy_roleRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.policy_userrole.Count() > 0)
                    throw new ArgumentException("Role นี้ถูกกำหนดให้ user บางรายไปแล้ว โปรดลบข้อมูลการกำหนดสิทธิ์ออก่กอน");

                _uow.policy_roleRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(policy_role model)
        {
            try
            {
                var editModel = GetSingle(model.roleid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.rolename = model.rolename;
                editModel.modifieduser = model.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.policy_roleRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<policy_role> GetAll()
        {
            return _uow.policy_roleRepository.Get().ToList();
        }

        public policy_role GetSingle(Guid id)
        {
            return _uow.policy_roleRepository.GetSingle(x => x.roleid == id);
        }
    }
}
