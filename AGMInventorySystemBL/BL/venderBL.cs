﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface IvenderBL
    {
        void Add(vender model);
        void Edit(vender model);
        void Delete(string venderid);
        vender GetSingle(string venderid);
        List<vender> GetAll();
    }

    public class venderBL : IvenderBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public venderBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(vender model)
        {
            try
            {
                var validation = new DomainValidation.IsvenderValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = _uow.venderRepository.Get();

                if (list.Where(x => x.venderid == model.venderid).Count() > 0)
                    throw new ArgumentException("vender id was dupplicated");

                if (list.Where(x => x.name == model.name).Count() > 0)
                    throw new ArgumentException("vender name was dupplicated");

                model.modifieddate = DateTime.Now;

                _uow.venderRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string venderid)
        {
            try
            {
                var model = GetSingle(venderid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var list = _uow.purchaseorder_masterRepository.Get(x => x.venderid == venderid);
                if (list.Count() > 0)
                    throw new ArgumentException("location " + model.name + " ถูกนำไปใช้อ้างอิงในรายการข้อมูลออเดอร์แล้ว ไม่สามารถลบได้");

                _uow.venderRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(vender model)
        {
            try
            {
                var validation = new DomainValidation.IsvenderValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var edit = GetSingle(model.venderid);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                edit.name = model.name;
                edit.homeid = model.homeid;
                edit.soi = model.soi;
                edit.moo = model.moo;
                edit.road = model.road;
                edit.tumbol = model.tumbol;
                edit.amphur = model.amphur;
                edit.province = model.province;
                edit.postal = model.postal;
                edit.tel = model.tel;
                edit.fax = model.fax;
                edit.taxid = model.taxid;
                edit.ssid = model.ssid;
                edit.remark = model.remark;
                edit.modifieduser = model.modifieduser;
                edit.modifieddate = DateTime.Now;

                _uow.venderRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<vender> GetAll()
        {
            return _uow.venderRepository.Get().ToList();
        }

        public vender GetSingle(string venderid)
        {
            return _uow.venderRepository.GetSingle(x => x.venderid == venderid);
        }
    }
}
