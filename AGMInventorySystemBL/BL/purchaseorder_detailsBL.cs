﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Ipurchaseorder_detailsBL
    {
        void Add(purchaseorder_details model);
        void Edit(purchaseorder_details model);
        void Delete(Guid podetailsid);
        purchaseorder_details GetSingle(Guid podetailsid);
        List<purchaseorder_details> GetByPono(string pono);
    }

    public class purchaseorder_detailsBL : Ipurchaseorder_detailsBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public purchaseorder_detailsBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(purchaseorder_details model)
        {
            try
            {
                model.podetailsid = Guid.NewGuid();

                var validation = new DomainValidation.Ispurchaseorder_detailsValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var pomaster = _uow.purchaseorder_masterRepository.GetSingle(x => x.pono == model.pono);
                if (pomaster == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (pomaster.approveddate1 != null)
                    throw new ArgumentException("PO นี้ได้รับการ approve โดย apporver1 แล้วจึงไม่สามารถเพิ่มรายการ item ได้");

                var podetails = GetByPono(model.pono);
                if (podetails.Where(x => x.itemid == model.itemid).Count() > 0)
                    throw new ArgumentException("มีรายการ item นี้ซ้ำแล้วใน PO#" + model.pono);

                _uow.purchaseorder_detailsRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid podetailsid)
        {
            try
            {
                var model = GetSingle(podetailsid);

                var validation = new DomainValidation.Ispurchaseorder_detailsValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                var po = _uow.purchaseorder_masterRepository.GetSingle(x => x.pono == model.pono);
                if (po.approveddate1 != null)
                    throw new ArgumentException("This po was approved by first approver already. It's cannot edit");

                _uow.purchaseorder_detailsRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(purchaseorder_details model)
        {
            try
            {
                var validation = new DomainValidation.Ispurchaseorder_detailsValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var edit = GetSingle(model.podetailsid);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                var po = _uow.purchaseorder_masterRepository.GetSingle(x => x.pono == edit.pono);
                if (po.approveddate1 != null)
                    throw new ArgumentException("This po was approved by first approver already. It's cannot edit");

                edit.quantity = model.quantity;
                edit.unitprice = model.unitprice;
                edit.totalprice = model.totalprice;
                edit.vatprice = model.vatprice;
                edit.modifieddate = DateTime.Now;
                edit.modifieduser = model.modifieduser;

                _uow.purchaseorder_detailsRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<purchaseorder_details> GetByPono(string pono)
        {
            return _uow.purchaseorder_detailsRepository.Get(x => x.pono == pono,
                null,
                x => x.material_item,
                x => x.material_item.material_unit,
                x => x.material_item.material_brand,
                x => x.material_item.material_category,
                x => x.material_item.material_unit);
        }

        public purchaseorder_details GetSingle(Guid podetailsid)
        {
            return _uow.purchaseorder_detailsRepository
                .GetSingle(x => x.podetailsid == podetailsid,
                x => x.material_item);
        }
    }
}
