﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGMInventorySystemBL.BL
{
    public interface Imaterial_itemBL
    {
        void Add(material_item model);
        void Edit(material_item model);
        void Delete(int itemid);
        material_item GetSingle(int itemid);
        List<material_item> GetAll();
        List<material_item> GetByCategory(int categoryid);
        bool UsedforPO(int itemid);
        material_item GetSingleByName(string itemname, int catid, int brandid);
    }

    public class material_itemBL : Imaterial_itemBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public material_itemBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public List<material_item> GetAll()
        {
            return _uow.material_itemRepository
                .Get(null, null,
                x => x.material_brand,
                x => x.material_category,
                x => x.material_unit)
                .ToList();
        }

        public void Add(material_item model)
        {
            try
            {
                var validation = new DomainValidation.IsItemValid().Validate(model);
                if (!validation.IsValid) throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (GetSingleByName(model.item_name, model.categoryid, model.brandid) != null)
                    throw new ArgumentException("This items already existed");

                model.modifieddate = DateTime.Now;

                _uow.material_itemRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int itemid)
        {
            try
            {
                //Check Existed
                var model = GetSingle(itemid);

                var validation = new DomainValidation.IsItemValid().Validate(model);
                if (!validation.IsValid) throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล Unit นี้ในระบบ");

                _uow.material_itemRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UsedforPO(int itemid)
        {
            try
            {
                bool used = false;

                var editModel = GetSingle(itemid);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var alreadyinItem = _uow.purchaseorder_detailsRepository.Get(x => x.itemid == itemid).ToList();
                if (alreadyinItem.Count() > 0) used = true;

                return used;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public material_item GetSingle(int itemid)
        {
            return _uow.material_itemRepository
                    .GetSingle(x => x.itemid == itemid,
                    x => x.purchaseorder_details,
                    x => x.material_brand,
                    x => x.material_category,
                    x => x.material_unit);
        }

        public void Edit(material_item item)
        {
            try
            {
                var validation = new DomainValidation.IsItemValid().Validate(item);
                if (!validation.IsValid) throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(item.itemid);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.item_name = item.item_name;
                editModel.item_description = item.item_description;
                editModel.lotno = item.lotno;
                editModel.unitid = item.unitid;
                editModel.modifieduser = item.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.material_itemRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public material_item GetSingleByName(string name, int catid, int brandid)
        {
            return _uow.material_itemRepository.GetSingle(x => x.item_name == name && x.categoryid == catid && x.brandid == brandid);
        }

        public List<material_item> GetByCategory(int categoryid)
        {
            return _uow.material_itemRepository
                .Get(x => x.categoryid == categoryid,
                null,
                x => x.material_brand,
                x => x.material_category,
                x => x.material_unit)
                .ToList();
        }
    }
}
