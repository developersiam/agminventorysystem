﻿using AGMInventorySystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using AGMInventorySystemEntities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Imaterial_categoryBL
    {
        void Add(material_category model);
        void Edit(material_category model);
        void Delete(int categoryid);
        material_category GetSingle(int catid);
        List<material_category> GetAll();
        bool UsedforItem(int categoryid);
        material_category GetSingleByName(string catName);
    }

    public class material_categoryBL : Imaterial_categoryBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public material_categoryBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<material_category> GetAll()
        {
            return _uow.material_categoryRepository.Get().ToList();
        }
        public void Add(material_category model)
        {
            try
            {
                var validation = new DomainValidation.IsCategoryValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (_uow.material_categoryRepository.Get(x => x.category_name == model.category_name).Count() > 0)
                    throw new ArgumentException("This category name is duplicate");

                model.modifieddate = DateTime.Now;

                _uow.material_categoryRepository.Add(model);
                _uow.Save();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(material_category model)
        {
            try
            {
                var editModel = GetSingleByName(model.category_name);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.category_name = model.category_name;
                editModel.category_description = model.category_description;
                editModel.modifieduser = model.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.material_categoryRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int catid)
        {
            try
            {
                var model = GetSingle(catid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล Category นี้ในระบบ");

                _uow.material_categoryRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public material_category GetSingle(int catid)
        {
            return _uow.material_categoryRepository
                .GetSingle(x => x.categoryid == catid);
        }
        public bool UsedforItem(int categoryid)
        {
            try
            {
                bool used = false;

                var editModel = GetSingle(categoryid);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var alreadyinItem = _uow.material_itemRepository.Get(x => x.categoryid == categoryid).ToList();
                if (alreadyinItem.Count() > 0) used = true;

                return used;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public material_category GetSingleByName(string catName)
        {
            return _uow.material_categoryRepository
                    .GetSingle(x => x.category_name == catName);
        }


    }
}
