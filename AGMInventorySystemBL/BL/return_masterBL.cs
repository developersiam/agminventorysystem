﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGMInventorySystemBL.BL
{
    public interface Ireturn_masterBL
    {
        List<return_master> GetByCrop(short crop);
        return_master GetSingle(string rtno);
        string Add(return_master model);
        void Edit(return_master model);
        void Delete(return_master model);

    }
    public class return_masterBL : Ireturn_masterBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public return_masterBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<return_master> GetByCrop(short crop)
        {
            var list = _uow.return_masterRepository
                .Get(x => x.crop == crop,
                null,
                x => x.location);
            return list;
        }
        public return_master GetSingle(string rtno)
        {
            return _uow.return_masterRepository
                .GetSingle(x => x.returnno == rtno,
                x => x.return_details,
                x => x.location,
                x => x.issue_master);
        }
        public string Add(return_master model)
        {
            try
            {
                string _rtno = "";

                _rtno = GetNewRtno(model.crop);

                model.returnno = _rtno;
                model.requesteddate = DateTime.Now;
                model.requesteduser = model.requesteduser;

                _uow.return_masterRepository.Add(model);
                _uow.Save();

                return _rtno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetNewRtno(short crop)
        {
            try
            {
                /// Generate pono.
                int _max;
                string _mmno;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.returnno.Substring(10, 4))) + 1;

                _mmno = "RETURN-" + crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(4, '0');

                return _mmno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(return_master model)
        {
            try
            {
                var edit = GetSingle(model.returnno);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                edit.remark = model.remark;
                edit.requesteddate = DateTime.Now;
                edit.requesteduser = model.requesteduser;

                _uow.return_masterRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(return_master model)
        {
            try
            {
                var existed = GetSingle(model.returnno);
                if (existed == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _uow.return_masterRepository.Remove(existed);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
