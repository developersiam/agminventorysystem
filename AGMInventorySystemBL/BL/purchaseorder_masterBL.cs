﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Ipurchaseorder_masterBL
    {
        void Add(purchaseorder_master model);
        void Edit(purchaseorder_master model);
        void Delete(string pono);
        void Approve(string pono, string approveduser);
        void FinalApprove(string pono, string approveduser);
        void CancelApprove(string pono);
        void CancelFinalApprove(string pono);
        void ChangePOStatus(string pono, string postatus, string modifieduser);
        void ChangeDeliveryStatus(string pono, string deliverystatus, string modifieduser);
        string GetNewPono(short crop);
        purchaseorder_master GetSingle(string pono);
        List<purchaseorder_master> GetAll();
        List<purchaseorder_master> GetByCrop(short crop);
        List<purchaseorder_master> GetByPurchaseOrderDateRange(DateTime from, DateTime to);
        List<purchaseorder_master> GetByLocation(string locationid);
    }

    public class purchaseorder_masterBL : Ipurchaseorder_masterBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public purchaseorder_masterBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(purchaseorder_master model)
        {
            try
            {
                string _pono = "";

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                _pono = GetNewPono(model.crop);

                model.pono = _pono;
                model.postatus = "Waiting";
                model.deliverystatus = "Waiting";
                model.modifieddate = DateTime.Now;

                _uow.purchaseorder_masterRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Approve(string pono, string approveduser)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model.purchaseorder_details.Count() <= 0)
                    throw new ArgumentException("ไม่พบรายการ item ใน PO นี้จึงไม่สามารถ approve PO นี้ได้");

                model.approveddate1 = DateTime.Now;
                model.approveduser1 = approveduser;
                model.postatus = "Approved 1";

                _uow.purchaseorder_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelApprove(string pono)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.approveddate2 != null)
                    throw new ArgumentException("This pono was approved by final approver already.It's cannot cencel approved.");

                if (model.shipment_master.Count() > 0)
                    throw new ArgumentException("Inside this purchase order have " +
                        model.shipment_master.Count() + "  shipment documents. So, it's not cancel approve.");

                model.approveddate1 = null;
                model.postatus = "Waiting";

                _uow.purchaseorder_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelFinalApprove(string pono)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.shipment_master.Count() > 0)
                    throw new ArgumentException("Inside this purchase order have " +
                        model.shipment_master.Count() + "  shipment documents. So, it's not cancel approve.");

                model.approveddate2 = null;
                model.postatus = "Approved 1";

                _uow.purchaseorder_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeDeliveryStatus(string pono, string deliverystatus, string modifieduser)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                model.deliverystatus = deliverystatus;
                model.modifieduser = modifieduser;
                model.modifieddate = DateTime.Now;

                _uow.purchaseorder_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangePOStatus(string pono, string postatus, string modifieduser)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                model.postatus = postatus;
                model.modifieduser = modifieduser;
                model.modifieddate = DateTime.Now;

                _uow.purchaseorder_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string pono)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.approveddate1 != null)
                    throw new ArgumentException("pono " + pono + " นี้ได้รับการอนุมัติโดย first approver แล้ว ไม่สามารถลบได้");

                _uow.purchaseorder_masterRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(purchaseorder_master model)
        {
            try
            {
                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var edit = GetSingle(model.pono);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (edit.approveddate1 != null)
                    throw new ArgumentException("pono " + edit.pono + " นี้ได้รับการอนุมัติโดย first approver แล้ว ไม่สามารถแก้ไขได้");

                edit.podate = model.podate;
                edit.invoice_no = model.invoice_no;
                edit.purchaserequisitionno = model.purchaserequisitionno;
                edit.purchaserequisitiondate = model.purchaserequisitiondate;
                edit.venderid = model.venderid;
                edit.locationid = model.locationid;
                edit.contact_person = model.contact_person;
                edit.delivery_amt = model.delivery_amt;
                edit.deliverydate = model.deliverydate;
                edit.total_amt = model.total_amt;
                edit.faxdate = model.faxdate;
                edit.paymentterm = model.paymentterm;
                edit.issuedate = model.issuedate;
                edit.issueuser = model.issueuser;
                edit.requesteddate = model.requesteddate;
                edit.requesteduser = model.requesteduser;
                edit.modifieduser = model.modifieduser;
                edit.modifieddate = DateTime.Now;
                edit.noted = model.noted;

                _uow.purchaseorder_masterRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FinalApprove(string pono, string approveduser)
        {
            try
            {
                var model = GetSingle(pono);

                var validation = new DomainValidation.Ispurchaseorder_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.postatus = "Approved 2";
                model.approveddate2 = DateTime.Now;
                model.approveduser2 = approveduser;
                if (model.approveddate1 == null)
                {
                    model.approveddate1 = DateTime.Now;
                    model.approveduser1 = approveduser;
                }

                _uow.purchaseorder_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<purchaseorder_master> GetAll()
        {
            return _uow.purchaseorder_masterRepository.Get();
        }

        public List<purchaseorder_master> GetByCrop(short crop)
        {
            return _uow.purchaseorder_masterRepository
                .Get(x => x.crop == crop,
                null,
                x => x.location,
                x => x.vender);
        }

        public List<purchaseorder_master> GetByPurchaseOrderDateRange(DateTime from, DateTime to)
        {
            var list = _uow.purchaseorder_masterRepository
             .Query(x => DbFunctions.TruncateTime(x.podate) >= DbFunctions.TruncateTime(from) &&
             DbFunctions.TruncateTime(x.podate) <= DbFunctions.TruncateTime(to),
             null,
             x => x.location,
             x => x.vender,
             x => x.shipment_master)
             .ToList();

            return list;
        }

        public List<purchaseorder_master> GetByLocation(string locationid)
        {
            return _uow.purchaseorder_masterRepository
                .Get(x => x.locationid == locationid,
                null,
                x => x.location,
                x => x.vender);
        }

        public string GetNewPono(short crop)
        {
            try
            {
                /// Generate pono.
                /// 
                int _max;
                string _pono;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.pono.Substring(6, 3))) + 1;

                _pono = "AGM" + crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(3, '0');

                return _pono;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public purchaseorder_master GetSingle(string pono)
        {
            return _uow.purchaseorder_masterRepository
                .GetSingle(x => x.pono == pono,
                x => x.purchaseorder_details,
                x => x.location,
                x => x.vender,
                x => x.shipment_master,
                x => x.requester_user);
        }
    }
}
