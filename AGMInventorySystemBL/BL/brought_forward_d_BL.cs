﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Ibrought_forward_d_BL
    {
        void Add(broughtforward_d model);
        List<broughtforward_d> GetBFDetails(string bfno);
    }
    public class brought_forward_d_BL : Ibrought_forward_d_BL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public brought_forward_d_BL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public void Add(broughtforward_d model)
        {
            try
            {
                _uow.broughtforward_dRepository.Add(model);
                _uow.Save();

                //material_transaction
                _uow.material_transactionRepository.Add(new material_transaction());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<broughtforward_d> GetBFDetails(string bfno)
        {
            return _uow.broughtforward_dRepository
                .Get(x => x.broughtforwardid == bfno
                , null
                , x => x.material_item
                , x => x.material_item.material_brand
                , x => x.material_item.material_category
                , x => x.material_item.material_unit);
        }
    }
}
