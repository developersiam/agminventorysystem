﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Ibrought_forwardBL
    {
        List<broughtforward> GetByCrop(short crop);
        string Add(broughtforward model);
        broughtforward GetSingle(string bfno);
        List<broughtforward> GetByCropLocation(short crop, string location);
    }
    public class brought_forwardBL : Ibrought_forwardBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public brought_forwardBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public string Add(broughtforward model)
        {
            try
            {
                string _bfno = "";

                _bfno = GetNewBFno(model.crop);

                model.broughtforwardid = _bfno;
                model.requesteddate = DateTime.Now;

                _uow.broughtforwardRepository.Add(model);
                _uow.Save();

                return _bfno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetNewBFno(short crop)
        {
            try
            {
                /// Generate pono.
                int _max;
                string _mmno;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.broughtforwardid.Substring(6, 4))) + 1;

                _mmno = "BF-" + crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(4, '0');

                return _mmno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<broughtforward> GetByCrop(short crop)
        {
            return _uow.broughtforwardRepository.Get(x => x.crop == crop);
        }
        public broughtforward GetSingle(string bfno)
        {
            return _uow.broughtforwardRepository
                .GetSingle(x => x.broughtforwardid == bfno,
                x => x.broughtforward_d);
        }
        public List<broughtforward> GetByCropLocation(short crop, string locationid)
        {
            return _uow.broughtforwardRepository.Get(x => x.crop == crop && x.locationid == locationid);
        }

    }
}
