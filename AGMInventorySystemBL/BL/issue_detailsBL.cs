﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;

namespace AGMInventorySystemBL.BL
{
    public interface Iissue_detailsBL
    {
        List<issue_details> GetIssueDetails(string isno);
        decimal GetItemQtybyIssueNo(string isno, int itemid);
        void Add(issue_details model);
        //void Edit(issue_details model);
    }
    public class issue_detailsBL : Iissue_detailsBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public issue_detailsBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<issue_details> GetIssueDetails(string isno)
        {
            return _uow.issue_detailsRepository
                .Get(x => x.issueno == isno, null, x => x.material_item);
        }
        public void Add(issue_details model)
        {
            try
            {
                _uow.issue_detailsRepository.Add(model);
                _uow.Save();

                //material_transaction
                _uow.material_transactionRepository.Add(new material_transaction());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public decimal GetItemQtybyIssueNo(string isno, int itemid)
        {
            decimal tmpQty = 0;
            var lstdetails = _uow.issue_detailsRepository.Get(x => x.issueno == isno && x.itemid == itemid);
            if (lstdetails == null) return tmpQty;

            foreach (var i in lstdetails)
            {
                tmpQty += Convert.ToDecimal(i.issue_quantity);
            };
            return tmpQty;
        }

    }
}
