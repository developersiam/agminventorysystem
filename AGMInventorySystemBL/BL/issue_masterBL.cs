﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Iissue_masterBL
    {
        List<issue_master> GetByCrop(short crop);
        string Add(issue_master model);
        void Edit(issue_master model);
        issue_master GetSingle(string isno);
        List<issue_master> GetByCropLocation(short crop, string locationid);
        List<issue_master> GetissuenoByType(int issuetype);
    }
    public class issue_masterBL : Iissue_masterBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public issue_masterBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<issue_master> GetByCrop(short crop)
        {
            return _uow.issue_masterRepository
                .Get(x => x.crop == crop);
        }
        public string Add(issue_master model)
        {
            try
            {
                string _isno = "";

                _isno = GetNewIsno(model.crop);

                model.issueno = _isno;
                model.requesteddate = DateTime.Now;

                _uow.issue_masterRepository.Add(model);
                _uow.Save();

                return _isno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetNewIsno(short crop)
        {
            try
            {
                /// Generate pono.
                int _max;
                string _mmno;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.issueno.Substring(9, 4))) + 1;

                _mmno = "ISSUE-" + crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(4, '0');

                return _mmno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(issue_master model)
        {
            try
            {
                var edit = GetSingle(model.issueno);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                edit.issuepicturepath = model.issuepicturepath;
                edit.issuesupplier = model.issuesupplier;
                edit.issueInternalreason = model.issueInternalreason;
                edit.remark = model.remark;
                edit.requesteddate = DateTime.Now;
                edit.requesteduser = model.requesteduser;

                _uow.issue_masterRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public issue_master GetSingle(string issueno)
        {
            return _uow.issue_masterRepository
                .GetSingle(x => x.issueno == issueno,
                x => x.issue_details);
        }
        public List<issue_master> GetByCropLocation(short crop, string locationid)
        {
            return _uow.issue_masterRepository
                .Get(x => x.crop == crop && x.locationid == locationid);
        }
        public List<issue_master> GetissuenoByType(int issuetype)
        {
            return _uow.issue_masterRepository
                .Get(x => x.issuetype == issuetype).ToList();
        }
    }
}
