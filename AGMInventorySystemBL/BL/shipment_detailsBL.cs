﻿using AGMInventorySystemDAL.EDMX;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Ishipment_detailsBL
    {
        void Add(shipment_details shipmentdetails, shipment_master shipmentmaster);
        void Edit(shipment_details shipmentdetails);
        void Delete(Guid shipmentdetailsid);
        void Receive(Guid shipmentdetailsid, int received_qty, string receiveUser);
        void CancelReceive(Guid shipmentdetailsid, string cancelUser);
        shipment_details GetSingle(Guid shipmentdetailsid);
        List<shipment_details> GetByShipmentno(string shipmentno);
        List<shipment_details> GetByPono(string pono);
    }

    public class shipment_detailsBL : Ishipment_detailsBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public shipment_detailsBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Add(shipment_details shipmentdetails, shipment_master shipmentmaster)
        {
            try
            {
                shipmentdetails.shipmentdetailsid = Guid.NewGuid();

                var validation = new DomainValidation.Isshipment_detailsValid().Validate(shipmentdetails);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                /// ตรวจสอบจำนวน item ที่แบ่งออกไปในแต่ละ shipment จะต้องไม่เกินที่ระบุไว้ใน PO
                ///
                var poItem = _uow.purchaseorder_detailsRepository
                    .GetSingle(x => x.pono == shipmentmaster.pono &&
                    x.itemid == shipmentdetails.itemid);

                var shippedItemList = _uow.shipment_detailsRepository
                    .Get(x => x.itemid == shipmentdetails.itemid &&
                    x.shipment_master.pono == shipmentmaster.pono);

                if (shippedItemList
                    .Where(x => x.shipmentno == shipmentdetails.shipmentno &&
                    x.itemid == shipmentdetails.itemid)
                    .Count() > 0)
                    throw new ArgumentException("ในแต่ละ shipment ไม่อนุญาตให้มีรายการ item ซ้ำกัน");

                /// จำนวนรวมที่แบ่งไปในแต่ละ shipment ของรายการ item นี้ (เมื่อรวมกับจำนวนที่จะบันทึกใหม่จะต้องไม่เกินจำนวนที่ระบุไว้ใน PO)
                /// 
                var shippedItemQty = shippedItemList.Sum(x => x.shipment_quantity);

                if (shippedItemQty + shipmentdetails.shipment_quantity > poItem.quantity)
                    throw new ArgumentException("จำนวนของ item ที่ต้องการบันทึกนี้ เกินกว่าที่ระบุไว้ใน PO");

                /// Unit price ของ item ที่จะบันทึกใน shipment ให้ใช้ค่าเดียวกับที่ระบุไว้ใน PO
                /// 
                shipmentdetails.unitprice = poItem.unitprice;
                shipmentdetails.modifieddate = DateTime.Now;

                _uow.shipment_detailsRepository.Add(shipmentdetails);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelReceive(Guid shipmentdetailsid, string cancelUser)
        {
            try
            {
                var model = GetSingle(shipmentdetailsid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                /// ถ้ามีการยกเลิกข้อมูลการรับสินค้ารายการนี้จะต้องมีการหักออกจากตาราง transaction ด้วย
                /// 
                model.receivedstatus = false;
                model.received_quantity = 0;
                model.remark = "last cancel receive item at " + DateTime.Now;

                _uow.shipment_detailsRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid shipmentdetailsid)
        {
            try
            {
                var model = GetSingle(shipmentdetailsid);

                var validation = new DomainValidation.Isshipment_detailsValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                var shipment = _uow.shipment_masterRepository.GetSingle(x => x.shipmentno == model.shipmentno);
                if (shipment.approveddate1 != null)
                    throw new ArgumentException("This po was approved by first approver already. It's cannot delte.");

                _uow.shipment_detailsRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(shipment_details shipmentdetails)
        {
            try
            {
                var validation = new DomainValidation.Isshipment_detailsValid().Validate(shipmentdetails);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var edit = GetSingle(shipmentdetails.shipmentdetailsid);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                var shipmentmaster = _uow.shipment_masterRepository.GetSingle(x => x.shipmentno == edit.shipmentno);
                if (shipmentmaster.approveddate1 != null)
                    throw new ArgumentException("This po was approved by first approver already. It's cannot edit");

                /// ตรวจสอบจำนวน item ที่แบ่งออกไปในแต่ละ shipment จะต้องไม่เกินที่ระบุไว้ใน PO
                ///
                var poItem = _uow.purchaseorder_detailsRepository
                    .GetSingle(x => x.pono == shipmentmaster.pono &&
                    x.itemid == shipmentdetails.itemid);

                var shippedItemList = _uow.shipment_detailsRepository
                    .Get(x => x.itemid == shipmentdetails.itemid);

                /// จำนวนรวมที่แบ่งไปในแต่ละ shipment ของรายการ item นี้ (เมื่อรวมกับจำนวนที่จะบันทึกใหม่จะต้องไม่เกินจำนวนที่ระบุไว้ใน PO)
                /// 
                var shippedItemQty = shippedItemList.Sum(x => x.shipment_quantity);

                if (shippedItemQty - edit.shipment_quantity + shipmentdetails.shipment_quantity > poItem.quantity)
                    throw new ArgumentException("จำนวนที่ต้องการบันทึกเกินกว่าจำนวนยอดที่ระบุไว้ใน PO");

                edit.shipment_quantity = shipmentdetails.shipment_quantity;
                edit.unitprice = shipmentdetails.unitprice;
                edit.remark = shipmentdetails.remark;
                edit.modifieduser = shipmentdetails.modifieduser;
                edit.modifieddate = DateTime.Now;

                _uow.shipment_detailsRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<shipment_details> GetByPono(string pono)
        {
            return _uow.shipment_detailsRepository
                .Get(x => x.shipment_master.pono == pono,
                null,
                x => x.shipment_master,
                x => x.material_item,
                x => x.material_item.material_unit)
                .ToList();
        }

        public List<shipment_details> GetByShipmentno(string shipmentno)
        {
            return _uow.shipment_detailsRepository.Get(x => x.shipmentno == shipmentno,
                null,
                x => x.material_item,
                x => x.material_item.material_unit);
        }

        public shipment_details GetSingle(Guid shipmentdetailsid)
        {
            return _uow.shipment_detailsRepository
                .GetSingle(x => x.shipmentdetailsid == shipmentdetailsid,
                x => x.material_item);
        }

        public void Receive(Guid shipmentdetailsid, int received_qty, string receiveUser)
        {
            try
            {
                var model = GetSingle(shipmentdetailsid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.shipment_quantity != received_qty)
                    throw new ArgumentException("จำนวนที่รับต้องเท่ากับจำนวนที่ระบุไว้ใน shipment");

                model.received_quantity = received_qty;
                model.receivedstatus = true;
                model.receiveduser = receiveUser;
                model.receiveddate = DateTime.Now;

                _uow.shipment_detailsRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
