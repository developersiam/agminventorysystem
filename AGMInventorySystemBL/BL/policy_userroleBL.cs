﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL
{
    public interface Ipolicy_userroleBL
    {
        void Add(policy_userrole model);
        void Delete(string username, Guid roleid);
        policy_userrole GetSingle(string username, Guid roleid);
        List<policy_userrole> GetAll();
        List<policy_userrole> GetByUsername(string username);
    }

    public class policy_userroleBL : Ipolicy_userroleBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public policy_userroleBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public void Add(policy_userrole model)
        {
            try
            {
                var list = GetByUsername(model.username);
                if (list.Where(x => x.roleid == model.roleid).Count() > 0)
                    throw new ArgumentException("มีการกำหนด role นี้ซ้ำแล้วในระบบ");

                model.createdate = DateTime.Now;
                _uow.policy_userroleRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, Guid roleid)
        {
            try
            {
                var model = GetSingle(username, roleid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _uow.policy_userroleRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<policy_userrole> GetAll()
        {
            return _uow.policy_userroleRepository
                .Get(null, null, x => x.policy_role)
                .ToList();
        }

        public List<policy_userrole> GetByUsername(string username)
        {
            return _uow.policy_userroleRepository
                .Get(x => x.username == username, null,
                x => x.policy_role).ToList();
        }

        public policy_userrole GetSingle(string username, Guid roleid)
        {
            return _uow.policy_userroleRepository
                .GetSingle(x => x.username == username && 
                x.roleid == roleid);
        }
    }
}
