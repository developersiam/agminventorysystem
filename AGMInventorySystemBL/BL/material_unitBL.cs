﻿using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Imaterial_unitBL
    {
        void Add(material_unit model);
        void Edit(material_unit model);
        void Delete(int unitid);
        material_unit GetSingle(int nitid);
        List<material_unit> GetAll();
        bool UsedforItem(int unitid);
        material_unit GetSingleByName(string unitName);
        string GetUnitNameByItem(int itemid);
    }

    public class material_unitBL : Imaterial_unitBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public material_unitBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public List<material_unit> GetAll()
        {
            return _uow.material_unitRepository.Get().ToList();
        }
        public void Add(material_unit model)
        {
            try
            {
                if (_uow.material_unitRepository.Get(x => x.unit_name == model.unit_name).Count() > 0)
                    throw new ArgumentException("This Unit already existed");

                model.modifieddate = DateTime.Now;

                _uow.material_unitRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(int unitid)
        {
            try
            {
                var model = GetSingle(unitid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล Unit นี้ในระบบ");

                _uow.material_unitRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public material_unit GetSingle(int unitid)
        {
            return _uow.material_unitRepository
                .GetSingle(x => x.unitid == unitid);
        }
        public void Edit(material_unit item)
        {
            try
            {
                var editModel = GetSingleByName(item.unit_name);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.unit_name = item.unit_name;
                editModel.unit_description = item.unit_description;
                editModel.modifieduser = item.modifieduser;
                editModel.modifieddate = DateTime.Now;

                _uow.material_unitRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public material_unit GetSingleByName(string unit_name)
        {
            return _uow.material_unitRepository.GetSingle(x => x.unit_name == unit_name);
        }
        public bool UsedforItem(int unitid)
        {
            try
            {
                bool used = false;

                var editModel = GetSingle(unitid);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var alreadyinItem = _uow.material_itemRepository.Get(x => x.unitid == unitid).ToList();
                if (alreadyinItem.Count() > 0) used = true;

                return used;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetUnitNameByItem(int itemid)
        {
            try
            {
                string unitname = "";
                int unitid = 0;
                var itemlst = _uow.material_itemRepository.Get(x => x.itemid == itemid);
                if (itemlst.Count() > 0) unitid = itemlst.FirstOrDefault().unitid;
                if (unitid != 0)
                {
                    var unitlst = _uow.material_unitRepository.Get(s => s.unitid == unitid);
                    unitname = unitlst.FirstOrDefault().unit_name;
                }

                return unitname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
