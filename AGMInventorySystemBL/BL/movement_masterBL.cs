﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;

namespace AGMInventorySystemBL.BL
{
    public interface Imovement_masterBL
    {
        string Add(movement_master model);
        void Edit(movement_master model);
        void Delete(string movementno);
        string GetNewMMno(short crop);
        movement_master GetSingle(string movementno);
        List<movement_master> GetAll();
        List<movement_master> GetByCrop(short crop);
        List<movement_master> GetByCropLocation(short crop, string from_locationid);
        List<movement_master> GetByCropuserLocation(short crop, string userlocationid);
        List<movement_master> GetByCrop2Location(short crop,string from_locationid, string to_locationid);
        void Approve(string movementno, string approveduser);
        void CancelApprove(string movementno);
        void Received(string movementno, string receiveduser);

    }
    public class movement_masterBL : Imovement_masterBL
    {
        IAGMInventorySystemUnitOfWork _uow;
        public movement_masterBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }
        public void Received(string mmNo, string receivedduser)
        {
            try
            {
                var model = GetSingle(mmNo);

                var validation = new DomainValidation.Ismovement_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.receiveddate = DateTime.Now;
                model.receiveduser = receivedduser;
                model.receivedstatus = "Complete";

                _uow.movement_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CancelApprove(string mmNo)
        {
            try
            {
                var model = GetSingle(mmNo);

                var validation = new DomainValidation.Ismovement_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                model.approveddate = null;
                model.approveduser = null;

                _uow.movement_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Approve(string mmNo, string approveduser)
        {
            try
            {
                var model = GetSingle(mmNo);

                var validation = new DomainValidation.Ismovement_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.approveddate = DateTime.Now;
                model.approveduser = approveduser;
                model.receivedstatus = "In Progress";

                _uow.movement_masterRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string Add(movement_master model)
        {
            try
            {
                string _mmno = "";

                _mmno = GetNewMMno(model.crop);

                model.movementno = _mmno;
                model.modifieddate = DateTime.Now;

                _uow.movement_masterRepository.Add(model);
                _uow.Save();

                return _mmno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetNewMMno(short crop)
        {
            try
            {
                /// Generate pono.
                int _max;
                string _mmno;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.movementno.Substring(3, 3))) + 1;

                _mmno = crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(3, '0');

                return _mmno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<movement_master> GetByCrop(short crop)
        {
            return _uow.movement_masterRepository
                .Get(x => x.crop == crop);
        }
        public List<movement_master> GetByCropLocation(short crop,string flocationid)
        {
            return _uow.movement_masterRepository
                .Get(x => x.crop == crop && x.fromlocationid == flocationid);
        }
        public List<movement_master> GetByCrop2Location(short crop, string flocationid,string tlocationid)
        {
            return _uow.movement_masterRepository
                .Get(x => x.crop == crop && x.tolocationid == tlocationid && x.fromlocationid == flocationid);
        }
        public List<movement_master> GetByCropuserLocation(short crop, string userlocationid)
        {
            //Get only user location and to their location with approved
            return _uow.movement_masterRepository
                .Get(x => x.crop == crop && (x.fromlocationid == userlocationid || (x.tolocationid == userlocationid && x.approveddate != null)));
        }
        public void Edit(movement_master model)
        {
            try
            {
                var edit = GetSingle(model.movementno);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                edit.movementdate = model.movementdate;
                edit.fromlocationid = model.fromlocationid;
                edit.tolocationid = model.tolocationid;
                edit.receiveduser = model.receiveduser;
                edit.receivedstatus = model.receivedstatus;
                edit.receiveddate = model.receiveddate;
                edit.approveduser = model.approveduser;
                edit.approveddate = model.approveddate;
                edit.modifieduser = model.modifieduser;
                edit.remark = model.remark;
                edit.modifieddate = DateTime.Now;

                _uow.movement_masterRepository.Update(edit);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public movement_master GetSingle(string movementno)
        {
            return _uow.movement_masterRepository
                .GetSingle(x => x.movementno == movementno,
                x => x.movement_details);
        }
        public void Delete(string movementno)
        {
            try
            {
                var model = GetSingle(movementno);

                var validation = new DomainValidation.Ismovement_masterValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (model.approveddate != null)
                    throw new ArgumentException("movement no " + model.movementno + " นี้ได้รับการอนุมัติโดย Approver แล้ว จึงไม่สามารถลบทิ้งได้");

                var existed_details = BLServices.movement_detailsBL().GetByMovementNo(movementno);
                if (existed_details.Count() > 0) throw new ArgumentException("กรุณา ลบ Items ออกจาก movement no " + model.movementno + " นี้ ก่อนทำการลบทิ้ง");

                _uow.movement_masterRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<movement_master> GetAll()
        {
            return _uow.movement_masterRepository.Get();
        }
    }
}
