﻿using AGMInventorySystemDAL;
using AGMInventorySystemDAL.UnitOfWork;
using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.BL
{
    public interface Imaterial_transactionBL
    {
        void In(material_transaction model);
        void Out(material_transaction model);
        void Return(material_transaction model);
        void Delete(int transactionid);
        material_transaction GetSingle(int transactionid);
        List<material_transaction> GetByLocation(short crop, string locationid);
        List<material_transaction> GetByItem(int itemid);
        List<material_transaction> GetByTransactionType(short crop, string transactiontype);
        List<sp_Material_TransactionByCropLocation_Result> GetMatStockByCropLocation(string locationid);
        List<sp_Material_TransactionByItemID_Result> GetStockByItemid(int itemid);
        List<material_transaction> GetTransactionlistByRefId(string Refid);

    }

    public class material_transactionBL : Imaterial_transactionBL
    {
        AGMInventorySystemUnitOfWork _uow;
        public material_transactionBL()
        {
            _uow = new AGMInventorySystemUnitOfWork();
        }

        public void Delete(int transactionid)
        {
            try
            {
                var model = GetSingle(transactionid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                _uow.material_transactionRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<material_transaction> GetByItem(int itemid)
        {
            return _uow.material_transactionRepository
                .Get(x => x.itemid == itemid
                , null
                , x => x.location)
                .ToList();
        }

        public List<material_transaction> GetByLocation(short crop, string locationid)
        {
            return _uow.material_transactionRepository
                .Get(x => x.crop == crop &&
                x.locationid == locationid)
                .ToList();
        }

        public List<material_transaction> GetByTransactionType(short crop, string transactiontype)
        {
            return _uow.material_transactionRepository
                .Get(x => x.crop == crop &&
                x.transactiontype == transactiontype)
                .ToList();
        }

        public material_transaction GetSingle(int transactionid)
        {
            return _uow.material_transactionRepository
                .GetSingle(x => x.transactionid == transactionid);
        }

        public void In(material_transaction model)
        {
            try
            {
                var validation = new DomainValidation.Ismaterial_transactionValid().Validate(model);
                if (!validation.IsValid) throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.transactiontype = "In";
                model.transactiondate = DateTime.Now;
                model.modifieddate = DateTime.Now;

                _uow.material_transactionRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Out(material_transaction model)
        {
            try
            {
                var validation = new DomainValidation.Ismaterial_transactionValid().Validate(model);
                if (!validation.IsValid) throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.quantity = model.quantity * -1;
                model.transactiontype = "Out";
                model.transactiondate = DateTime.Now;
                model.modifieddate = DateTime.Now;

                _uow.material_transactionRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Return(material_transaction model)
        {
            try
            {
                var validation = new DomainValidation.Ismaterial_transactionValid().Validate(model);
                if (!validation.IsValid) throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.transactiontype = "In";
                model.transactiondate = DateTime.Now;
                model.modifieddate = DateTime.Now;

                _uow.material_transactionRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Material_TransactionByCropLocation_Result> GetMatStockByCropLocation(string locationid)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Material_Get_Stock_ByCropLocation(locationid);
        }
        public List<sp_Material_TransactionByItemID_Result> GetStockByItemid(int itemid)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Material_Get_Transaction_item_ByItemid(itemid);
        }
        public List<material_transaction> GetTransactionlistByRefId(string Refid)
        {
            return _uow.material_transactionRepository
                .Get(x => x.refid == Refid).ToList();
        }
    }
}
