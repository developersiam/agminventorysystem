﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class Ismaterial_transactionValid : Validator<material_transaction>
    {
        public Ismaterial_transactionValid()
        {
            Add("cropIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.crop), "crop field is missing"));
            Add("itemidIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.itemid), "itemid field is missing"));
            Add("unitidIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.unitid), "unitid field is missing"));
            Add("quantityIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.quantity), "quantity field is missing"));
            Add("unitpriceIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.unitprice), "unitprice field is missing"));
            Add("locationidIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.locationid), "locationid field is missing"));
            Add("transactiontypeIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.transactiontype), "transactiontype field is missing"));
            Add("transactiondateIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.transactiondate), "transactiondate field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<material_transaction>(new IsNotNull<material_transaction>(x => x.modifieduser), "modifieduser field is missing"));
        }
    }
}
