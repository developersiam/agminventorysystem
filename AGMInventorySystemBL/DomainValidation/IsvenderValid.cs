﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class IsvenderValid : Validator<vender>
    {
        public IsvenderValid()
        {
            Add("venderidIsNotNullOrWhiteSpace", new Rule<vender>(new IsNotNull<vender>(x => x.venderid), "venderid field is missing"));
            Add("nameIsNotNullOrWhiteSpace", new Rule<vender>(new IsNotNull<vender>(x => x.name), "name field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<vender>(new IsNotNull<vender>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<vender>(new IsNotNull<vender>(x => x.modifieduser), "modifieduser field is missing"));
        }
    }
}
