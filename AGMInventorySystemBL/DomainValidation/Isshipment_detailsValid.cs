﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class Isshipment_detailsValid : Validator<shipment_details>
    {
        public Isshipment_detailsValid()
        {
            Add("shipmentdetailsidIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.shipmentdetailsid), "shipmentdetailsid field is missing"));
            Add("shipmentnoIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.shipmentno), "shipmentno field is missing"));
            Add("itemidIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.itemid), "itemid field is missing"));
            Add("shipmentquantityIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.shipment_quantity), "shipment quantity field is missing"));
            Add("unitpriceIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.unitprice), "unitprice field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<shipment_details>(new IsNotNull<shipment_details>(x => x.modifieduser), "modifieduser field is missing"));

            Add("shipmentquantityValid", new Rule<shipment_details>(new IsExpressionValid<shipment_details>(x => x.shipment_quantity > 0), "shipment quantity จะต้องมากกว่า 0"));
            Add("unitpriceValid", new Rule<shipment_details>(new IsExpressionValid<shipment_details>(x => x.unitprice > 0), "unitprice จะต้องมากกว่า 0"));
        }
    }
}
