﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class Ispurchaseorder_detailsValid : Validator<purchaseorder_details>
    {
        public Ispurchaseorder_detailsValid()
        {
            Add("podetailsidIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.podetailsid), "podetailsid field is missing"));
            Add("ponoIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.pono), "pono field is missing"));
            Add("itemidIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.itemid), "itemid field is missing"));
            Add("quantityIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.quantity), "quantity field is missing"));
            Add("unitpriceIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.unitprice), "unitprice field is missing"));
            Add("totalpriceIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.totalprice), "totalprice field is missing"));
            Add("vatpriceIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.vatprice), "vatprice field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<purchaseorder_details>(new IsNotNull<purchaseorder_details>(x => x.modifieduser), "modifieduser field is missing"));

            Add("quantityValid", new Rule<purchaseorder_details>(new IsExpressionValid<purchaseorder_details>(x => x.quantity > 0), "quantity จะต้องมากกว่า 0"));
            Add("unitpriceValid", new Rule<purchaseorder_details>(new IsExpressionValid<purchaseorder_details>(x => x.unitprice > 0), "unitprice จะต้องมากกว่า 0"));
            Add("totalpriceValid", new Rule<purchaseorder_details>(new IsExpressionValid<purchaseorder_details>(x => x.totalprice > 0), "totalprice จะต้องมากกว่า 0"));
            Add("vatpriceValid", new Rule<purchaseorder_details>(new IsExpressionValid<purchaseorder_details>(x => x.vatprice > 0), "vatprice จะต้องมากกว่า 0"));
        }
    }
}
