﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class Isshipment_masterValid : Validator<shipment_master>
    {
        public Isshipment_masterValid()
        {
            Add("ponoIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.pono), "pono field is missing"));
            Add("shipmentnoIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.shipmentno), "shipmentno field is missing"));
            Add("locationidIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.locationid), "locationid field is missing"));
            Add("shipmentdateIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.shipmentdate), "shipmentdate field is missing"));
            Add("shipmentstatusIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.shipmentstatus), "shipmentstatus field is missing"));
            Add("requesteddateIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.requesteddate), "requesteddate field is missing"));
            Add("requesteduserIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.requesteduser), "requesteduser field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<shipment_master>(new IsNotNull<shipment_master>(x => x.modifieduser), "modifieduser field is missing"));
        }
    }
}
