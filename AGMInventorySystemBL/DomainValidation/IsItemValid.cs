﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;

namespace AGMInventorySystemBL.DomainValidation
{
    public class IsItemValid : Validator<material_item>
    {
        public IsItemValid()
        {
            Add("categoryidIsNotNullOrWhiteSpace", new Rule<material_item>(new IsNotNull<material_item>(x => x.categoryid), "category field is missing"));
            Add("brandidIsNotNullOrWhiteSpace", new Rule<material_item>(new IsNotNull<material_item>(x => x.brandid), "brand name field is missing"));
            Add("unitidIsNotNullOrWhiteSpace", new Rule<material_item>(new IsNotNull<material_item>(x => x.unitid), "unit field is missing"));
            Add("itemnameIsNotNullOrWhiteSpace", new Rule<material_item>(new IsNotNull<material_item>(x => x.item_name), "Item name field is missing"));
            //Add("itemidIsNotNullOrWhiteSpace", new Rule<material_item>(new IsNotNull<material_item>(x => x.itemid), "item id field is missing"));
        }

    }
}
