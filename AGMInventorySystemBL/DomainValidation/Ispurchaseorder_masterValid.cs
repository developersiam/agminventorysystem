﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class Ispurchaseorder_masterValid : Validator<purchaseorder_master>
    {
        public Ispurchaseorder_masterValid()
        {
            Add("ponoIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.pono), "pono field is missing"));
            Add("podateIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.podate), "podate field is missing"));
            Add("locationidIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.locationid), "locationid field is missing"));
            Add("postatusIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.postatus), "postatus field is missing"));
            Add("deliverydateIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.deliverydate), "deliverydate field is missing"));
            Add("deliverystatusIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.deliverystatus), "deliverystatus field is missing"));
            Add("requesteddateIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.requesteddate), "requesteddate field is missing"));
            Add("requesteduserIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.requesteduser), "requesteduser field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<purchaseorder_master>(new IsNotNull<purchaseorder_master>(x => x.modifieduser), "modifieduser field is missing"));
        }
    }
}
