﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;

namespace AGMInventorySystemBL.DomainValidation
{
    public class IsCategoryValid : Validator<material_category>
    {
        public IsCategoryValid()
        {
            Add("categoryidDuplicate", new Rule<material_category>(new IsNotNull<material_category>(x => x.categoryid), "This category is duplicate"));
            //Add("categoryUsedforItem", new Rule<material_category>(new IsNotNull<material_item>(x => x.categoryid), "This category is already used for Item"));
        }
    }
}
