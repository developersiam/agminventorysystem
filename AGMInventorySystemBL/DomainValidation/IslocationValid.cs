﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL.DomainValidation
{
    public class IslocationValid : Validator<location>
    {
        public IslocationValid()
        {
            Add("locationidIsNotNullOrWhiteSpace", new Rule<location>(new IsNotNull<location>(x => x.locationid), "locationid field is missing"));
            Add("nameIsNotNullOrWhiteSpace", new Rule<location>(new IsNotNull<location>(x => x.name), "name field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<location>(new IsNotNull<location>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<location>(new IsNotNull<location>(x => x.modifieduser), "modifieduser field is missing"));
        }
    }
}
