﻿using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;

namespace AGMInventotySystemBL.DomainValidation
{
    public class Isrequester_userValid : Validator<requester_user>
    {
        public Isrequester_userValid()
        {
            Add("nameIsNotNullOrWhiteSpace", new Rule<requester_user>(new IsNotNull<requester_user>(x => x.name), "Name field is missing"));
            Add("lastnameIsNotNullOrWhiteSpace", new Rule<requester_user>(new IsNotNull<requester_user>(x => x.lastname), "Last name field is missing"));
            Add("positionIsNotNullOrWhiteSpace", new Rule<requester_user>(new IsNotNull<requester_user>(x => x.position), "position field is missing"));
        }
    }
}
