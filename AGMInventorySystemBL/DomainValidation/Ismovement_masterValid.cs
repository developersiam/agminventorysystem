﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemBL.DomainValidation.Expression;
using AGMInventorySystemEntities;
using DomainValidation.Validation;

namespace AGMInventorySystemBL.DomainValidation
{
    public class Ismovement_masterValid : Validator<movement_master>
    {
        public Ismovement_masterValid()
        {
            Add("movementnoIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.movementno), "movement no. field is missing"));
            Add("movementdateIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.movementdate), "movement date field is missing"));
            Add("fromlocationidIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.fromlocationid), "from locationid field is missing"));
            Add("tolocationidIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.tolocationid), "to locationid field is missing"));
            Add("createddateIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.createdate), "created date field is missing"));
            Add("createduserIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.createduser), "created user field is missing"));
            Add("modifieddateIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.modifieddate), "modifieddate field is missing"));
            Add("modifieduserIsNotNullOrWhiteSpace", new Rule<movement_master>(new IsNotNull<movement_master>(x => x.modifieduser), "modifieduser field is missing"));
        }
    }
}
