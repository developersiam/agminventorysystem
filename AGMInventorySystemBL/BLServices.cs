﻿using AGMInventorySystemBL.BL;
using AGMInventotySystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemBL
{
    public static class BLServices
    {
        public static IlocationBL locationBL()
        {
            IlocationBL obj = new locationBL();
            return obj;
        }
        public static Imaterial_brandBL material_brandBL()
        {
            Imaterial_brandBL obj = new material_brandBL();
            return obj;
        }
        public static Imaterial_categoryBL material_categoryBL()
        {
            Imaterial_categoryBL obj = new material_categoryBL();
            return obj;
        }
        public static Imaterial_itemBL material_itemBL()
        {
            Imaterial_itemBL obj = new material_itemBL();
            return obj;
        }
        public static Imaterial_unitBL material_unitBL()
        {
            Imaterial_unitBL obj = new material_unitBL();
            return obj;
        }
        public static Ipolicy_roleBL policy_roleBL()
        {
            Ipolicy_roleBL obj = new policy_roleBL();
            return obj;
        }
        public static Ipolicy_userBL policy_userBL()
        {
            Ipolicy_userBL obj = new policy_userBL();
            return obj;
        }
        public static Ipolicy_userroleBL policy_userroleBL()
        {
            Ipolicy_userroleBL obj = new policy_userroleBL();
            return obj;
        }
        public static Ipurchaseorder_detailsBL purchaseorder_detailsBL()
        {
            Ipurchaseorder_detailsBL obj = new purchaseorder_detailsBL();
            return obj;
        }
        public static Ipurchaseorder_masterBL purchaseorder_masterBL()
        {
            Ipurchaseorder_masterBL obj = new purchaseorder_masterBL();
            return obj;
        }
        public static Ishipment_detailsBL shipment_detailsBL()
        {
            Ishipment_detailsBL obj = new shipment_detailsBL();
            return obj;
        }
        public static Ishipment_masterBL shipment_masterBL()
        {
            Ishipment_masterBL obj = new shipment_masterBL();
            return obj;
        }
        public static IvenderBL venderBL()
        {
            IvenderBL obj = new venderBL();
            return obj;
        }
        public static Imovement_detailsBL movement_detailsBL()
        {
            Imovement_detailsBL obj = new movement_detailsBL();
            return obj;
        }
        public static Imovement_masterBL movement_masterBL()
        {
            Imovement_masterBL obj = new movement_masterBL();
            return obj;
        }
        public static Iuser_locationBL user_locationBL()
        {
            Iuser_locationBL obj = new user_locationBL();
            return obj;
        }
        public static Imaterial_transactionBL material_transactionBL()
        {
            Imaterial_transactionBL obj = new material_transactionBL();
            return obj;
        }
        public static IrequesterBL requesterBL()
        {
            IrequesterBL obj = new requesterBL();
            return obj;
        }
        public static Iissue_detailsBL issue_detailsBL()
        {
            Iissue_detailsBL obj = new issue_detailsBL();
            return obj;
        }
        public static Iissue_masterBL issue_masterBL()
        {
            Iissue_masterBL obj = new issue_masterBL();
            return obj;
        }
        public static ISupplierBL supplierBL()
        {
            ISupplierBL obj = new supplierBL();
            return obj;
        }
        public static Iissue_typeBL issue_typeBL()
        {
            Iissue_typeBL obj = new issue_typeBL();
            return obj;
        }
        public static Ibrought_forwardBL brought_forwardBL()
        {
            Ibrought_forwardBL obj = new brought_forwardBL();
            return obj;
        }
        public static Ibrought_forward_d_BL brought_forward_d_BL()
        {
            Ibrought_forward_d_BL obj = new brought_forward_d_BL();
            return obj;
        }
        public static Ireturn_masterBL return_masterBL()
        {
            Ireturn_masterBL obj = new return_masterBL();
            return obj;
        }
        public static Ireturn_detailsBL return_detailsBL()
        {
            Ireturn_detailsBL obj = new return_detailsBL();
            return obj;
        }
    }
}
