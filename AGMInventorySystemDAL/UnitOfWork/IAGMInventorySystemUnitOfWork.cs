﻿using AGMInventorySystemEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGMInventorySystemDAL.UnitOfWork
{
    public interface IAGMInventorySystemUnitOfWork
    {
        IGenericDataRepository<location> locationRepository { get; }
        IGenericDataRepository<material_brand> material_brandRepository { get; }
        IGenericDataRepository<material_category> material_categoryRepository { get; }
        IGenericDataRepository<material_item> material_itemRepository { get; }
        IGenericDataRepository<material_unit> material_unitRepository { get; }
        IGenericDataRepository<policy_role> policy_roleRepository { get; }
        IGenericDataRepository<policy_user> policy_userRepository { get; }
        IGenericDataRepository<policy_userrole> policy_userroleRepository { get; }
        IGenericDataRepository<purchaseorder_details> purchaseorder_detailsRepository { get; }
        IGenericDataRepository<purchaseorder_master> purchaseorder_masterRepository { get; }
        IGenericDataRepository<shipment_details> shipment_detailsRepository { get; }
        IGenericDataRepository<shipment_master> shipment_masterRepository { get; }
        IGenericDataRepository<vender> venderRepository { get; }
        IGenericDataRepository<movement_details> movement_detailsRepository { get; }
        IGenericDataRepository<movement_master> movement_masterRepository { get; }
        IGenericDataRepository<material_transaction> material_transactionRepository { get; }
        IGenericDataRepository<user_location> user_locationRepository { get; }
        IGenericDataRepository<requester_user> requester_userRepository { get; }
        IGenericDataRepository<issue_details> issue_detailsRepository { get; }
        IGenericDataRepository<issue_master> issue_masterRepository { get; }
        IGenericDataRepository<issue_type> issue_typeRepository { get; }
        IGenericDataRepository<supplier> supplierRepository { get; }
        IGenericDataRepository<broughtforward> broughtforwardRepository { get; }
        IGenericDataRepository<broughtforward_d> broughtforward_dRepository { get; }
        IGenericDataRepository<return_details> return_detailsRepository { get; }
        IGenericDataRepository<return_master> return_masterRepository { get; }

        void Save();
    }
}
