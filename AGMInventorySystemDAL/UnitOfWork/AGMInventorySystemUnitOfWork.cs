﻿
using AGMInventorySystemDAL.EDMX;
using AGMInventorySystemEntities;


namespace AGMInventorySystemDAL.UnitOfWork
{
    public class AGMInventorySystemUnitOfWork : IAGMInventorySystemUnitOfWork, System.IDisposable
    {
        private readonly AGM_Inventory_SystemEntities _context;

        private IGenericDataRepository<location> _locationRepository;
        private IGenericDataRepository<material_brand> _material_brandRepository;
        private IGenericDataRepository<material_category> _material_categoryRepository;
        private IGenericDataRepository<material_item> _material_itemRepository;
        private IGenericDataRepository<material_unit> _material_unitRepository;
        private IGenericDataRepository<policy_role> _policy_roleRepository;
        private IGenericDataRepository<policy_user> _policy_userRepository;
        private IGenericDataRepository<policy_userrole> _policy_userroleRepository;
        private IGenericDataRepository<purchaseorder_details> _purchaseorder_detailsRepository;
        private IGenericDataRepository<purchaseorder_master> _purchaseorder_masterRepository;
        private IGenericDataRepository<shipment_details> _shipment_detailsRepository;
        private IGenericDataRepository<shipment_master> _shipment_masterRepository;
        private IGenericDataRepository<vender> _venderRepository;
        private IGenericDataRepository<movement_master> _movement_masterRepository;
        private IGenericDataRepository<movement_details> _movement_detailsRepository;
        private IGenericDataRepository<material_transaction> _material_transactionRepository;
        private IGenericDataRepository<user_location> _user_locationRepository;
        private IGenericDataRepository<requester_user> _requester_userRepository;
        private IGenericDataRepository<issue_master> _issue_masterRepository;
        private IGenericDataRepository<issue_details> _issue_detailsRepository;
        private IGenericDataRepository<issue_type> _issue_typeRepository;
        private IGenericDataRepository<supplier> _supplierRepository;
        private IGenericDataRepository<broughtforward> _broughtforwardRepository;
        private IGenericDataRepository<broughtforward_d> _broughtforward_dRepository;
        private IGenericDataRepository<return_master> _return_masterRepository;
        private IGenericDataRepository<return_details> _return_detailsRepository;

        public AGMInventorySystemUnitOfWork()
        {
            _context = new AGM_Inventory_SystemEntities();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        public IGenericDataRepository<location> locationRepository
        {
            get
            {
                return _locationRepository ?? (_locationRepository = new GenericDataRepository<location>(_context));
            }
        }

        public IGenericDataRepository<material_brand> material_brandRepository
        {
            get
            {
                return _material_brandRepository ?? (_material_brandRepository = new GenericDataRepository<material_brand>(_context));
            }
        }

        public IGenericDataRepository<material_category> material_categoryRepository
        {
            get
            {
                return _material_categoryRepository ?? (_material_categoryRepository = new GenericDataRepository<material_category>(_context));
            }
        }

        public IGenericDataRepository<material_item> material_itemRepository
        {
            get
            {
                return _material_itemRepository ?? (_material_itemRepository = new GenericDataRepository<material_item>(_context));
            }
        }

        public IGenericDataRepository<material_unit> material_unitRepository
        {
            get
            {
                return _material_unitRepository ?? (_material_unitRepository = new GenericDataRepository<material_unit>(_context));
            }
        }

        public IGenericDataRepository<policy_role> policy_roleRepository
        {
            get
            {
                return _policy_roleRepository ?? (_policy_roleRepository = new GenericDataRepository<policy_role>(_context));
            }
        }

        public IGenericDataRepository<policy_user> policy_userRepository
        {
            get
            {
                return _policy_userRepository ?? (_policy_userRepository = new GenericDataRepository<policy_user>(_context));
            }
        }

        public IGenericDataRepository<policy_userrole> policy_userroleRepository
        {
            get
            {
                return _policy_userroleRepository ?? (_policy_userroleRepository = new GenericDataRepository<policy_userrole>(_context));
            }
        }

        public IGenericDataRepository<purchaseorder_details> purchaseorder_detailsRepository
        {
            get
            {
                return _purchaseorder_detailsRepository ?? (_purchaseorder_detailsRepository = new GenericDataRepository<purchaseorder_details>(_context));
            }
        }

        public IGenericDataRepository<purchaseorder_master> purchaseorder_masterRepository
        {
            get
            {
                return _purchaseorder_masterRepository ?? (_purchaseorder_masterRepository = new GenericDataRepository<purchaseorder_master>(_context));
            }
        }

        public IGenericDataRepository<shipment_details> shipment_detailsRepository
        {
            get
            {
                return _shipment_detailsRepository ?? (_shipment_detailsRepository = new GenericDataRepository<shipment_details>(_context));
            }
        }

        public IGenericDataRepository<shipment_master> shipment_masterRepository
        {
            get
            {
                return _shipment_masterRepository ?? (_shipment_masterRepository = new GenericDataRepository<shipment_master>(_context));
            }
        }

        public IGenericDataRepository<movement_master> movement_masterRepository
        {
            get
            {
                return _movement_masterRepository ?? (_movement_masterRepository = new GenericDataRepository<movement_master>(_context));
            }
        }

        public IGenericDataRepository<movement_details> movement_detailsRepository
        {
            get
            {
                return _movement_detailsRepository ?? (_movement_detailsRepository = new GenericDataRepository<movement_details>(_context));
            }
        }

        public IGenericDataRepository<vender> venderRepository
        {
            get
            {
                return _venderRepository ?? (_venderRepository = new GenericDataRepository<vender>(_context));
            }
        }

        public IGenericDataRepository<material_transaction> material_transactionRepository
        {
            get
            {
                return _material_transactionRepository ?? (_material_transactionRepository = new GenericDataRepository<material_transaction>(_context));
            }
        }

        public IGenericDataRepository<user_location> user_locationRepository
        {
            get
            {
                return _user_locationRepository ?? (_user_locationRepository = new GenericDataRepository<user_location>(_context));
            }
        }
        public IGenericDataRepository<requester_user> requester_userRepository
        {
            get
            {
                return _requester_userRepository ?? (_requester_userRepository = new GenericDataRepository<requester_user>(_context));
            }
        }
        public IGenericDataRepository<issue_details> issue_detailsRepository
        {
            get
            {
                return _issue_detailsRepository ?? (_issue_detailsRepository = new GenericDataRepository<issue_details>(_context));
            }
        }

        public IGenericDataRepository<issue_master> issue_masterRepository
        {
            get
            {
                return _issue_masterRepository ?? (_issue_masterRepository = new GenericDataRepository<issue_master>(_context));
            }
        }
        public IGenericDataRepository<issue_type> issue_typeRepository
        {
            get
            {
                return _issue_typeRepository ?? (_issue_typeRepository = new GenericDataRepository<issue_type>(_context));
            }
        }

        public IGenericDataRepository<supplier> supplierRepository
        {
            get
            {
                return _supplierRepository ?? (_supplierRepository = new GenericDataRepository<supplier>(_context));
            }
        }
        public IGenericDataRepository<broughtforward> broughtforwardRepository
        {
            get
            {
                return _broughtforwardRepository ?? (_broughtforwardRepository = new GenericDataRepository<broughtforward>(_context));
            }
        }

        public IGenericDataRepository<broughtforward_d> broughtforward_dRepository
        {
            get
            {
                return _broughtforward_dRepository ?? (_broughtforward_dRepository = new GenericDataRepository<broughtforward_d>(_context));
            }
        }
        public IGenericDataRepository<return_details> return_detailsRepository
        {
            get
            {
                return _return_detailsRepository ?? (_return_detailsRepository = new GenericDataRepository<return_details>(_context));
            }
        }

        public IGenericDataRepository<return_master> return_masterRepository
        {
            get
            {
                return _return_masterRepository ?? (_return_masterRepository = new GenericDataRepository<return_master>(_context));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
