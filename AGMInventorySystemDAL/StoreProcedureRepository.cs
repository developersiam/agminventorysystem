﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGMInventorySystemEntities;
using AGMInventorySystemDAL.EDMX;

namespace AGMInventorySystemDAL
{
    public interface IStoreProcedureRepository
    {
        List<sp_Material_TransactionByCropLocation_Result> sp_Material_Get_Stock_ByCropLocation(string location);
        List<sp_Material_TransactionByItemID_Result> sp_Material_Get_Transaction_item_ByItemid(int itemid);
    }
    public class StoreProcedureRepository : IStoreProcedureRepository
    {
        public List<sp_Material_TransactionByCropLocation_Result> sp_Material_Get_Stock_ByCropLocation(string location)
        {
            try
            {
                using (AGM_Inventory_SystemEntities _context = new AGM_Inventory_SystemEntities())
                {
                    return _context.sp_Material_TransactionByCropLocation(location).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Material_TransactionByItemID_Result> sp_Material_Get_Transaction_item_ByItemid(int itemid)
        {
            try
            {
                using (AGM_Inventory_SystemEntities _context = new AGM_Inventory_SystemEntities())
                {
                    return _context.sp_Material_TransactionByItemID(itemid).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
