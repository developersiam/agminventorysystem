//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AGMInventorySystemEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class policy_userrole
    {
        public string username { get; set; }
        public System.Guid roleid { get; set; }
        public System.DateTime createdate { get; set; }
        public string createuser { get; set; }
    
        public virtual policy_role policy_role { get; set; }
        public virtual policy_user policy_user { get; set; }
    }
}
