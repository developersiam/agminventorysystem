//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AGMInventorySystemEntities
{
    using System;
    
    public partial class sp_Material_TransactionByCropLocation_Result
    {
        public string name { get; set; }
        public string brand_name { get; set; }
        public int itemid { get; set; }
        public string item_name { get; set; }
        public string item_description { get; set; }
        public Nullable<int> RunningTotal { get; set; }
        public string unit_name { get; set; }
        public Nullable<decimal> Average_unitPrice { get; set; }
        public string lotno { get; set; }
    }
}
